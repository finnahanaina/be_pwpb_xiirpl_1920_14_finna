<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;

	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbadmin', TRUE);
	}

	public function view_by_date($date){
        $this->db2->where('DATE(tgl_pemesanan)', $date); // Tambahkan where tanggal nya
        $this->db2->where('status','verived');
        $this->db2->join('petugas','petugas.id_petugas=pemesanan.id_petugas');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
		return $this->db2->get('pemesanan')->result();// Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
	}
    
	public function view_by_month($month, $year){
        $this->db2->where('MONTH(tgl_pemesanan)', $month); // Tambahkan where bulan
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
        $this->db2->where('status','verived');
        $this->db2->join('petugas','petugas.id_petugas=pemesanan.id_petugas');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
	public function view_by_year($year){
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
        $this->db2->where('status','verived');
        $this->db2->join('petugas','petugas.id_petugas=pemesanan.id_petugas');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
	}
    
	public function view_all(){
		$this->db2->where('status','verived');
		$this->db2->join('petugas','petugas.id_petugas=pemesanan.id_petugas');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
		return $this->db2->get('pemesanan')->result(); // Tampilkan semua data transaksi
	}
    
    public function option_tahun(){
        $this->db2->select('YEAR(tgl_pemesanan) AS tahun'); // Ambil Tahun dari field tgl
        $this->db2->where('status','verived');
        $this->db2->from('pemesanan'); // select ke tabel transaksi
        $this->db2->order_by('YEAR(tgl_pemesanan)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db2->group_by('YEAR(tgl_pemesanan)'); // Group berdasarkan tahun pada field tgl
        
        return $this->db2->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }

    // Laporan KEberangkatan
    public function view_by_date_keberangkatan($date){
        $this->db2->where('DATE(tgl_berangkat)', $date); // Tambahkan where tanggal nya
        $this->db2->join('transportasi','transportasi.id_transportasi=rute.id_transportasi');
        $this->db2->join('tipe_transportasi','transportasi.id_tipe_transportasi=tipe_transportasi.id_tipe_transportasi');
		return $this->db2->get('rute')->result();// Tampilkan data transaksi sesuai tanggal yang diinput oleh user pada filter
	}
    
	public function view_by_month_keberangkatan($month, $year){
        $this->db2->where('MONTH(tgl_berangkat)', $month); // Tambahkan where bulan
        $this->db2->where('YEAR(tgl_berangkat)', $year); // Tambahkan where tahun
        $this->db2->join('transportasi','transportasi.id_transportasi=rute.id_transportasi');
        $this->db2->join('tipe_transportasi','transportasi.id_tipe_transportasi=tipe_transportasi.id_tipe_transportasi');
		return $this->db2->get('rute')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
	public function view_by_year_keberangkatan($year){
        $this->db2->where('YEAR(tgl_berangkat)', $year); // Tambahkan where tahun
        $this->db2->join('transportasi','transportasi.id_transportasi=rute.id_transportasi');
         $this->db2->join('tipe_transportasi','transportasi.id_tipe_transportasi=tipe_transportasi.id_tipe_transportasi');
		return $this->db2->get('rute')->result(); // Tampilkan data transaksi sesuai tahun yang diinput oleh user pada filter
	}
    
	public function view_all_keberangkatan(){
		$this->db2->join('transportasi','transportasi.id_transportasi=rute.id_transportasi');
        $this->db2->join('tipe_transportasi','transportasi.id_tipe_transportasi=tipe_transportasi.id_tipe_transportasi');
		return $this->db2->get('rute')->result(); // Tampilkan semua data transaksi
	}
    
    public function option_tahun_keberangkatan(){
        $this->db2->select('YEAR(tgl_berangkat) AS tahun'); // Ambil Tahun dari field tgl
        $this->db2->from('rute'); // select ke tabel transaksi
        $this->db2->order_by('YEAR(tgl_berangkat)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db2->group_by('YEAR(tgl_berangkat)'); // Group berdasarkan tahun pada field tgl
        
        return $this->db2->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }

    //laporan penumpang 
     public function view_by_date_penumpang($date){
        $this->db2->where('DATE(tgl_pemesanan)', $date); // Tambahkan where tanggal nya
       	$this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
         $this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    public function view_by_month_penumpang($month, $year){
        $this->db2->where('MONTH(tgl_pemesanan)', $month); // Tambahkan where bulan
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
        	$this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
         $this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
	public function view_by_year_penumpang($year){
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
       	$this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
         $this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
	public function view_all_penumpang(){
       	$this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
         $this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
		return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
    
    public function option_tahun_penumpang(){
        $this->db2->select('YEAR(tgl_pemesanan) AS tahun'); // Ambil Tahun dari field tgl
        $this->db2->from('pemesanan'); // select ke tabel transaksi
        $this->db2->order_by('YEAR(tgl_pemesanan)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db2->group_by('YEAR(tgl_pemesanan)'); // Group berdasarkan tahun pada field tgl
        
        return $this->db2->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }

    //laporan petugas 
     public function view_by_date_petugas($date){
        $this->db2->where('DATE(tgl_pemesanan)', $date); // Tambahkan where tanggal nya
        $this->db2->where('id_petugas', $this->session->userdata('id_petugas'));
        //$this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        //$this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
        //$this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
        return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
    }
    public function view_by_month_petugas($month, $year){
        $this->db2->where('MONTH(tgl_pemesanan)', $month); // Tambahkan where bulan
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
        $this->db2->where('id_petugas', $this->session->userdata('id_petugas'));
        return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
    }
    
    public function view_by_year_petugas($year){
        $this->db2->where('YEAR(tgl_pemesanan)', $year); // Tambahkan where tahun
        $this->db2->where('id_petugas', $this->session->userdata('id_petugas'));
        return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
    }
    
    public function view_all_petugas(){
       $this->db2->where('id_petugas', $this->session->userdata('id_petugas'));
       return $this->db2->get('pemesanan')->result(); // Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
    }
    
    public function option_tahun_petugas(){
        $this->db2->select('YEAR(tgl_pemesanan) AS tahun'); // Ambil Tahun dari field tgl
        $this->db2->from('pemesanan'); // select ke tabel transaksi
        $this->db2->where('id_petugas', $this->session->userdata('id_petugass'));
        $this->db2->order_by('YEAR(tgl_pemesanan)'); // Urutkan berdasarkan tahun secara Ascending (ASC)
        $this->db2->group_by('YEAR(tgl_pemesanan)'); // Group berdasarkan tahun pada field tgl
        return $this->db2->get()->result(); // Ambil data pada tabel transaksi sesuai kondisi diatas
    }
}
