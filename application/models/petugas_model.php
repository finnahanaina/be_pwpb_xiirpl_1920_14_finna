<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;
	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbpetugas', TRUE);	         
	}

	 public function getverivikasi() {
	   	$this->db2->select('penumpang.*,pemesanan.*');
        $this->db2->from('pemesanan');
        $this->db2->join('penumpang', 'penumpang.id_penumpang=pemesanan.id_penumpang');
       	$this->db2->where('status','unveriv');
       	//$this->db2->where('bukti_pembayaran','');
       //  $this->db2->join('detail_pemesanan', 'pemesanan.id_pemesanan =detail_pemesanan.id_pemesanan');
       //  $this->db2->join('rute', 'detail_pemesanan.id_rute=rute.id_rute');
       // $this->db2->join('harga_per_kelas', 'harga_per_kelas.id_rute=rute.id_rute');
        
        return $this->db2->get()->result_array();
	}
	public function updatepemesanan($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('pemesanan',$data);
		//var_dump($coba);die();
	}	
	public function getlaporanverivikasi() {
		$id_petugas = $this->session->userdata('id_petugas');
		//var_dump($id_petugas);die();
	   	$this->db2->select('pemesanan.*');
        $this->db2->from('pemesanan');        
        $this->db2->where('id_petugas', $id_petugas);
       //  $this->db2->join('detail_pemesanan', 'pemesanan.id_pemesanan =detail_pemesanan.id_pemesanan');
       //  $this->db2->join('rute', 'detail_pemesanan.id_rute=rute.id_rute');
       // $this->db2->join('harga_per_kelas', 'harga_per_kelas.id_rute=rute.id_rute');
        
        return $this->db2->get();
	}
}