<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penumpang_model extends CI_Model {

	public function __construct(){
	  parent::__construct();
	}

	public function tampilrutepesawat($where){
		$this->db->select('vrute.*,partner.*');
		$this->db->join('partner','partner.id=vrute.keterangan');
	 return $this->db->get_where('vrute',$where);
	 }
	public function tampilrutekereta($where){
		$this->db->select('vrute.*,partner.*');
		$this->db->join('partner','partner.id=vrute.keterangan');
	  return $this->db->get_where('vrute',$where);
	}

	public function tampilakun($where){
	  return $this->db->get_where('penumpang',$where);
	}

	public function updatedataakun($where,$data){
		$this->db->where($where);
		return $this->db->update('penumpang',$data);
	}	
	public function get_pembayaran_instan(){
		$this->db->where('keterangan','pembayaran instan');
		return $this->db->get('partner');
	}
	public function get_gerai_retail(){
		$this->db->where('keterangan','gerai retail');
		return $this->db->get('partner');
	}
	public function get_pilihan_metode($metodepembayaran){
		$this->db->where('id',$metodepembayaran);
		return $this->db->get('partner');
	}
	public function get_info_rute($id_rute,$id_harga){
		//var_dump($where);die;
		 return $this->db->get_where('vrute',  array('id_rute' => $id_rute,'id_harga'=>$id_harga ));
		//var_dump($data);die();
	}
	public function insert_detail($data){
	 return $this->db->insert('detail_pemesanan',$data);
	}
	public function insert_pemesanan($data){
	 $this->db->insert('pemesanan',$data);
	 return $this->db->insert_id();
	}
	public function get_where_pemesanan($id){
		$where=array('id_pemesanan'=> $id);
		return $this->db->get_where('pemesanan', $where);
		//var_dump($data);die();
	}
	public function get_where_pemesanan_by_user(){
		$where=array('id_penumpang'=> $this->session->userdata('id_penumpang'));
		$this->db->order_by('tgl_pemesanan','DESC');	
		$this->db->order_by('id_pemesanan','DESC');	

		return $this->db->get_where('pemesanan', $where);
		//var_dump($data);die();
	}
	public function get_where_detail_pemesanan_by_user($id){
		
		$where = array('pemesanan.id_pemesanan' =>$id );
		$this->db->select('pemesanan.*,detail_pemesanan.*,vrute.*');
		$wherevrute=array(
			'v_rute.id_rute'=>' detail_pemesanan.id_rute',
			'vrute.id_harga' => 'pemesanan.id_harga'
		);
		//$this->db->from('pemesanan');
		$this->db->join('detail_pemesanan','pemesanan.id_pemesanan=detail_pemesanan.id_pemesanan');
		$this->db->join('vrute','vrute.id_harga = pemesanan.id_harga' );
		
		return $this->db->get_where('pemesanan', $where);
		
	}
	public function konfirmasi_pembayaran($id,$data){
		$this->db->where('id_pemesanan', $id);
		return $this->db->update('pemesanan', $data);
	}

	public function get_where_kursi($id_transportasi,$bagian){
		$this->db->join('detail_pemesanan','detail_pemesanan.kode_kursi=kursi.id','left outer');
		$this->db->where('bagian',$bagian);
		$this->db->where('kode_kursi',null);
		$this->db->where('id_transportasi',$id_transportasi);
		return $this->db->get('kursi');
	}

	public function updatekursi($id,$data){
		$this->db->where('id_detail_pemesanan', $id);
		return $this->db->update('detail_pemesanan', $data);
	}

	public function get_where_detail_pemesanan_by_user_for_cetak($id_pemesanan,$id_detail_pemesanan){
		
		$where = array('pemesanan.id_pemesanan' =>$id_pemesanan,'detail_pemesanan.id_detail_pemesanan' =>$id_detail_pemesanan );
		$this->db->select('pemesanan.*,detail_pemesanan.*,transportasi.*,vrute.*,partner.*');
		$wherevrute=array(
			'v_rute.id_rute'=>' detail_pemesanan.id_rute',
			'vrute.id_harga' => 'pemesanan.id_harga'
		);
		//$this->db->from('pemesanan');
		$this->db->join('detail_pemesanan','pemesanan.id_pemesanan=detail_pemesanan.id_pemesanan');
		$this->db->join('vrute','vrute.id_harga = pemesanan.id_harga' );
		$this->db->join('transportasi','vrute.id_transportasi = transportasi.id_transportasi' );
		$this->db->join('partner','transportasi.keterangan = partner.id' );
		
		return $this->db->get_where('pemesanan', $where);
		
	}
 
} 