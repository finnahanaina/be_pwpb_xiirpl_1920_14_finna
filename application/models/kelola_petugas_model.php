<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kelola_petugas_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;
	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbadmin', TRUE);	         
	}
	public function get(){
		$this->db2->where('id_level',2);
		return $this->db2->get('petugas');
	}
	public function getpetugasbyid($id){
		$this->db2->where('id_petugas',$id);
		return $this->db2->get('petugas');
	}
	public function addpetugas($data){
		return $this->db2->insert('petugas',$data);
	}
	public function editpetugas($id,$data){
		$where = array('id_petugas' => $id );
		//var_dump($where);die();
		$this->db2->where('id_petugas',$id);
		return $this->db2->update('petugas',$data);
	}
	public function deletepetugas($id){
		$this->db2->where('id_petugas',$id);
		return $this->db2->delete('petugas');
	}

}