<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;
	private $db3;

	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbadmin', TRUE);
	         $this->db3 = $this->load->database('dbpetugas', TRUE);
	}

	public function loginAdmin($table,$where) {
	  return $this->db2->get_where($table,$where);
	}

	public function loginPetugas($table,$where) {
	    return $this->db3->get_where($table,$where);
	}

	public function loginPenumpang($table,$where) {
	    return $this->db->get_where($table,$where);
	}
 
} 