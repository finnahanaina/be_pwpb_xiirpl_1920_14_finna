<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;
	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbadmin', TRUE);	         
	}

	
	

	public function tampiltransportasi() {
	   	$this->db2->select('transportasi.*,nama_tipe,partner.*');
        $this->db2->from('transportasi');
        $this->db2->join('tipe_transportasi', 'tipe_transportasi.id_tipe_transportasi=transportasi.id_tipe_transportasi');
        $this->db2->join('partner', 'partner.id=transportasi.keterangan');
        
        return $this->db2->get()->result_array();
	}
	public function tampilrute() {
	   	$this->db2->select('rute.*,kode_transportasi,nama_tipe');
        $this->db2->from('rute');
        $this->db2->join('transportasi', 'transportasi.id_transportasi=rute.id_transportasi');
        $this->db2->join('tipe_transportasi', 'tipe_transportasi.id_tipe_transportasi=transportasi.id_tipe_transportasi');
        
        return $this->db2->get()->result_array();
	}

	function editdatatransportasi($table,$where){		
		return $this->db2->get_where($table,$where);
	}
	function updatedatatransportasi($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('transportasi',$data);
		return $coba;
	}	
	function getkursi($id){
		$this->db2->where('id_transportasi', $id);
		
		return $this->db2->get('kursi');
	}
	function insertkursi($data){
		return $this->db2->insert('kursi',$data);
	}
	function deletekursi($id){
		
		$where = array('id' => $id);
		return $this->db2->delete('kursi',$where);
	}
	function deletekursibytransportasi($id){
		
		$where = array('id_transportasi' => $id);
		return $this->db2->delete('kursi',$where);
	}

	function editdatarute($table,$where){		
		return $this->db2->get_where($table,$where);
	}
		
	function updatedatarute($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('rute',$data);
		//var_dump($coba);die();
	}	

	function updatedataekonomi($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('harga_per_kelas',$data);
		//var_dump($coba);die();
	}	
	function updatedatabisnis($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('harga_per_kelas',$data);
		//var_dump($coba);die();
	}	

	function hapusdatausertransportasi($where,$table){
		$this->db2->where($where);
		$this->db2->delete($table);
	}
	function inputdatatransportasi($data,$table){
		$this->db2->insert($table,$data);
		return $this->db2->insert_id();
	}
	function inputdatarute($data){
		$this->db2->insert('rute',$data);
		  return $this->db2->insert_id();
		
	}
	function inputdataharga($data,$table){
		$this->db2->insert($table,$data);
	}

	
	  public function hapustransportasi($id)
    {
        // $this->db->where('id', $id);
        return $this->db2->delete('transportasi', ['id_transportasi' => $id]);
    }
     public function hapusrute($id)
    { 
        // $this->db->where('id', $id);
        $this->db2->delete('rute', ['id_rute' => $id]);
    }
    public function getverivikasi() {
	   	$this->db2->select('penumpang.*,pemesanan.*');
        $this->db2->from('pemesanan');
        $this->db2->join('penumpang', 'penumpang.id_penumpang=pemesanan.id_penumpang');
       $this->db2->where('status','unveriv');
       //  $this->db2->join('detail_pemesanan', 'pemesanan.id_pemesanan =detail_pemesanan.id_pemesanan');
       //  $this->db2->join('rute', 'detail_pemesanan.id_rute=rute.id_rute');
       // $this->db2->join('harga_per_kelas', 'harga_per_kelas.id_rute=rute.id_rute');
        
        return $this->db2->get()->result_array();
	}
	public function updatepemesanan($where,$data,$table){
		$this->db2->where($where);
		$coba = $this->db2->update('pemesanan',$data);
		//var_dump($coba);die();
	}	
	public function getlaporankeberangkatan(){
		
		
		$tahun=date('Y');
		$bulan=date('m');
		$tgl = date('d');
		$date= $tahun .'-'. $bulan .'-'. $tgl;
		// echo $date;die();
		// echo "2019-09-18";die();
		$where = array(
			'tgl_berangkat' =>$date
			 );
			
		$this->db2->select('rute.*,kode_transportasi,nama_tipe');
        $this->db2->from('rute');
        $this->db2->join('transportasi', 'transportasi.id_transportasi=rute.id_transportasi');      
        $this->db2->join('tipe_transportasi', 'tipe_transportasi.id_tipe_transportasi=transportasi.id_tipe_transportasi');
        //$this->db2->form('rute');
        $this->db2->where(['tgl_berangkat'=>$date]);
        return $this->db2->get();

        //var_dump($coba);die();
	}

	public function getpenumpanghariini(){
		
		
		$tahun=date('Y');
		$bulan=date('m');
		$tgl = date('d');
		$date= $tahun .'-'. $bulan .'-'. $tgl;
		// echo $date;die();
		// echo "2019-09-18";die();
		$where = array(
			'tgl_berangkat' =>$date
			 );
			
		$this->db2->select('detail_pemesanan.*,rute.*,pemesanan.*');
        $this->db2->from('pemesanan');
        $this->db2->join('detail_pemesanan','detail_pemesanan.id_pemesanan=pemesanan.id_pemesanan');
        $this->db2->join('rute', 'rute.id_rute=detail_pemesanan.id_rute');
        $this->db2->where(['tgl_berangkat'=>$date]);
        return $this->db2->get();

        //var_dump($coba);die();
	}

	public function getpendapatan(){
		
		
		$tahun=date('Y');
		$bulan=date('m');
		$tgl = date('d');
		$date= $tahun .'-'. $bulan .'-'. $tgl;
		// echo $date;die();
		// echo "2019-09-18";die();
		$where = array(
			'tgl_berangkat' =>$date
			 );
			
		$this->db2->select('pemesanan.*,detail_pemesanan.*');
        $this->db2->from('pemesanan');   
        $this->db2->join('detail_pemesanan','pemesanan.id_pemesanan=detail_pemesanan.id_pemesanan');     
        $this->db2->where(['pemesanan.status'=>'verived']);
        $this->db2->order_by('tgl_pemesanan','DESC');
        return $this->db2->get();

        //var_dump($coba);die();
	}
	
	public function view_by_month_bulan($bulan,$tahun){
        $this->db2->where('MONTH(tgl_pemesanan)', $bulan); // Tambahkan where bulan
        $this->db2->where('YEAR(tgl_pemesanan)', $tahun); // Tambahkan where tahun
        $this->db2->where('status','verived');
        $this->db2->join('penumpang','penumpang.id_penumpang=pemesanan.id_penumpang');
		return $this->db2->get('pemesanan');// Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}
	public function getpemesananbytype($tipe){
        $this->db2->where('nama_tipe', $tipe);
        $this->db2->join('rute','rute.id_rute=detail_pemesanan.id_rute');
        $this->db2->join('transportasi','transportasi.id_transportasi=rute.id_transportasi');
        $this->db2->join('tipe_transportasi','tipe_transportasi.id_tipe_transportasi=transportasi.id_tipe_transportasi');
		return $this->db2->get('detail_pemesanan');// Tampilkan data transaksi sesuai bulan dan tahun yang diinput oleh user pada filter
	}

}