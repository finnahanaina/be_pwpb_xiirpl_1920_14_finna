<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner_model extends CI_Model {
	 // db2 digunakan untuk mengakses database ke-2
	private $db2;
	public function __construct(){
	  parent::__construct();
	         $this->db2 = $this->load->database('dbadmin', TRUE);	         
	}
	public function get(){
		return $this->db2->get('partner');
	}
	public function getpartnerbyid($id){
		$this->db2->where('id',$id);
		return $this->db2->get('partner');
	}
	public function get_partner_transportasi($id){
		$this->db2->where('keterangan',$id);
		return $this->db2->get('partner');
	}
	public function addpartner($data){
		return $this->db2->insert('partner',$data);
	}
	public function editpartner($id,$data){
		$where = array('id' => $id );
		//var_dump($where);die();
		$this->db2->where('id',$id);
		return $this->db2->update('partner',$data);
	}
	public function deletepartner($id){
		$this->db2->where('id',$id);
		return $this->db2->delete('partner');
	}

}