<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('partner_model');
		$this->db2 = $this->load->database('dbadmin',TRUE);
		
		
	}
	
	public function index()
	{
		$data['title'] = 'Partner - Travela';
		$data['partner'] = $this->partner_model->get()->result();
		$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/viewpartner');
		$this->load->view('template/footeradmin');
	}

	public function add()
	{
		$data['title'] = 'Partner - Travela';
		
		$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/addpartner');
		$this->load->view('template/footeradmin');
	}

	public function aksi_add(){
		$nama = $this->input->post('nama');
		$keterangan = $this->input->post('keterangan');
		$biayatransaksi = $this->input->post('biayatransaksi');
		$post           = $_FILES['thumbnail'];
	    $file_data 		= array(
	    						"name"     => $post['name'], 
	                            "type"      => $post['type'], 
	                            "tmp_name"  => $post['tmp_name'], 
	                            "error"     => $post['error'], 
	                            "size"      => $post['size']
	                        );

	    $dataUpload     = $file_data;
	    $nama_filenya   = "Partner"."-".time()."-". str_replace(" ", "_", $post['name']);
	                        
	    //UNTUK UPLOADNYA
	    move_uploaded_file( $post['tmp_name'], 'assets/img/partner/'.$nama_filenya);
		$data  = array(
			'nama' => $nama,
			'keterangan' => $keterangan,
			'gambar' =>$nama_filenya ,
			'biaya_transaksi' => $biayatransaksi,
			 );
		$insert = $this->partner_model->addpartner($data);
		if ($insert) {
			$this->session->set_flashdata('flash', 'Ditambahkan');
			redirect('partner/index');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('partner/add');
		}
	}

	public function edit($id){
		$data['title'] = 'Edit Partner - Travela';
		$data['partner'] = $this->partner_model->getpartnerbyid($id)->row();
		$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/editpartner',$data);
		$this->load->view('template/footeradmin');

	}
	public function aksi_edit(){
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$keterangan = $this->input->post('keterangan');
		$biayatransaksi = $this->input->post('biayatransaksi');
		$gambarlama = $this->input->post('thumbnail2');
		$post           = $_FILES['thumbnail'];
		//var_dump($id);die();
	    $file_data 		= array(
	    						"name"     => $post['name'], 
	                            "type"      => $post['type'], 
	                            "tmp_name"  => $post['tmp_name'], 
	                            "error"     => $post['error'], 
	                            "size"      => $post['size']
	                        );

	    $dataUpload     = $file_data;
	    $nama_filenya   = "Partner"."-".time()."-". str_replace(" ", "_", $post['name']);
	   	$upload =  move_uploaded_file( $post['tmp_name'], 'assets/img/partner/'.$nama_filenya);

	    if ($upload) {
			$gambar =  $nama_filenya;
			unlink('assets/img/partner/' . $gambarlama);
			
		}else{
			$gambar = $this->input->post('thumbnail2');			
		}
	             

	    //UNTUK UPLOADNYA
	    
		$data  = array(
			'nama' => $nama,
			'keterangan' => $keterangan,
			'gambar' =>$gambar,
			'biaya_transaksi' => $biayatransaksi,
			 );
		$edit = $this->partner_model->editpartner($id,$data);
		if ($edit) {
			$this->session->set_flashdata('flash', 'Diubah');
			redirect('partner/index');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('partner/edit/'.$id);
		}
	}

	public function delete($id){
		$data['partner'] = $this->partner_model->getpartnerbyid($id)->row();
		$delete= $this->partner_model->deletepartner($id);

		if ($delete) {
			unlink('assets/img/partner/'.$data['partner']->gambar);
			$this->session->set_flashdata('flash', 'Dihapus');
			redirect('partner/index');
		} else {
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('partner/index');
		}
	}

}
