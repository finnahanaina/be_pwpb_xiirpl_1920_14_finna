<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	private $db2;
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Admin_model');
		$this->load->model('partner_model');
		$this->db2 = $this->load->database('dbadmin',TRUE);
		
		
	}

	public function index()
	{		
		$data['title'] = 'Dashboard Admin - Travela';
		$data['jumlah_transportasi'] = $this->db2->get('transportasi')->num_rows();
		$data['jumlah_rute'] = $this->db2->get('rute')->num_rows();
		$data['jumlah_penumpang'] = $this->db2->get('penumpang')->num_rows();
		$data['jumlah_partner'] = $this->partner_model->get()->num_rows();
		$data['partner'] = $this->partner_model->get()->result_array();

		//chart
		$data['januari'] = $this->Admin_model->view_by_month_bulan('01','2019')->num_rows();
		$data['februari'] = $this->Admin_model->view_by_month_bulan('02','2019')->num_rows();
		$data['maret'] = $this->Admin_model->view_by_month_bulan('03','2019')->num_rows();
		$data['april'] = $this->Admin_model->view_by_month_bulan('04','2019')->num_rows();
		$data['mei'] = $this->Admin_model->view_by_month_bulan('05','2019')->num_rows();
		$data['juni'] = $this->Admin_model->view_by_month_bulan('06','2019')->num_rows();
		$data['juli'] = $this->Admin_model->view_by_month_bulan('07','2019')->num_rows();
		$data['agustus'] = $this->Admin_model->view_by_month_bulan('08','2019')->num_rows();
		$data['september'] = $this->Admin_model->view_by_month_bulan('09','2019')->num_rows();
		$data['oktober'] = $this->Admin_model->view_by_month_bulan('10','2019')->num_rows();
		$data['november'] = $this->Admin_model->view_by_month_bulan('11','2019')->num_rows();
		$data['desember'] = $this->Admin_model->view_by_month_bulan('12','2019')->num_rows();
		$data['pesawat'] = $this->Admin_model->getpemesananbytype('pesawat')->num_rows();
		$data['kereta'] = $this->Admin_model->getpemesananbytype('kereta')->num_rows();
		///var_dump($data['pesawat']);die();
	 	$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/dashboard',$data);
		$this->load->view('template/footeradmin');
	}

	public function search(){
    // Ambil data NIS yang dikirim via ajax post
    $keyword = $this->input->post('keyword');
    $data['searchtransportasi'] = $this->Admin_model->search($keyword);    
    // Kita load file view.php sambil mengirim data siswa hasil query function search di SiswaModel
    $hasil = $this->load->view('admin/transportasi',$data);
    
    // Buat sebuah array
    $callback = array(
      'hasil' => $hasil, // Set array hasil dengan isi dari view.php yang diload tadi
    );
    echo json_encode($callback); // konversi varibael $callback menjadi JSON
  }


	public function transportasi()
	{
		$data['level'] = "Admin";
		$data['statushalaman'] = "Transportasi";
		$data['transportasi'] = $this->Admin_model->tampiltransportasi();
		//var_dump($data['transportasi']);die();
		$this->load->view('template/headeradmin');
		$this->load->view('admin/transportasi',$data);
		$this->load->view('template/footeradmin');
		
	}

	function formtambahtransportasi(){
		$data['tipe'] = $this->db2->get('tipe_transportasi')->result_array();
		//var_dump($data['tipe']);die();
		$this->form_validation->set_rules('tipe', 'Tipe', 'required|trim',
			['required' => 'kolom Tipe Harus Diisi!']);
		$this->form_validation->set_rules('kode', 'Kode', 'required|trim',
		['required' => 'kolom Kode Harus Diisi!']);
		$this->form_validation->set_rules('kursi_ekonomi', 'Kursi_ekonomi', 'required|trim',
			['required' => 'kolom Jumlah kursi Ekonomi Harus Diisi!']);
		$this->form_validation->set_rules('kursi_bisnis', 'Kursi_bisnis', 'required|trim',
			['required' => 'kolom Jumlah kursi Bisnis Harus Diisi!']);
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required|trim',
			['required' => 'kolom Keterangan Harus Diisi!']);

		$data['partner'] = $this->partner_model->get_partner_transportasi('transportasi')->result_array();

		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';			
			$this->load->view('template/headeradmin',$data);
			$this->load->view('admin/inputtransportasi',$data);
			$this->load->view('template/footeradmin',$data);
		} else {
			$tipe = $this->input->post('tipe');
			$kode = $this->input->post('kode');
			$kursi_ekonomi = $this->input->post('kursi_ekonomi');
			$kursi_bisnis = $this->input->post('kursi_bisnis');
			$keterangan = $this->input->post('keterangan');
			 $data = array(
		   			'id_tipe_transportasi'=>$tipe,
		   			'kode_transportasi'=>$kode,
		   			'jumlah_kursi'=>$kursi_ekonomi + $kursi_bisnis,
		   			'kursi_ekonomi'=>$kursi_ekonomi,
		   			'kursi_bisnis'=>$kursi_bisnis,
		   			'keterangan'=>$keterangan,

		 );
			
			$id_transportasi = $this->Admin_model->inputdatatransportasi($data,'transportasi');

		    $bagian_kursi = $this->input->post('bagian_kursi');
		    $jumlah_kursi = $this->input->post('jumlah_kursi');
		    $index=0;
		    //var_dump($bagian_kursi);die();
		    //var_dump((int)$jumlah_kursi[0]);die();
		    $intjumlahkursi=(int)$jumlah_kursi[$index];
			    foreach ($bagian_kursi as $key) {
			    	for($i=1;$i<=$intjumlahkursi;$i++):
			    		//echo $i;
			    		$datakursi = array(
			    			'id_transportasi' =>$id_transportasi ,
			    			'bagian' => $bagian_kursi[$index],
			    			'kursi' => $i
			    			 );
			    		//var_dump($datakursi);die();
			    		$this->db2->insert('kursi',$datakursi);
			    	endfor;
			    $index++;
			    }
//die();

		    $this->session->set_flashdata('flash', 'Ditambahkan');

	        redirect('admin/transportasi');       

			}
		
	}

	public function kursitransportasi($id){

		$data['level'] = "Admin";
		$data['statushalaman'] = "Pengelolaan Kursi";
		$data['id_transportasi_kursi'] = $id;
		$data['kursi'] = $this->Admin_model->getkursi($id)->result_array();
		//var_dump($data['kursi']);die();
		$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/kursitransportasi',$data);
		$this->load->view('template/footeradmin',$data);


	}

	public function tambahkursi($id){
		$bagian=$this->input->post('bagian');
		$kursi=$this->input->post('kursi');
		for($i=1;$i<=$kursi;$i++):
    		//echo $i;
    		$datakursi = array(
    			'id_transportasi' =>$id ,
    			'bagian' => $bagian,
    			'kursi' => $i
    			 );
    		//var_dump($datakursi);die();
    		$insert = $this->Admin_model->insertkursi($datakursi);
    	endfor;
		
		
		if ($insert) {
			 $this->session->set_flashdata('flash', 'Ditambah');
	        redirect('admin/kursitransportasi/'.$id); 
		} else {
			$this->session->set_flashdata('flash', 'Gagal Ditambah');
	        redirect('admin/kursitransportasi/'.$id); 
		}
	}

	public function hapuskursi($id,$id_transportasi){
		$hapus = $this->Admin_model->deletekursi($id);
		if ($hapus) {
			 $this->session->set_flashdata('flash', 'Dihapus');
	        redirect('admin/kursitransportasi/'.$id_transportasi); 
		} else {
			$this->session->set_flashdata('flash', 'Gagal Dihapus');
	        redirect('admin/kursitransportasi/'.$id_transportasi); 
		}
	}



	public function edittransportasi($id) {  
     	    
     		$tipe =$this->uri->segment(4);
	    	$where = array('id_transportasi' => $id);
			$data['transportasi'] = $this->Admin_model->editdatatransportasi('transportasi',$where)->result_array();
			$data['tipe'] =$this->db2->get_where('tipe_transportasi',['id_tipe_transportasi' => $tipe])->result_array();
			$data['tipetransportasi'] = $this->db2->get('tipe_transportasi')->result_array();
			//var_dump($data['transportasi']);die();
	    	$this->load->view('template/headeradmin',$data);
			$this->load->view('admin/edittransportasi',$data);
			$this->load->view('template/footeradmin',$data);
	        
	       
    }

     public function updatetransportasi(){
    	
		$id = $this->input->post('id_transportasi');
		$tipe = $this->input->post('tipe');
		$kode = $this->input->post('kode');
		$kursi_ekonomi = $this->input->post('kursi_ekonomi');
		$kursi_bisnis = $this->input->post('kursi_bisnis');
		

		// $tipe = "2";
		//var_dump($tipe);die();
		$data = array(
			
			'id_tipe_transportasi'=>$tipe,
			'kode_transportasi'=>$kode,
			'jumlah_kursi'=>$kursi_ekonomi + $kursi_bisnis,
			'kursi_ekonomi'=>$kursi_ekonomi,
			'kursi_bisnis'=>$kursi_bisnis,
			
		);	 
		$where = array(
			'id_transportasi' => $id
		);	 
		$status = $this->Admin_model->updatedatatransportasi($where,$data,'transportasi');
		//var_dump($status);die();
		//var_dump($data);var_dump($where);die();
		if(!$status){
			$this->session->set_flashdata('flash', 'gagal');
			redirect('admin/edittransportasi'.'/'. $id.'/'.$tipe);
		}else{
			$this->session->set_flashdata('flash', 'Diubah');
			redirect('admin/transportasi');
			
		}
		
		
		
	}

	 function hapustransportasi($id){
		$hapus = $this->Admin_model->hapustransportasi($id);
		if ($hapus) {
			$this->Admin_model->deletekursibytransportasi($id);
			$this->session->set_flashdata('flash', 'Dihapus');
			redirect('admin/transportasi');
		}else{
			$this->session->set_flashdata('flash', 'Gagal');
			redirect('admin/transportasi');
		}
		
	}


	public function rute()
	{
		$data['level'] = "Admin";
		$data['statushalaman'] = "Rute";
		$data['rute'] = $this->Admin_model->tampilrute();
		//var_dump($data['transportasi']);die();
		$this->load->view('template/headeradmin',$data);
		$this->load->view('admin/rute',$data);
		$this->load->view('template/footeradmin',$data);
	}

	 


	 public function editrute($id) {  
     	    
     		$transportasi =$this->uri->segment(4);
	    	$where = array('id_rute' => $id);
	    	$whereekonomi=array(
	    		'id_rute'=> $id,
	    		'nama_kelas'=>'ekonomi'
	    	);
	    	$wherebisnis=array(
	    		'id_rute'=> $id,
	    		'nama_kelas'=>'bisnis'
	    	);
			$data['rute'] = $this->Admin_model->editdatarute('rute',$where)->result_array();
			$data['transportasi'] =$this->db2->get_where('transportasi',['id_transportasi' => $transportasi])->result_array();
			$data['transportasiget'] = $this->db2->get('transportasi')->result_array();
			$data['hargaekonomi'] =$this->db2->get_where('harga_per_kelas',$whereekonomi)->row_array();
			$data['hargabisnis'] =$this->db2->get_where('harga_per_kelas',$wherebisnis)->row_array();
			//var_dump($data['hargaekonomi']);die();	    	
	        $this->load->view('template/headeradmin',$data);
			$this->load->view('admin/editrute',$data);
			$this->load->view('template/footeradmin',$data);
	       
    }

     public function updaterute(){
    	
		$id = $this->input->post('idrute');
		$kode = $this->input->post('kode');
		$awal = $this->input->post('awal');
		$akhir = $this->input->post('akhir');
		$idtransportasi = $this->input->post('idtransportasi');	
		$tgl = $this->input->post('tgl');
		$jamberangkat = $this->input->post('berangkat');	
		$jamcekin = $this->input->post('cekin');

		//untuk table harga_per_kelas
		$ekonomi = $this->input->post('ekonomi');
		$bisnis = $this->input->post('bisnis');
		

		// $tipe = "2";
		//var_dump($tipe);die();
		$data = array(
			
			'id_transportasi'=>$idtransportasi,
			'kode_rute'=>$kode,
			'tujuan' =>$awal,
			'rute_awal'=>$awal,
			'rute_akhir'=>$akhir,
			'tgl_berangkat'=>$tgl,
			'jam_berangkat'=>$jamberangkat,
			'jam_cekin'=>$jamcekin,
					
		);	 
		//var_dump($data);die();
		$where = array(
			'id_rute' => $id
		);	 
		//var_dump($data);var_dump($where);die();
		$this->Admin_model->updatedatarute($where,$data,'rute');

		$hargaekonomi = array(
			'harga'=>$ekonomi
		);
		$whereekonomi= array(
			'id_rute' => $id,
			'nama_kelas' => 'ekonomi'
		);
		$this->Admin_model->updatedataekonomi($whereekonomi,$hargaekonomi,'harga_per_kelas');		

		$hargabisnis = array(
			'harga'=>$bisnis
		);
		$wherebisnis= array(
			'id_rute' => $id,
			'nama_kelas' => 'bisnis'
		);
		$this->Admin_model->updatedatabisnis($wherebisnis,$hargabisnis,'harga_per_kelas');
		$this->session->set_flashdata('flash', 'Diubah');
			redirect('admin/rute');
		
	}

	

	function formtambahrute(){
		$data['transportasi'] = $this->db2->get('transportasi')->result_array();
		//var_dump($data['tipe']);die();
		$this->form_validation->set_rules('koderute', 'Koderute', 'required|trim',
			['required' => 'kolom Kode Rute Harus Diisi!']);
		$this->form_validation->set_rules('transportasi', 'Transportasi', 'required|trim',
			['required' => 'kolom Kode Transportasi Harus Diisi!']);
		$this->form_validation->set_rules('awal', 'Awal', 'required|trim',
			['required' => 'kolom Rute awal Harus Diisi!']);
		$this->form_validation->set_rules('akhir', 'Akhir', 'required|trim',
			['required' => 'kolom Rute Akhir Harus Diisi!']);
		$this->form_validation->set_rules('ekonomi', 'Ekonomi', 'required|trim',
			['required' => 'kolom Harga Ekonomi Harus Diisi!']);
		$this->form_validation->set_rules('bisnis', 'Bisnis', 'required|trim',
			['required' => 'kolom Harga Bisnis Harus Diisi!']);
		$this->form_validation->set_rules('tglberangkat', 'Tglberangkat', 'required|trim',
			['required' => 'kolom Tanggal Berangkat Harus Diisi!']);
		$this->form_validation->set_rules('jamberangkat', 'Jamberangkat', 'required|trim',
			['required' => 'kolom Jam Berangkat Harus Diisi!']);
		$this->form_validation->set_rules('jamcekin', 'jamcekin', 'required|trim',
			['required' => 'kolom Jam Cekin Harus Diisi!']);
		$data['transportasiget'] = $this->db2->get('transportasi')->result_array();
		
		
		
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';			
			$this->load->view('template/headeradmin',$data);
			$this->load->view('admin/inputrute',$data);
			$this->load->view('template/footeradmin',$data);
		} else {
			$kode= $this->input->post('koderute');
			$transportasi = $this->input->post('transportasi');
			$awal = $this->input->post('awal');
			$akhir = $this->input->post('akhir');
			$ekonomi = $this->input->post('ekonomi');
			$bisnis = $this->input->post('bisnis');
			$tglberangkat = $this->input->post('tglberangkat');
			$jamberangkat = $this->input->post('jamberangkat');
			$jamcekin = $this->input->post('jamcekin');
			
			 $data = array(
			 		'id_transportasi'=>$transportasi,
		   			'kode_rute'=>$kode,
		   			'tujuan'=>$akhir,
		   			'rute_awal'=>$awal,
		   			'rute_akhir'=>$akhir,
		   			'tgl_berangkat'=>$tglberangkat,
		   			'jam_berangkat'=>$jamberangkat,
		   			'jam_cekin'=>$jamcekin
				 );
			 $id_rute=$this->Admin_model->inputdatarute($data);
			 	//var_dump($id_rute);die();

			 $dataekonomi = array(
			 		'id_rute'=>$id_rute,
				 	'nama_kelas'=>'ekonomi',
			   		'harga'=>$ekonomi
			   		
			 );
			 $this->Admin_model->inputdataharga($dataekonomi,'harga_per_kelas');

			 $databisnis = array(
			 		'id_rute'=>$id_rute,
				 	'nama_kelas'=>'bisnis',
			   		'harga'=>$ekonomi			   		
			 );
			 $this->Admin_model->inputdataharga($databisnis,'harga_per_kelas');

			 $this->session->set_flashdata('flash', 'Ditambahkan');

	        redirect('admin/rute');       

			}
		
	}

	
	 function hapusrute($id){
		$this->Admin_model->hapusrute($id);
		$this->session->set_flashdata('flash', 'Dihapus');
		redirect('admin/rute');
	}

	function verivikasi(){
		$data['verivikasi'] = $this->Admin_model->getverivikasi();
		//var_dump($data['verivikasi'] );die();
		$this->load->view('template/headeradmin');
		$this->load->view('admin/verivikasi',$data);
		$this->load->view('template/footeradmin');
	}

	function updatepemesanan($id){
		$where = array (
			'id_pemesanan' => $id,
		);
		$petugas = $this->session->userdata('id_petugas');
		$data= array(
			'status' => 'verived',
			'id_petugas' => $petugas,
		);
		//var_dump($data);die();
		$this->Admin_model->updatepemesanan($where,$data,'pemesanan');
		$this->session->set_flashdata('flash', 'Diubah');
		redirect('admin/verivikasi');
	}
	function laporan(){
		
		$this->load->view('template/headeradmin');
		$this->load->view('admin/laporan');
		$this->load->view('template/footeradmin');
	}
	function laporankeberangkatan(){
		$data['laporankeberangkatan'] = $this->Admin_model->getlaporankeberangkatan()->result_array();
		//var_dump($data['laporankeberangkatan']);die();
		$this->load->view('template/headeradmin');
		$this->load->view('admin/laporankeberangkatan',$data);
		$this->load->view('template/footeradmin');
	}
	function penumpanghariini(){
		$data['penumpanghariini'] = $this->Admin_model->getpenumpanghariini()->result_array();
		//var_dump($data['penumpanghariini']);die();
		$this->load->view('template/headeradmin');
		$this->load->view('admin/listpenumpang',$data);
		$this->load->view('template/footeradmin');
	}
	function pendapatan(){
		$data['pendapatan'] = $this->Admin_model->getpendapatan()->result_array();
		//var_dump($data['pendapatan']);die();
		$this->load->view('template/headeradmin');
		$this->load->view('admin/pendapatanperbulan',$data);
		$this->load->view('template/footeradmin');
	}
	function cetaklaporanpendapatan(){
		$data['pendapatan'] = $this->Admin_model->getpendapatan()->result_array();
		$data['total_bayar'] = $this->db2->query('SELECT Sum(total_bayar) AS total FROM pemesanan')->result_array();
		//var_dump($data['total_bayar']);die();

		$this->load->view('admin/cetaklaporanpendapatan',$data);
			
	}

	public function cetaklaporankeberangkatan(){
		$data['laporankeberangkatan'] = $this->Admin_model->getlaporankeberangkatan()->result_array();
		$this->load->view('admin/cetaklaporankeberangkatan',$data);
		
	}
	public function cetakpenumpanghariini(){
		$data['penumpanghariini'] = $this->Admin_model->getpenumpanghariini()->result_array();
		
		$this->load->view('admin/cetakpenumpanghariini',$data);
		
	}




	public function logout(){
		$this->session->sess_destroy();
		redirect('auth/index');
	}

}

