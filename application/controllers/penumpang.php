<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penumpang extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('penumpang_model');
		$this->load->helper('string');

	}

	public function index()
	{	
		//$this->load->view('template/headerpenumpang');
		$this->load->view('penumpang/index');
		$this->load->view('template/footerpenumpang');
	}
	public function rutepesawat()
	{
		$data['title'] ="Travela";

		$this->form_validation->set_rules('rute_awal', 'Rute_awal', 'required|trim');
		$this->form_validation->set_rules('rute_akhir', 'Rute_akhir', 'required|trim');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required|trim');
		$this->form_validation->set_rules('nama_kelas', 'Password', 'required|trim');
		$this->form_validation->set_rules('jmldewasa', 'Jmldewasa', 'required|trim');
		$this->form_validation->set_rules('jmlanak', 'Jmlanak', 'required|trim');
		//$this->form_validation->set_rules('jmlbayi', 'Jmlbayi', 'required|trim');
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';			
			redirect('penumpang/index');
		} else {
			$tipe =1;
			$rute_awal=$this->input->post('rute_awal');
			$rute_akhir=$this->input->post('rute_akhir');
			$tanggal=$this->input->post('tanggal');
			$nama_kelas=$this->input->post('nama_kelas');
			$jmldewasa=$this->input->post('jmldewasa');
			$jmlanak=$this->input->post('jmlanak');
			//$jmlbayi=$this->input->post('jmlbayi');
			$data['jmlpenumpang'] = $jmldewasa + $jmlanak;			
			//var_dump($data['jmlpenumpang']);die();


			$where = array(
				'id_tipe_transportasi' => $tipe,
		  		'rute_awal'=>$rute_awal,
		  		'rute_akhir'=>$rute_akhir,
		  		'tgl_berangkat'=>$tanggal,
		  		'nama_kelas'=>$nama_kelas
		  	);		
			$data['rutepesawat'] = $this->penumpang_model->tampilrutepesawat($where)->result_array();	
			$data['jumlahrutepesawat']=$this->penumpang_model->tampilrutepesawat($where)->num_rows();
			// var_dump($data['rutepesawat']);
			if( !$data['rutepesawat'] ){
				redirect('penumpang/rutepesawatnotfound');
			} else{
				//var_dump($data['rutepesawat']);
				$data['rute_awal']=$rute_awal;
				$data['rute_akhir']=$rute_akhir;
				$data['tanggal']=$tanggal;
				$data['nama_kelas']=$nama_kelas;
				$this->load->view('template/headerpenumpang');
				$this->load->view('penumpang/pilihrutepesawat',$data);
				$this->load->view('template/footerpenumpang');
			}
		}

		
	}

	public function inputdatapenumpang(){
		$data['idrute'] = $this->uri->segment(3);
		$data['idhargarute'] = $this->uri->segment(4);
		$data['jmlpenumpang'] = $this->uri->segment(5);
		$data['title'] = 'Travela';			
		$this->load->view('template/headerpenumpang');	
		$this->load->view('penumpang/inputdatapenumpang',$data);
		$this->load->view('template/footerpenumpang',$data);
	}

	public function pilihpembayaran(){

		$data['idrute'] = $this->input->post('idrute');
		$data['idhargarute'] = $this->input->post('idhargarute');
		$data['jmlpenumpang'] = $this->input->post('jmlpenumpang');
		//var_dump($data['jmlpenumpang']);die();
		$nama = $this->input->post('namalengkap');
		$nik  = $this->input->post('nik');
		//var_dump($nama);die();
	   
	    foreach($nama as $key =>$datanama){ 
	    	$datapenumpang[$key] = ["nama"=>$datanama, "nik"=>$nik[$key]]; 		   
	    }
	   // untuk halaman pilih pembayaran
	    $data['datapenumpang'] = $datapenumpang;
	    $data['pembayaran_instan'] = $this->penumpang_model->get_pembayaran_instan()->result_array();
		$data['gerai_retail'] = $this->penumpang_model->get_gerai_retail()->result_array();
	    $this->load->view('template/headerpenumpang',$data);	
		$this->load->view('penumpang/pilihpembayaran',$data);
		$this->load->view('template/footerpenumpang',$data);
		
		
	}

	public function pembayaran(){
		//informasi Rute
		$idrute=$this->input->post('idrute');
		$idhargarute=$this->input->post('idhargarute');
		$data['jmlpenumpang'] = $this->input->post('jmlpenumpang');
		$data['idrute']=$idrute;
		$data['idhargarute']=$idhargarute;
		//var_dump($data['jmlpenumpang']);die();
		$where=array(
			'id_rute'=>$idrute,
			'id_harga'=>$idhargarute
		);
		//informasi penumpang
		$nama=$this->input->post('nama');
		$nik=$this->input->post('nik');
		$metodepembayaran=$this->input->post('metode');
		$data['metodepembayaran']=$metodepembayaran;

		$data['pilihanmetode'] = $this->penumpang_model->get_pilihan_metode($metodepembayaran)->row();
		$data['inforute'] = $this->penumpang_model->get_info_rute($idrute,$idhargarute)->row();
		//var_dump($data['inforute']);die();

	    foreach($nama as $key => $datanama){ 
	    	$datapenumpang[$key] = ["nama"=>$datanama, "nik"=>$nik[$key]]; 		   
	    }
	    $data['datapenumpang'] = $datapenumpang;

		$this->load->view('template/headerpenumpang',$data);	
		$this->load->view('penumpang/pembayaran',$data);
		$this->load->view('template/footerpenumpang');
	}

	public function proses_pesan(){
		$idrute=$this->input->post('idrute');
		$idhargarute=$this->input->post('idhargarute');
		$data['jmlpenumpang'] = $this->input->post('jmlpenumpang');
		$totalbayar = $this->input->post('totalbayar');
		$data['idrute']=$idrute;
		$data['idhargarute']=$idhargarute;
		//var_dump($data['jmlpenumpang']);die();
		$where=array(
			'id_rute'=>$idrute,
			'id_harga'=>$idhargarute
		);
		//var_dump($where);die();
		//informasi penumpang
		$nama=$this->input->post('nama');
		$nik=$this->input->post('nik');
		$metodepembayaran=$this->input->post('metodepembayaran');
		$data['metodepembayaran']=$metodepembayaran;

		$data['pilihanmetode'] = $this->penumpang_model->get_pilihan_metode($metodepembayaran)->row();
		$data['inforute'] = $this->penumpang_model->get_info_rute($idrute,$idhargarute)->row();
		//var_dump($data['inforute']);die();
		 date_default_timezone_set('Asia/Jakarta');
          $timestamp = time();
          $jam=date('H')+2;
          $batas_pembayaran=date("M d, Y ".$jam.":i:s");

		$data1 = array(
			'id_penumpang' => $this->session->userdata('id_penumpang'),
			'id_petugas' => 2,
			'id_harga' => $idhargarute,
			'kode_pemesanan' => 'travela-'.strtolower(random_string('numeric', 10)),
			'tgl_pemesanan' => date('Y-m-d'),
			'tempat_pemesanan' => $data['inforute']->rute_awal,
			'total_bayar' => $totalbayar,
			'status' =>'unveriv',
			'bukti_pembayaran' =>'',
			'batas_pembayaran' =>$batas_pembayaran
			 );
		$id_pemesanan = $this->penumpang_model->insert_pemesanan($data1);
		$data['data_pemesanan'] = $this->penumpang_model->get_where_pemesanan($id_pemesanan)->row_array();

		// var_dump($data);die();
		//insert ke database
		$index=0;
		foreach ($nama as $key) {
			$data2 = array(
				'id_pemesanan' => $id_pemesanan,
				'id_penumpang' =>$this->session->userdata('id_penumpang'),
				'id_rute' =>$idrute,
				'nama' => $nama[$index],
				'nik' =>$nik[$index],
				'kode_kursi' => ''
			);
			$this->penumpang_model->insert_detail($data2);
			$index++;
		}


	    foreach($nama as $key => $datanama){ 
	    	$datapenumpang[$key] = ["nama"=>$datanama, "nik"=>$nik[$key]]; 		   
	    }
	    $data['datapenumpang'] = $datapenumpang;

		$this->load->view('template/headerpenumpang',$data);	
		$this->load->view('penumpang/kodepembayaran',$data);
		$this->load->view('template/footerpenumpang');
	}
		
	public function riwayat_pemesanan(){
		$data['data_pemesanan'] = $this->penumpang_model->get_where_pemesanan_by_user()->result();
		// /$data['detail_data_pemesanan'] = $this->penumpang_model->get_where_detail_pemesanan_by_user()->result();
		//var_dump($data['data_pemesanan'] );die();
		$this->load->view('template/headerpenumpang',$data);	
		$this->load->view('penumpang/riwayat_pemesanan',$data);
		$this->load->view('template/footerpenumpang');
	}

	public function detail_pemesanan($id){
		$data['detail_data_pemesanan'] = $this->penumpang_model->get_where_detail_pemesanan_by_user($id)->result();
		$id_transportasi=$data['detail_data_pemesanan'][0]->id_transportasi;
		$id_pemesanan=$data['detail_data_pemesanan'][0]->id_pemesanan;
		$where=array('id_pemesanan' => $id_pemesanan);
		$data['pemesanan']=$this->db->get_where('pemesanan',$where)->row();
		//var_dump($data['detail_data_pemesanan'][0]->id_pemesanan);die();
		//var_dump($data['pemesanan']);die();
		//var_dump($where);die();
		$data['kursi_a'] = $this->penumpang_model->get_where_kursi($id_transportasi,'A')->result();
		$data['kursi_b'] = $this->penumpang_model->get_where_kursi($id_transportasi,'B')->result();
		$data['kursi_c'] = $this->penumpang_model->get_where_kursi($id_transportasi,'C')->result();
		$data['kursi_d'] = $this->penumpang_model->get_where_kursi($id_transportasi,'D')->result();
		$data['kursi_e'] = $this->penumpang_model->get_where_kursi($id_transportasi,'E')->result();
		// var_dump($data['kursi_a'] );die();
		$this->load->view('template/headerpenumpang',$data);	
		$this->load->view('penumpang/detail_pemesanan',$data);
		$this->load->view('template/footerpenumpang');
	}
	public function konfirmasi_pembayaran(){
			$id=$this->input->post('id_pemesanan');
			//$where = array('id_pemesanan' =>$id);
				$post           = $_FILES['gambar'];
                $file_data 		= array(
                						"name"     => $post['name'], 
                                        "type"      => $post['type'], 
                                        "tmp_name"  => $post['tmp_name'], 
                                        "error"     => $post['error'], 
                                        "size"      => $post['size']
                                    );

                $dataUpload     = $file_data;
                $nama_filenya   = "bukti_pembayaran"."-".time()."-". str_replace(" ", "_", $post['name']);
                                    
                //UNTUK UPLOADNYA
                move_uploaded_file( $post['tmp_name'], 'assets/img/bukti_pembayaran/'.$nama_filenya);

	        $data = array('bukti_pembayaran' => $nama_filenya);
	        $update =$this->penumpang_model->konfirmasi_pembayaran($id,$data);
	        if ($update) {
	        	$this->session->set_flashdata('flash', 'Upload Berhasil');
	        	redirect('penumpang/riwayat_pemesanan');
	        	
	        } else {
	        	$this->session->set_flashdata('flash', 'Upload Gagal!');
	        	redirect('penumpang/riwayat_pemesanan');
	        }
		}

	public function ubahkursi($idpemesanan,$id){
		$bagian = $this->input->post('bagian');
		$kursi = $this->input->post('kode_kursi');
		//var_dump($kursi);die();
		$data = array(
			'kode_kursi' => $kursi,
			 );
		//var_dump($id);die();
		$update = $this->penumpang_model->updatekursi($id,$data);
		if ($update) {
	        	$this->session->set_flashdata('flash', 'Ubah Berhasil');
	        	redirect('penumpang/detail_pemesanan/'.$idpemesanan);
	        	
	        } else {
	        	$this->session->set_flashdata('flash', 'Ubah Gagal!');
	        	redirect('penumpang/detail_pemesanan/'.$idpemesanan);
	        }

	}
	
	public function akun()
	{
		$id = $this->session->userdata('id_penumpang');
		$where = array(
			'id_penumpang' => $id			
		);
		//var_dump($where);die();		
		$data['akun'] = $this->penumpang_model->tampilakun($where)->row_array();
		$this->load->view('template/headerpenumpang',$data);
		$this->load->view('penumpang/akun',$data);
		$this->load->view('template/footerpenumpang',$data);
	}
	public function updateakun(){
		$id =$this->input->post('id_penumpang');
		$nama = $this->input->post('nama');
		$tgl_lahir = $this->input->post('tgl_lahir');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('no_telp');
		$jenis_kelamin = $this->input->post('jenis_kelamin');

		$where = array(
			'id_penumpang' =>$id , 
		);
		$data = array(
			'nama_penumpang' => $nama,
			'tanggal_lahir' => $tgl_lahir,
			'alamat_penumpang' => $alamat,
			'telephone' => $telp,
			'jenis_kelamin' => $jenis_kelamin
			 );
		$this->penumpang_model->updatedataakun($where,$data);
		$this->session->set_flashdata('flash', 'DiUpdate');
		redirect('penumpang/akun');
	}

	public function petunjuk(){
		$this->load->view('template/headerpenumpang');	
		$this->load->view('penumpang/petunjuk');
		$this->load->view('template/footerpenumpang');
	}
	
	
	public function kodepembayaran(){
		$this->load->view('template/headerpenumpang');	
		$this->load->view('penumpang/kodepembayaran');
		$this->load->view('template/footerpenumpang');
	}

	public function kereta(){
		$data['title'] ="Travela";

		$this->form_validation->set_rules('rute_awal', 'Rute_awal', 'required|trim');
		$this->form_validation->set_rules('rute_akhir', 'Rute_akhir', 'required|trim');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required|trim');
		$this->form_validation->set_rules('nama_kelas', 'Password', 'required|trim');
		$this->form_validation->set_rules('jmldewasa', 'Jmldewasa', 'required|trim');
		$this->form_validation->set_rules('jmlanak', 'Jmlanak', 'required|trim');
		//$this->form_validation->set_rules('jmlbayi', 'Jmlbayi', 'required|trim');
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';		
			$this->load->view('template/headerpenumpang');		
			$this->load->view('penumpang/kereta');
			$this->load->view('template/footerpenumpang');
		} else {
			$tipe =2;
			$rute_awal=$this->input->post('rute_awal');
			$rute_akhir=$this->input->post('rute_akhir');
			$tanggal=$this->input->post('tanggal');
			$nama_kelas=$this->input->post('nama_kelas');
			$jmldewasa=$this->input->post('jmldewasa');
			$jmlanak=$this->input->post('jmlanak');
			//$jmlbayi=$this->input->post('jmlbayi');
			$data['jmlpenumpang'] = $jmldewasa + $jmlanak;			
			//var_dump($data['jmlpenumpang']);die();


			$where = array(
				'id_tipe_transportasi' => $tipe,
		  		'rute_awal'=>$rute_awal,
		  		'rute_akhir'=>$rute_akhir,
		  		'tgl_berangkat'=>$tanggal,
		  		'nama_kelas'=>$nama_kelas
		  	);		
			$data['rutekereta'] = $this->penumpang_model->tampilrutekereta($where)->result_array();
			$data['jumlahrutekereta']=$this->penumpang_model->tampilrutekereta($where)->num_rows();
			// var_dump($data['rutekereta']);
			if( !$data['rutekereta'] ){
				redirect('penumpang/rutekeretanotfound');
			} else{
				//var_dump($data['rutepesawat']);
				$data['rute_awal']=$rute_awal;
				$data['rute_akhir']=$rute_akhir;
				$data['tanggal']=$tanggal;
				$data['nama_kelas']=$nama_kelas;

				$this->load->view('template/headerpenumpang',$data);
				$this->load->view('penumpang/pilihrutekereta',$data);
				$this->load->view('template/footerpenumpang');
			}
		}
	}
	public function pesawat(){
		$data['title'] ="Travela";

		$this->form_validation->set_rules('rute_awal', 'Rute_awal', 'required|trim');
		$this->form_validation->set_rules('rute_akhir', 'Rute_akhir', 'required|trim');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required|trim');
		$this->form_validation->set_rules('nama_kelas', 'Password', 'required|trim');
		$this->form_validation->set_rules('jmldewasa', 'Jmldewasa', 'required|trim');
		$this->form_validation->set_rules('jmlanak', 'Jmlanak', 'required|trim');
		//$this->form_validation->set_rules('jmlbayi', 'Jmlbayi', 'required|trim');
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';	
			$this->load->view('template/headerpenumpang');		
			$this->load->view('penumpang/pesawat');
			$this->load->view('template/footerpenumpang');
		} else {
			$tipe =1;
			$rute_awal=$this->input->post('rute_awal');
			$rute_akhir=$this->input->post('rute_akhir');
			$tanggal=$this->input->post('tanggal');
			$nama_kelas=$this->input->post('nama_kelas');
			$jmldewasa=$this->input->post('jmldewasa');
			$jmlanak=$this->input->post('jmlanak');
			//$jmlbayi=$this->input->post('jmlbayi');
			$data['jmlpenumpang'] = $jmldewasa + $jmlanak;			
			//var_dump($data['jmlpenumpang']);die();


			$where = array(
				'id_tipe_transportasi' => $tipe,
		  		'rute_awal'=>$rute_awal,
		  		'rute_akhir'=>$rute_akhir,
		  		'tgl_berangkat'=>$tanggal,
		  		'nama_kelas'=>$nama_kelas
		  	);		
			$data['rutepesawat'] = $this->penumpang_model->tampilrutepesawat($where)->result_array();
			$data['jumlahrutepesawat']=$this->penumpang_model->tampilrutepesawat($where)->num_rows();
			// var_dump($data['rutepesawat']);
			if( !$data['rutepesawat'] ){
				redirect('penumpang/rutepesawatnotfound');
			} else{
				//var_dump($data['rutepesawat']);
				$data['rute_awal']=$rute_awal;
				$data['rute_akhir']=$rute_akhir;
				$data['tanggal']=$tanggal;
				$data['nama_kelas']=$nama_kelas;

				$this->load->view('template/headerpenumpang',$data);
				$this->load->view('penumpang/pilihrutepesawat',$data);
				$this->load->view('template/footerpenumpang');
			}
		}
	}

	public function cetaktiket($id_pemesanan,$id_detail_pemesanan){
		$data['cetaktiket'] = $this->penumpang_model->get_where_detail_pemesanan_by_user_for_cetak($id_pemesanan,$id_detail_pemesanan)->row();
		//var_dump($data['cetaktiket']);die();
		$this->load->view('penumpang/cetaktiket',$data);
	}

	public function rutepesawatnotfound(){
		$data['title'] = 'Rute Tidak Ditemukan';
		$this->load->view('template/headerpenumpang',$data);
		$this->load->view('penumpang/rutepesawatnotfound',$data);
		$this->load->view('template/footerpenumpang');
	}
	public function rutekeretanotfound(){
		$data['title'] = 'Rute Tidak Ditemukan';
		$this->load->view('template/headerpenumpang',$data);
		$this->load->view('penumpang/rutekeretanotfound',$data);
		$this->load->view('template/footerpenumpang');
	}

public function logout(){
		$this->session->sess_destroy();
		redirect('auth/index');
	}

}
