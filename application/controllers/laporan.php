<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){
		parent::__construct();

		$this->load->model('laporan_model');
	}

	public function index(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Data Pendapatan Tanggal '.date('d-m-y', strtotime($tgl));
                $url_cetak = 'laporan/cetak?filter=1&tanggal='.$tgl;
                $url_excel = 'laporan/excel?filter=1&tanggal='.$tgl;
                $laporan = $this->laporan_model->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Data Pendapatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_cetak = 'laporan/cetak?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $url_excel = 'laporan/excel?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Data Pendapatan Tahun '.$tahun;
                $url_cetak = 'laporan/cetak?filter=3&tahun='.$tahun;
                $url_excel = 'laporan/excel?filter=3&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Data Pendapatan';
            $url_cetak = 'laporan/cetak';
            $url_excel = 'laporan/excel';
            $laporan = $this->laporan_model->view_all(); // Panggil fungsi view_all yang ada di laporan_model
        }

		$data['ket'] = $ket;
		$data['url_cetak'] = base_url($url_cetak);
        $data['url_excel'] = base_url($url_excel);
		$data['laporan'] = $laporan;
        $data['option_tahun'] = $this->laporan_model->option_tahun();
		$this->load->view('template/headeradmin',$data);
        $this->load->view('admin/laporanpendapatan',$data);
        $this->load->view('template/footeradmin');
	}

	public function cetak(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Data Pendapatan Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Data Pendapatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Data Pendapatan Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Data Pendapatan';
            $laporan = $this->laporan_model->view_all(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

		
		$this->load->view('admin/cetaklaporan', $data);
		
	}

    public function excel(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Data Pendapatan Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date($tgl); // Panggil fungsi view_by_date yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Data Pendapatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month($bulan, $tahun); // Panggil fungsi view_by_month yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Data Pendapatan Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year($tahun); // Panggil fungsi view_by_year yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Data Pendapatan';
            $laporan = $this->laporan_model->view_all(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('admin/excellaporan', $data);
        
    }



    public function indexlaporankeberangkatan(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan Keberangkatan Tanggal '.date('d-m-y', strtotime($tgl));
                $url_cetak = 'laporan/cetaklaporankeberangkatan?filter=1&tanggal='.$tgl;
                $url_excel = 'laporan/excellaporankeberangkatan?filter=1&tanggal='.$tgl;
                $laporan = $this->laporan_model->view_by_date_keberangkatan($tgl); // Panggil fungsi view_by_date_keberangkatan yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan Keberangkatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_cetak = 'laporan/cetaklaporankeberangkatan?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $url_excel = 'laporan/excellaporankeberangkatan?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_month_keberangkatan($bulan, $tahun); // Panggil fungsi view_by_mont_keberangkatanh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan Keberangkatan Tahun '.$tahun;
                $url_cetak = 'laporan/cetaklaporankeberangkatan?filter=3&tahun='.$tahun;
                $url_excel = 'laporan/excellaporankeberangkatan?filter=3&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_year_keberangkatan($tahun); // Panggil fungsi view_by_year_keberangkatan yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan Keberangkatan';
            $url_cetak = 'laporan/cetaklaporankeberangkatan';
            $url_excel = 'laporan/excellaporankeberangkatan';
            $laporan = $this->laporan_model->view_all_keberangkatan(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['url_cetak'] = base_url($url_cetak);
        $data['url_excel'] = base_url($url_excel);
        $data['laporan'] = $laporan;
        //var_dump($laporan);die();
        $data['option_tahun'] = $this->laporan_model->option_tahun();
        $this->load->view('template/headeradmin',$data);
        $this->load->view('admin/laporankeberangkatan',$data);
        $this->load->view('template/footeradmin');
    }

    public function cetaklaporankeberangkatan(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan Keberangkatan Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_keberangkatan($tgl); // Panggil fungsi view_by_date_keberangkatan yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan Keberangkatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_keberangkatan($bulan, $tahun); // Panggil fungsi view_by_mont_keberangkatanh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan Keberangkatan Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_keberangkatan($tahun); // Panggil fungsi view_by_year_keberangkatan yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan Keberangkatan';
            $laporan = $this->laporan_model->view_all_keberangkatan(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('admin/cetaklaporankeberangkatan', $data);
        
    }
    public function excellaporankeberangkatan(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan Keberangkatan Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_keberangkatan($tgl); // Panggil fungsi view_by_date_keberangkatan yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan Keberangkatan Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_keberangkatan($bulan, $tahun); // Panggil fungsi view_by_mont_keberangkatanh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan Keberangkatan Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_keberangkatan($tahun); // Panggil fungsi view_by_year_keberangkatan yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan Keberangkatan';
            $laporan = $this->laporan_model->view_all_keberangkatan(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('admin/excellaporankeberangkatan', $data);
        
    }

    //Laporan Penumpang Hari ini
     public function indexlaporanpenumpang(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan penumpang Tanggal '.date('d-m-y', strtotime($tgl));
                $url_cetak = 'laporan/cetaklaporanpenumpang?filter=1&tanggal='.$tgl;
                $url_excel = 'laporan/excellaporanpenumpang?filter=1&tanggal='.$tgl;
                $laporan = $this->laporan_model->view_by_date_penumpang($tgl); // Panggil fungsi view_by_date_penumpang yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan penumpang Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_cetak = 'laporan/cetaklaporanpenumpang?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $url_excel = 'laporan/excellaporanpenumpang?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_month_penumpang($bulan, $tahun); // Panggil fungsi view_by_mont_penumpangh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan penumpang Tahun '.$tahun;
                $url_cetak = 'laporan/cetaklaporanpenumpang?filter=3&tahun='.$tahun;
                $url_excel = 'laporan/excellaporanpenumpang?filter=3&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_year_penumpang($tahun); // Panggil fungsi view_by_year_penumpang yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan penumpang';
            $url_cetak = 'laporan/cetaklaporanpenumpang';
            $url_excel = 'laporan/excellaporanpenumpang';
            $laporan = $this->laporan_model->view_all_penumpang(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['url_cetak'] = base_url($url_cetak);
        $data['url_excel'] = base_url($url_excel);
        $data['laporan'] = $laporan;
        //var_dump($laporan);die();
        $data['option_tahun'] = $this->laporan_model->option_tahun();
        $this->load->view('template/headeradmin',$data);
        $this->load->view('admin/laporanpenumpang',$data);
        $this->load->view('template/footeradmin');
    }

    public function cetaklaporanpenumpang(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan penumpang Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_penumpang($tgl); // Panggil fungsi view_by_date_penumpang yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan penumpang Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_penumpang($bulan, $tahun); // Panggil fungsi view_by_mont_penumpangh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan penumpang Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_penumpang($tahun); // Panggil fungsi view_by_year_penumpang yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan penumpang';
            $laporan = $this->laporan_model->view_all_penumpang(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('admin/cetaklaporanpenumpang', $data);
        
    }
    public function excellaporanpenumpang(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan penumpang Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_penumpang($tgl); // Panggil fungsi view_by_date_penumpang yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan penumpang Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_penumpang($bulan, $tahun); // Panggil fungsi view_by_mont_penumpangh yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan penumpang Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_penumpang($tahun); // Panggil fungsi view_by_year_penumpang yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan penumpang';
            $laporan = $this->laporan_model->view_all_penumpang(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('admin/excellaporanpenumpang', $data);
        
    }

    public function indexlaporanpetugas(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan verivikasi petugas Tanggal '.date('d-m-y', strtotime($tgl));
                $url_cetak = 'laporan/cetaklaporanpetugas?filter=1&tanggal='.$tgl;
                $url_excel = 'laporan/excellaporanpetugas?filter=1&tanggal='.$tgl;
                $laporan = $this->laporan_model->view_by_date_petugas($tgl); // Panggil fungsi view_by_date_verivikasi petugas yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan verivikasi petugas Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $url_cetak = 'laporan/cetaklaporanpetugas?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $url_excel = 'laporan/excellaporanpetugas?filter=2&bulan='.$bulan.'&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_month_petugas($bulan, $tahun); // Panggil fungsi view_by_mont_verivikasi petugash yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan verivikasi petugas Tahun '.$tahun;
                $url_cetak = 'laporan/cetaklaporanpetugas?filter=3&tahun='.$tahun;
                $url_excel = 'laporan/excellaporanpetugas?filter=3&tahun='.$tahun;
                $laporan = $this->laporan_model->view_by_year_petugas($tahun); // Panggil fungsi view_by_year_verivikasi petugas yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan verivikasi petugas';
            $url_cetak = 'laporan/cetaklaporanpetugas';
            $url_excel = 'laporan/excellaporanpetugas';
            $laporan = $this->laporan_model->view_all_petugas(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['url_cetak'] = base_url($url_cetak);
        $data['url_excel'] = base_url($url_excel);
        $data['laporan'] = $laporan;
        //var_dump($laporan);die();
        $data['option_tahun'] = $this->laporan_model->option_tahun_petugas();
        //var_dump($data['option_tahun']);die();
        $this->load->view('template/headerpetugas',$data);
        $this->load->view('petugas/laporanverivikasi',$data);
        $this->load->view('template/footerpetugas');
    }

    public function cetaklaporanpetugas(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan verivikasi petugas Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_petugas($tgl); // Panggil fungsi view_by_date_verivikasi petugas yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan verivikasi petugas Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_petugas($bulan, $tahun); // Panggil fungsi view_by_mont_verivikasi petugash yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan verivikasi petugas Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_petugas($tahun); // Panggil fungsi view_by_year_verivikasi petugas yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan verivikasi petugas';
            $laporan = $this->laporan_model->view_all_petugas(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('petugas/cetaklaporanverivikasi', $data);
        
    }
    public function excellaporanpetugas(){
        if(isset($_GET['filter']) && ! empty($_GET['filter'])){ // Cek apakah user telah memilih filter dan klik tombol tampilkan
            $filter = $_GET['filter']; // Ambil data filder yang dipilih user

            if($filter == '1'){ // Jika filter nya 1 (per tanggal)
                $tgl = $_GET['tanggal'];

                $ket = 'Laporan verivikasi petugas Tanggal '.date('d-m-y', strtotime($tgl));
                $laporan = $this->laporan_model->view_by_date_petugas($tgl); // Panggil fungsi view_by_date_verivikasi petugas yang ada di laporan_model
            }else if($filter == '2'){ // Jika filter nya 2 (per bulan)
                $bulan = $_GET['bulan'];
                $tahun = $_GET['tahun'];
                $nama_bulan = array('', 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');

                $ket = 'Laporan verivikasi petugas Bulan '.$nama_bulan[$bulan].' '.$tahun;
                $laporan = $this->laporan_model->view_by_month_petugas($bulan, $tahun); // Panggil fungsi view_by_mont_verivikasi petugash yang ada di laporan_model
            }else{ // Jika filter nya 3 (per tahun)
                $tahun = $_GET['tahun'];

                $ket = 'Laporan verivikasi petugas Tahun '.$tahun;
                $laporan = $this->laporan_model->view_by_year_petugas($tahun); // Panggil fungsi view_by_year_verivikasi petugas yang ada di laporan_model
            }
        }else{ // Jika user tidak mengklik tombol tampilkan
            $ket = 'Semua Laporan verivikasi petugas';
            $laporan = $this->laporan_model->view_all_petugas(); // Panggil fungsi view_all yang ada di laporan_model
        }

        $data['ket'] = $ket;
        $data['laporan'] = $laporan;

        
        $this->load->view('petugas/excelverivikasi', $data);
        
    }

}
