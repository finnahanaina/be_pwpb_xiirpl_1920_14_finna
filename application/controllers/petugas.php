<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('petugas_model');
		$this->db2 = $this->load->database('dbpetugas',TRUE);
		
		
	}

	public function index()
	{
		$this->load->view('template/headerpetugas');
		$this->load->view('petugas/dashboard');
		$this->load->view('template/footerpetugas');
	}
	function verivikasi(){
		$data['verivikasi'] = $this->petugas_model->getverivikasi();
		//var_dump($data['verivikasi'] );die();
		$this->load->view('template/headerpetugas');
		$this->load->view('petugas/verivikasi',$data);
		$this->load->view('template/footerpetugas');
	}

	function updatepemesanan($id){
		$where = array (
			'id_pemesanan' => $id
		);
		$petugas = $this->session->userdata('id_petugas');
		$data= array(
			'status' => 'verived',
			'id_petugas' => $petugas
		);
		//var_dump($data);die();
		$this->petugas_model->updatepemesanan($where,$data,'pemesanan');
		$this->session->set_flashdata('flash', 'Diubah');
		redirect('petugas/verivikasi');
	}
	function laporan(){
		// $data['verivikasi'] = $this->petugas_model->getverivikasi();
		//var_dump($data['verivikasi'] );die();
		$this->load->view('template/headerpetugas');
		$this->load->view('petugas/laporan');
		$this->load->view('template/footerpetugas');
	}

	function laporanverivikasi(){
		$data['laporanverivikasi'] = $this->petugas_model->getlaporanverivikasi()->result_array();
		//var_dump($data['laporanverivikasi'] );die();
		$this->load->view('template/headerpetugas');
		$this->load->view('petugas/laporanverivikasi',$data);
		$this->load->view('template/footerpetugas');
	}

	function cetaklaporanverivikasi(){
		$data['laporanverivikasi'] = $this->petugas_model->getlaporanverivikasi()->result_array();
		
		$this->load->view('petugas/cetaklaporanverivikasi',$data);
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('auth/index');
	}
}
