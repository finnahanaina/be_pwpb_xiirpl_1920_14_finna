<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelola_petugas extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('kelola_petugas_model');
    
    
  }
 
  public function index()
  {
    $data['title'] = 'Partner - Travela';
    $data['petugas'] = $this->kelola_petugas_model->get()->result();
    $this->load->view('template/headeradmin',$data);
    $this->load->view('admin/viewpetugas');
    $this->load->view('template/footeradmin');
  }

  public function add()
  {
    $data['title'] = 'Kelola petugas - Travela';
    
    $this->load->view('template/headeradmin',$data);
    $this->load->view('admin/addpetugas');
    $this->load->view('template/footeradmin');
  }

  public function aksi_add(){
    $nama = $this->input->post('nama');
    $username = $this->input->post('username');
    $password = password_hash($this->input->post('password'),PASSWORD_DEFAULT);
    
    $data  = array(
      'id_level' =>2,
      'username' => $username,
      'password' => $password,
      'nama_petugas' => $nama,
       );
    $insert = $this->kelola_petugas_model->addpetugas($data);
    if ($insert) {
      $this->session->set_flashdata('flash', 'Ditambahkan');
      redirect('kelola_petugas/index');
    } else {
      $this->session->set_flashdata('flash', 'Gagal');
      redirect('kelola_petugas/add');
    }
  }
  public function edit($id){
    $data['title'] = 'Edit petugas - Travela';
    $data['petugas'] = $this->kelola_petugas_model->getpetugasbyid($id)->row();
    $this->load->view('template/headeradmin',$data);
    $this->load->view('admin/editpetugas',$data);
    $this->load->view('template/footeradmin');

  }
  public function aksi_edit(){
    $id = $this->input->post('id');
    $nama = $this->input->post('nama');
    $username = $this->input->post('username');
   $password = password_hash($this->input->post('password'),PASSWORD_DEFAULT);
   // $password = $this->input->post('password');
    if ($this->input->post('password') == null) {
       $data  = array(
        'username' => $username,
        'nama_petugas' => $nama,
         );
    } else{
       $data  = array(
        'username' => $username,
        'password' => $password,
        'nama_petugas' => $nama,
         );
    }
   // var_dump($data);die();

    $edit = $this->kelola_petugas_model->editpetugas($id,$data);
    if ($edit) {
      $this->session->set_flashdata('flash', 'Diubah');
      redirect('kelola_petugas/index');
    } else {
      $this->session->set_flashdata('flash', 'Gagal');
      redirect('kelola_petugas/edit/'.$id);
    }
  }
  public function delete($id){
   
    $delete= $this->kelola_petugas_model->deletepetugas($id);

    if ($delete) {
      $this->session->set_flashdata('flash', 'Dihapus');
      redirect('kelola_petugas/index');
    } else {
      $this->session->set_flashdata('flash', 'Gagal');
      redirect('kelola_petugas/index');
    }
  }


}
