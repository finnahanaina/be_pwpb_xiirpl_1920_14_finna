<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->db2 = $this->load->database('dbadmin', TRUE);
		$this->load->library('form_validation');
		$this->load->model('auth_model');

	}
	
	public function index()
	{
		// $session= $this->session->userdata('id_petugas');
		// var_dump($session);die();
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim');
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela';			
			$this->load->view('login');
		} else {
			$this->aksi_login();
		}
	}
	public function aksi_login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'Password' => $password,
		);

		//cekpenumpang
		$userpenumpang = $this->db->get_where('penumpang', ['username' => $username])->row_array();
		
		if($userpenumpang){
			//cek password
				if(password_verify($password, $userpenumpang['password'])){
				$data = [
						'id_penumpang' => $userpenumpang['id_penumpang'],
						'username' => $userpenumpang['username'],
						'id_level' =>$userpenumpang['id_level']
					];
					//var_dump($data);die;
					$this->session->set_userdata($data);
					//var_dump($userpenumpang['password']);die();
					redirect('penumpang/index');
				}else{
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah!</div>');
					redirect('auth');
				}
		} else {
			$userpetugas = $this->db2->get_where('petugas', ['username' => $username])->row_array();
			//var_dump($userpetugas);die();
			if($userpetugas){
			//cek password
				if(password_verify($password, $userpetugas['password']) && $userpetugas['id_level'] == 2 ){
				$data = [
						'id_petugas'=>$userpetugas['id_petugas'],
						'username' => $userpetugas['username'],
						'id_level' =>$userpetugas['id_level']
					];
					$this->session->set_userdata($data);
					redirect('petugas/index');
				} else {
					if(password_verify($password, $userpetugas['password']) && $userpetugas['id_level'] == 1 ){
						$data = [
							'id_petugas'=>$userpetugas['id_petugas'],
							'username' => $userpetugas['username'],
							'id_level' =>$userpetugas['id_level']
						];
					$this->session->set_userdata($data);
					redirect('admin/index');				
					} 
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah</div>');	
						redirect('auth');
				}
			}
			else{
						$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Username Tidak Ditemukan!</div>');	
						redirect('auth');
					}
		}	

	}

	// public function proses_login(){
	// 	$username = $this->input->post('username');
	// 	$password = $this->input->post('password');
	// 	$where = array(
	// 		'username' => $username,
	// 		'Password' => $password,
	// 	);

	// 	$pelanggan = 
	// }


	public function registration()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required|trim',
			['required' => 'kolom Nama Harus Diisi!']);
		$this->form_validation->set_rules('username', 'Email', 'required|trim|is_unique[penumpang.username]',	['is_unique' => 'Username Tidak Tersedia!']);
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
			'required' => 'kolom Password  Harus Diisi!',
			'matches' => 'Password tidak sama!',
			'min_length' =>'Password terlalu pendek!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');
		$this->form_validation->set_rules('tanggallahir', 'Tanggallahir', 'required|trim',
			['required' => 'kolom tanggal Lahir Harus Diisi!']);
		$this->form_validation->set_rules('notelp', 'Notelp', 'required|trim',
			['required' => 'kolom No Telphone Harus Diisi!']);
		if ($this->form_validation->run() == false){
			$data['title'] = 'Travela User registration';
			$this->load->view('register');
			
		} else {
			$data  = [
				'id_level' => 3,				
				'username' => htmlspecialchars($this->input->post('username',true)),	
				'password' => password_hash($this->input->post('password1'),PASSWORD_DEFAULT),
				'nama_penumpang' => htmlspecialchars($this->input->post('nama', true)),
				'tanggal_lahir'=>htmlspecialchars($this->input->post('tanggallahir',true)),
				'telephone' =>htmlspecialchars($this->input->post('notelp',true)),
				'created_when' => time()
				
			];

			$this->db->insert('penumpang', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Selamat! Akun anda sudah terdaftar, Silahkan Login.</div>');
			redirect('auth');
		}
	}
	public function test()
	{
		$this->load->model('test_model');
		$data['data1'] = $this->test_model->get_db1();

		//$this->load->view('login', $data);
		var_dump($data['data1']);
		
	}
}
