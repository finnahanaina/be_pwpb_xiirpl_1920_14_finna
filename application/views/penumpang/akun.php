
  <!-- lokasi halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / Akun</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->



<!-- keterangan -->
 <div class="fluid-container">
        <div class="fluid-container-items-center ">
            <div class="container">
                <h3>Akun</h3>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->

<!-- content -->
  <div class="fluid-container">
     <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
        <div class="fluid-container-items-center ">
            <div class="container">
               <div class="row">
                  <?php if ($akun['jenis_kelamin'] == "l"): ?>
                     <div class="col-md-4">
                      <img src="<?= base_url() ?>assets/img/akun/men.png" class="img-akun">
                    </div>
                    <?php else: ?>
                     <div class="col-md-4">
                      <img src="<?= base_url() ?>assets/img/akun/women.png" class="img-akun">
                    </div>
                  <?php endif ?>
                
                 <div class="col-md-8 keterangan-akun">
                  <table class="table table-pemesanan">
                  <tbody>
                    <form action="<?= base_url() ?>penumpang/updateakun" method="post">
                    <tr class="tr-akun">
                      <input type="hidden" class="form-control" name="id_penumpang" value="<?= $akun['id_penumpang'] ?>">
                      <td>Nama</td>
                      <td><input type="text" class="form-control" name="nama" value="<?= $akun['nama_penumpang'] ?>"></td>
                    </tr>
                     <tr class="tr-akun">
                      <td>Username</td>
                      <td> <input type="text" class="form-control" name="username" value="<?= $akun['username'] ?>" readonly style="background-color: #fff"></td>
                    </tr>
                     <tr class="tr-akun">
                      <td>Tempat tanggal Lahir</td>
                      <td><input type="date" class="form-control" name="tgl_lahir" value="<?= $akun['tanggal_lahir']; ?>">  </td>
                    </tr>
                    <tr class="tr-akun">
                      <td>Alamat</td>
                      <td><input type="text" class="form-control" name="alamat" value="<?=$akun['alamat_penumpang'] ?>"> </td>
                    </tr>
                    <tr class="tr-akun">
                      <td>No Telphone</td>
                      <td> <input type="num" class="form-control" name="no_telp" value="<?= $akun['telephone'] ?>"></td>
                    </tr>
                     <tr class="tr-akun">
                      <td>Jenis Kelamin</td>
                      <td>
                        <select class="form-control" name="jenis_kelamin">
                          <option selected="<?= ($akun['jenis_kelamin']) ?>"><?php if($akun['jenis_kelamin']=="p"){
                            echo "Wanita";
                          } else {
                            echo "Pria";
                          }?></option>
                          <option value="p"> Wanita</option>
                          <option value="l"> Pria </option>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td class="row-span-2">
                        <button type="submit" class="btn btn-primary btn-block "> Update 
                        </button>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td class="row-span-2">
                        <a href="<?= base_url('penumpang/logout') ?>">
                        <button type="button" class="btn btn-info btn-block ">Logout 
                        </button>
                      </a>
                      </td>
                    </tr>
                  </tbody>
                </form>
              </table>
                  
                 
                 </div>
               </div>
            </div>
        </div>
  </div>
 
  
<!-- end content -->

