
<!-- lokasi halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / Kereta</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->

<!-- keterangan halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h3>Cari Rute Kereta</h3>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->

<div class="fluid-container">
        <div class="fluid-container d-flex align-items-center cari-tiket" style="min-height: 350px">
             
               
              <form action="<?= base_url()?>penumpang/kereta" method="post">
                <div class="row">
                  <div class="col-md-4">
                    <label for="dari">Dari</label>
                    <input type="text" class="form-control" id="dari" placeholder="Dari" name="rute_awal">
                  </div>
                  <div class="col-md-4">
                    <label for="tanggal">Tanggal</label>
                    <input type="date" class="form-control" id="tanggal" placeholder="Tanggal" name="tanggal" min="<?= date('Y-m-d')?>">                                      
                  </div>
                  <div class="col-md-4">
                    <label for="inputState">Kelas</label>
                    <select id="inputState" class="form-control" name="nama_kelas">
                        <option selected>Ekonomi</option>
                        <option>Bisnis</option>
                    </select>
                  </div>
                </div>

                <div class="row" style="margin-top: 10px;">
                  <div class="col-md-4">
                    <label for="inputEmail4">Ke</label>
                    <input type="text" class="form-control" id="inputEmail4" placeholder="Ke" name="rute_akhir">
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md-4">
                         <label for="inputState">Dewasa</label>
                          <select id="inputState" class="form-control" name="jmldewasa">
                              <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                      </div>
                      <div class="col-md-4">
                        <label for="inputState">Anak</label>
                          <select id="inputState" class="form-control" name="jmlanak">
                             <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                      </div>
                    </div>
                  </div>
                  
                   
                   <div class="col-md-4 ">
                   
                    <button type="submit" class="btn btn-primary btn-block mt-3" style="margin-top: 20px;"><i class="glyphicon glyphicon-search"></i>Cari tiket</button>
                  </div>
                </div>
              </form>
             
            
        </div>
</div>



