
  <!-- lokasi halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / pilih Pembayaran</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->



<!-- keterangan -->
 <div class="fluid-container">
        <div class="fluid-container-items-center ">
            <div class="container keteranganinput">
                <h6><img src="<?= base_url() ?>assets/bootstrap/img/cash.png">Pilih Metode Pembayaran</h6>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->
<form action="<?= base_url('penumpang/pembayaran') ?>" method="post">
  <input type="hidden" name="idrute" value="<?= $idrute ?>">
  <input type="hidden" name="idhargarute" value="<?= $idhargarute?>">
  <input type="hidden" name="jmlpenumpang" value="<?= $jmlpenumpang?>">
  <?php $i=0;foreach ($datapenumpang as $value): ?>
      <input type="hidden" name="nama[]" value="<?= $datapenumpang[$i]['nama']?>">
      <input type="hidden" name="nik[]" value="<?= $datapenumpang[$i]['nik'] ?>">
  <?php $i++;endforeach ?>
<!-- content -->
 <div class="fluid-container container-pilih-pembayaran" >
        <div class="fluid-container-items-center">            
                 <div class="container keteranganinputform">
                      <h5>Pembayaran Instan</h5> <hr>
                      <?php foreach ($pembayaran_instan as $value): ?>
                        <div class="row">
                          <div class="col-md-10">
                            <h6><?= $value['nama'] ?></h6>
                          </div>
                          <div class="col-md-2">
                            <table class="table table-pilih-rute">
                              <tbody>
                                <tr>
                                  <td class="pilihpembayaranimg"><img src="<?= base_url() ?>assets/img/partner/<?= $value['gambar'] ?>" style="min-width: 70px;"></td>
                                  <td><button class="btn btn-primary" name="metode" value="<?= $value['id'] ?>"><img src="<?= base_url() ?>assets/bootstrap/img/iconrightputih.png" style="max-width: 20px;"></a></button></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      <?php endforeach; ?>
                      
                 </div>
         </div>
  </div>
  <div class="fluid-container container-pilih-pembayaran">
        <div class="fluid-container-items-center">            
                 <div class="container keteranganinputform">
                      <h5>Gerai Retail</h5> <hr>
                      <?php foreach ($gerai_retail as $value): ?>
                        <div class="row">
                          <div class="col-md-10">
                            <h6><?= $value['nama'] ?></h6>
                          </div>
                          <div class="col-md-2">
                            <table class="table table-pilih-rute">
                              <tbody>
                                <tr>
                                  <td class="pilihpembayaranimg"><img src="<?= base_url() ?>assets/img/partner/<?= $value['gambar'] ?>" style="min-width: 70px;" ></td>
                                  <td><button class="btn btn-primary" name="metode" value="<?= $value['id'] ?>"><img src="<?= base_url() ?>assets/bootstrap/img/iconrightputih.png" style="max-width: 20px;"></a></button></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      <?php endforeach; ?>
                                        
                 </div>
         </div>

  </div>
 
</form>
  
<!-- end content -->


<!-- footer -->
