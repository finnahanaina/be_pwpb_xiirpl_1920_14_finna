
  <!-- keterangan rute-->
 <div class="container-fluid keterangan-rute">
        <div class="fluid-container-items-center text-light">
            <div class="container" ">
                <h4><?= $rute_awal ?> - <?= $rute_akhir ?></h4>
                <h5><?= $tanggal ?>  - <?= $nama_kelas ?> - 2 orang</h5>
            </div>
        </div>
  </div>
<!-- end keterangan rute-->

<!-- keterangan -->
 <div class="fluid-container sub-keterangan-rute">
        <div class="fluid-container-items-center">
            <div class="container">
                <h6 class="text-center">Pilih Penerbangan Dari <?= $jumlahrutepesawat ?> pilihan</h6>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->


 <div class="fluid-container" style="min-height: 500px;">
        <div class="fluid-container-items-center">
            <div class="container container-pilih-rute" >
                

                <table class="table table-hover">
                 
                  <tbody>
                   <?php $i=1;foreach ($rutepesawat  as $rute) : ?>
                    <tr>
                      <th scope="row"></th>
                      <td><?= $i ?></td>
                      <td>
                        <h4><?= $rute['nama'] ?></h4>
                        <h5><?= $rute['jam_berangkat'] ?></h5>
                      </td> 
                      <td>
                         <img src="<?= base_url('assets/img/partner/'.$rute['gambar'] ) ?>" class="img-pilih-rute">
                      </td>      
                      <td>Rp  .<?= number_format($rute['harga'],2,',','.');  ?> /orang</td>
                      <td><a href="<?= base_url('penumpang/inputdatapenumpang/').$rute['id_rute'].'/' .$rute['id_harga'] .'/'.$jmlpenumpang ; ?>"><button class="btn btn-primary">Pilih</button></a></td>
                    </tr>                   
                  </tbody>
                <?php $i++;endforeach; ?>
                </table>
            </div>
        </div>
  </div>

 