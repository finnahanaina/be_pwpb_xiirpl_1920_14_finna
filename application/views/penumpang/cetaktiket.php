

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Laporan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" />
   
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js" "></script>
    <script src="<?= base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
</head>
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
        padding: 20px;
    }
</style>
<body style="min-height: 1000px;">
    <div class="container-fluid" style="min-height: 500px;">
        <div class="row">
            <div class="col-md-12" style="background: #007bff; border-bottom: 15px solid #007bff; border-top: 15px solid #007bff;">
            </div>
        </div>
        <div class="row">
           <div class="col-md-4" style="padding: 20px;font-size: 1em;">
                 <img src="<?= base_url('assets/img/partner/'.$cetaktiket->gambar) ?>" style="width: 20vh">
            </div>
        </div>
        <div class="row justify-content-center" >
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top: 40px;">
                <center><h1></h1></center>
                   <div class="row">
            <div class="col-md-12 greycontainer mb-5 p-5">
                <table class="table">
                    <thead class="thead-light bg-primary">
                       <tr>

                          <th scope="col">Kode Pemesanan</th>
                          <th scope="col">Nama</th>
                          <th scope="col">Nik</th>
                          <th scope="col">Kode Kursi</th>
                          <th scope="col">Rute Awal</th>
                          <th scope="col">rute akhir</th>
                          <th scope="col">Tanggal</th> 
                          <th scope="col">Jam</th>
                          <th scope="col">Checkin</th>                       
                        </tr>
                      </thead>
                     <tbody> 
                        <?php
                        
                                
                            echo "<tr>";
                            echo "<td>".$cetaktiket->kode_pemesanan."</td>";
                            echo "<td>".$cetaktiket->nama."</td>";
                            echo "<td>".$cetaktiket->nik."</td>";
                             echo "<td>".$cetaktiket->kode_kursi."</td>";
                            echo "<td>".$cetaktiket->rute_awal."</td>";
                            echo "<td>".$cetaktiket->rute_akhir."</td>";
                            echo "<td>".$cetaktiket->tgl_berangkat ."</td>";
                            echo "<td>".$cetaktiket->jam_berangkat."</td>";
                            echo "<td>".$cetaktiket->jam_cekin."</td>";
                            echo "</tr>";
                           
                        ?>
                      </tbody>
                </table>

            </div>
        </div>
        </div>
        <hr>
        <div class="row justify-content-center" >
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top: 40px;">
                <center><h1><?php// $ket ?></h1></center>
                   <div class="row">
            <div class="col-md-12 greycontainer mb-5 p-5">
                
            </div>
        </div>
            </div>
        </div>
    </div>
</style>

</div>
<script>
   window.print();
  </script>
</body>
</html>