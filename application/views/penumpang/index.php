<?php if ($this->session->userdata('id_level') != 3) {
  redirect('auth/index');
}?>

<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style.css">
     <link rel="stylesheet" href="<?= base_url() ?>assets/datepicker/css/datepicker.css">
    <script src="<?= base_url() ?>assets/bootstrap/css/style.css"></script>
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?= base_url() ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
    <script defer src="<?= base_url() ?>/assets/bootstrap/js/fontawesome.js"></script>
    <title>Travela</title>
    <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
    <style>
      /* carousel */
#quote-carousel 
{
  padding: 0 10px 30px 10px;
  margin-top: 30px;
}

/* Control buttons  */
#quote-carousel .carousel-control
{
  background: none;
  color: #222;
  font-size: 2.3em;
  text-shadow: none;
  margin-top: 30px;
}
/* Previous button  */
#quote-carousel .carousel-control .left 
{
  left: -12px;
}
/* Next button  */
#quote-carousel .carousel-control .right 
{
  right: -12px !important;
}
/* Changes the position of the indicators */
#quote-carousel .carousel-indicators 
{
  right: 50%;
  top: auto;
  bottom: 0px;
  margin-right: -19px;
}
/* Changes the color of the indicators */
#quote-carousel .carousel-indicators li 
{
  background: #c0c0c0;
}
#quote-carousel .carousel-indicators .active 
{
  background: #333333;
}
#quote-carousel img
{
  width: 250px;
  height: 100px
}
/* End carousel */

.item blockquote {
    border-left: none; 
    margin: 0;
}

.item blockquote img {
    margin-bottom: 10px;
}

.item blockquote p:before {
    content: "\f10d";
    font-family: 'Fontawesome';
    float: left;
    margin-right: 10px;
}



/**
  MEDIA QUERIES
*/

/* Small devices (tablets, 768px and up) */
@media (min-width: 768px) { 
    #quote-carousel 
    {
      margin-bottom: 0;
      padding: 0 40px 30px 40px;
    }
    
}

/* Small devices (tablets, up to 768px) */
@media (max-width: 768px) { 
    
    /* Make the indicators larger for easier clicking with fingers/thumb on mobile */
    
    #quote-carousel .carousel-indicators {
        bottom: -20px !important;  
    }
    #quote-carousel .carousel-indicators li {
        display: inline-block;
        margin: 0px 5px;
        width: 15px;
        height: 15px;
    }
    #quote-carousel .carousel-indicators li.active {
        margin: 0px 5px;
        width: 20px;
        height: 20px;
    }
}
    </style>
    <script>
      $(document).ready(function() {
  //Set the carousel options
  $('#quote-carousel').carousel({
    pause: true,
    interval: 4000,
  });
});
    </script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Travela</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">      
      <ul class="nav navbar-nav navbar-right">
       
       <li><a href="<?=base_url() ?>penumpang/index"></span> Homepage</a></li>
      <li><a href="<?=base_url() ?>penumpang/Pesawat"></span> Pesawat</a></li>
      <li><a href="<?=base_url() ?>penumpang/kereta"></span> Kereta</a></li>
       <li><a href="<?=base_url() ?>penumpang/petunjuk"></span> Petunjuk</a></li>
       <li><a href="<?=base_url() ?>penumpang/riwayat_pemesanan"></span>Pemesanan</a></li>
      <li><a href="<?=base_url() ?>penumpang/akun"><span class="glyphicon glyphicon-user"></span></a></li>
      </ul>
    </div>
  </div>
 <div class="fluid-container bg-img hpx-600">
    <div class="fluid-container bg-gradient hpx-600 d-flex align-items-center">
        <div class="container text-center text-light ">
            <h1 class="travela-slider"><p>Travela</p></h1>
            <h2 class="deskripsi-slider">Pesan Tiket Pesawat Dan Kereta Dengan Mudah Di Travela!</h2>
        </div>
    </div>
</div>
</nav>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="card" style=" margin: 75px;">
        <div class="card-body">
          <div class="tabs" >
            <ul class="nav nav-tabs">
              <li class="active">
                <a href="#pesawat" data-toggle="tab" aria-expanded="true" role="tab" aria-controls="pesawat" aria-selected="true"><img src="<?= base_url() ?>assets/bootstrap/img/pesawat.png" class="icon-nav-tabs"> Pesawat</a>
              </li>
              <li class="">
                <a href="#kereta" data-toggle="tab" aria-expanded="" area-selected="false"><img src="<?= base_url() ?>assets/bootstrap/img/kereta.png"  class="icon-nav-tabs"> Kereta</a>
              </li>
            </ul>
            <div class="tab-content" style="padding: 50px;">
              <div id="pesawat" class="tab-pane active">
                <p>
                  <form action="<?= base_url()?>penumpang/rutepesawat" method="post">
                    <div class="row">
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Dari</label>
                         <input type="text" class="form-control" id="dari" placeholder="Dari" name="rute_awal">
                       </div>
                     </div>
                     <div class="col-md-4"> 
                       <div class="form-group">
                        <label>Tanggal</label>
                          <input type="date" class="form-control" id="tanggal" placeholder="Tanggal" name="tanggal" min="<?= date('Y-m-d')?>">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Kelas Penerbangan</label>
                          <select id="inputState" class="form-control" name="nama_kelas">
                            <option selected>Ekonomi</option>
                            <option>Bisnis</option>
                          </select>
                       </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Ke</label>
                         <input type="text" class="form-control" id="inputEmail4" placeholder="Ke" name="rute_akhir">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="row">
                        <div class="col-md-4">
                          <label for="inputState">Dewasa</label>
                          <select id="inputState" class="form-control" name="jmldewasa">
                            <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <label for="inputState">Anak</label>
                          <select id="inputState" class="form-control" name="jmlanak">
                            <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                        
                      </div>
                     </div>
                     <div class="col-md-4">
                       <button class="btn btn-primary btn-block" style="margin-top: 20px;"> Cari Tiket Pesawat !</button>
                     </div>
                   </div>
                  </form>
                </p>
                <p class="tulisan-ngeselin">
                  a
                </p>    
              </div>
              <div id="kereta" class="tab-pane">
                <p>
                <form action="<?= base_url()?>penumpang/kereta" method="post">
                   <div class="row">
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Dari</label>
                         <input type="text" class="form-control" id="dari" placeholder="Dari" name="rute_awal">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Tanggal</label>
                          <input type="date" class="form-control" id="tanggal" placeholder="Tanggal" name="tanggal" min="<?= date('Y-m-d')?>">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Kelas</label>
                          <select id="inputState" class="form-control" name="nama_kelas">
                            <option selected>Ekonomi</option>
                            <option>Bisnis</option>
                          </select>
                       </div>
                     </div>
                   </div>
                   <div class="row">
                     <div class="col-md-4">
                       <div class="form-group">
                        <label>Ke</label>
                         <input type="text" class="form-control" id="inputEmail4" placeholder="Ke" name="rute_akhir">
                       </div>
                     </div>
                     <div class="col-md-4">
                       <div class="row">
                        <div class="col-md-4">
                          <label for="inputState">Dewasa</label>
                          <select id="inputState" class="form-control" name="jmldewasa">
                            <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                        <div class="col-md-4">
                          <label for="inputState">Anak</label>
                          <select id="inputState" class="form-control" name="jmlanak">
                            <option selected>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                          </select>
                        </div>
                        
                      </div>
                     </div>
                     <div class="col-md-4">
                       <button class="btn btn-primary btn-block" style="margin-top: 20px;"> Cari Tiket Kereta !</button>
                     </div>
                   </div>
                </form>
                </p>
                <p class="tulisan-ngeselin">
                  a
                </p>    
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="fluid-container fluid-container-kenapa-pilih-travela">
    <div class="fluid-container bg-gradient d-flex align-items-center">
        <div class="fluid-container text-center text-light">   
            <div class="container container-kenapa-pilih-travela">            
                  <h1>Kenapa Pilih travela ?</h1>                
                    <div class="row row-kenapa">
                      <div class="col-md-4 col-kenapa">
                        <div class="row">
                            <div class="col-md-12">
                              <img src="<?= base_url() ?>assets/bootstrap/img/a.png" class="img-kenapa">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                              <h2>Mudah</h2>
                             
                            </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                              <img src="<?= base_url() ?>assets/bootstrap/img/c.png"  class="img-kenapa">
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12 col-kenapa">
                              <h2>Cepat</h2>
                             
                            </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                              <img src="<?= base_url() ?>assets/bootstrap/img/b.png"  class="img-kenapa">
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12 col-kenapa">
                              <h2>Aman</h2>
                             
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div> 
              </div>
            </div>            
        </div>
</div>
<div class="fluid-container container-responsive">
    <div class="fluid-container d-flex align-items-center">
        <div class="container text-center container-section-4">
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url() ?>assets/bootstrap/img/e.png">
                </div> 
                <div class="col-md-6 keterangan-section-4">
                    <h2>Berbagai Pilihan Pembayaran</h2>
                    <h4> Anda dapat menggunakan beragam metode pembayaran yang akan mempermudah proses pemesanan
                    </h4>
                </div>                                    
                </div>
                <hr>      
            <div class="row">
                <div class="col-md-6 keterangan-section-4">
                    <h2>Harga Terjangkau</h2>
                    <h4>Menawarkan Fasilitas perjalanan dengan harga yang pas
                    </h4> 
                </div> 
                <div class="col-md-6">
                    <img src="<?= base_url() ?>assets/bootstrap/img/d.png">
                </div>                                    
                </div>
                <hr>
            <div class="row align-items-center">
                <div class="col-md-6">
                   <img src="<?= base_url() ?>assets/bootstrap/img/f.png">
                </div> 
                <div class="col-lg-6 ">
                    <div class="p-5 keterangan-section-4">  
                      <h2>Pencarian cerdas</h2>                    
                      <h4>Temukan rute yang anda cari dengan cepat </h4>
                    </div>
                </div>                                  
            </div>               
        </div>
   </div>
</div>

<div class="fluid-container fluid-container-pesan-sekarang">
  <div class="fluid-container d-flex align-items-center">
    <div class="container text-center text-light ">
      <h1> Pesan Sekarang</h1>    
      <h4 class="text-center">Pesan sekarang untuk dapatkan harga terbaik</h4>
      <div class="row row-pesan-sekarang"><center>
        <div class="col-md-4"></div>
        <div class="col-md-4 ">
            <a href="<?= base_url('penumpang/pesawat') ?>"><button class="btn btn-lg btn-primary button-pesan">Pesawat</button></a>
            <a href="<?= base_url('penumpang/kereta') ?>"><button class="btn btn-lg btn-primary button-pesan">Kereta</button></a>
        </div>
        
      </div>    
    </div>
  </div>
</div>

<div class="fluid-container">
    <div class="fluid-container d-flex align-items-center">
        <div class="container container-partner">  
            <div class="row">
                <div class="col-md-6 col-partner">
                  <h2>Partner</h2>
                    <div class="row">
                        <div class="col-md-10">
                            <h4>Travela Bekerja sama Dengan beberapa Partner demi menjamin kenyamanan dan kemudahan anda dalam melakukan perjalanan</h4>
                        </div>
                    </div>
                </div>    
                <div class="col-md-6 col-partner-img">
                    <div class="row" > 
                        
                            <img src="<?= base_url() ?>assets/bootstrap/img/lion.png">
                        
                       
                            <img src="<?= base_url() ?>assets/bootstrap/img/CITILINK.png">
                        
                        
                            <img src="<?= base_url() ?>assets/bootstrap/img/garuda.png">
                                         
                       
                            <img src="<?= base_url() ?>assets/bootstrap/img/kai.jpg">
                        
                    </div>
                </div>                
            </div>  
        </div>
    </div>
</div>

<div class="fluid-container fluid-container-subscribe">
  <div class="fluid-container d-flex align-items-center">
    <div class="container text-center text-light ">
        <h1> Subscribe</h1>    
        <h4 class="text-center">Subscribe untuk mendapatkan pemberitahuan promo menarik setiap harinya
        </h4>
        <div class="row row-subscribe">
            <center>
                <div class="col-md-5"></div>
                 <div class="col-md-2">
                    <button class="btn btn-lg btn-danger"><i class="glyphicon glyphicon-play"></i>subsribe</button>
                </div>
            </center>
        </div>    
    </div>
    </div>
</div>

<div class="fluid-container" style="padding: 50px;">
     <div class="fluid-container d-flex align-items-center">
        <div class="container">
          <div class='row'>
    <div class='col-md-offset-2 col-md-8'>
      <div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
          <li data-target="#quote-carousel" data-slide-to="1"></li>
          
        </ol>
        
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner">
        
          <!-- Quote 1 -->
          <div class="item active">
            <blockquote>
              <div class="row">
                <div class="col-sm-3 text-center">
                  <img class="img-circle" src="<?= base_url('assets/img/quote/ava_ceo.png') ?>" style="width: 100px;height:100px;">
                  <!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
                </div>
                <div class="col-sm-9">
                  <p>Travela adalah sarana mempermudah explor indonesia!</p>
                  <small>CEO</small>
                </div>
              </div>
            </blockquote>
          </div>
          <!-- Quote 2 -->
          <div class="item">
            <blockquote>
              <div class="row">
                <div class="col-sm-3 text-center">
                  <img class="img-circle" src="<?= base_url('assets/img/quote/ava_manager.png') ?>" style="width: 100px;height:100px;">
                </div>
                <div class="col-sm-9">
                  <p>Mari Explor indonesia dengan Travela</p>
                  <small>Manager</small>
                </div>
              </div>
            </blockquote>
          </div>
         
        </div>
            
        <!-- Carousel Buttons Next/Prev -->
        <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
      </div>                          
    </div>
  </div>

          </div>    
               
        </div>
     </div>
  </div>
