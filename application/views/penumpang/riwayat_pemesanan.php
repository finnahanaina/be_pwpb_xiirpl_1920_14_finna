
<!-- lokasi halaman -->
 <div class="container" >
    <h5>Homepage / pemesanan</h5>
</div>
<!-- end lokasi halaman -->

<!-- keterangan halaman -->
 <div class="container " >
     <h3>Riwayat pemesanan</h3>
</div>
<!-- end keterangan halaman -->

<div class="fluid-container">
  <div class="container" style="margin-top: 40px;min-height: 300px;">
    <?php if ($data_pemesanan==null): ?>
      <div class="row">
        <div class="col-md-12" style="margin-bottom: 30px;">
          <img src="<?= base_url('assets/img/undrawempty.png') ?>" class="img-fluid" style="width: 100%;">
          <center><h2 style="color: rgb(51, 122, 183)">Anda Belum Pernah Melakukan Pemesanan :(</h2></center>
          <center>
            <a href="<?= base_url('penumpang/pesawat') ?>">
              <button class="btn btn-primary">Pesan Pesawat</button>
            </a>
            <a href="<?= base_url('penumpang/kereta') ?>">
              <button class="btn btn-primary">Pesan Kereta</button>
            </a>
          </center>
        </div>
      </div>
    <?php endif; ?>
    <?php foreach ($data_pemesanan as $value):?>
    <div class="row row-riwayat-pemesanan" style="border-bottom: solid 1px; border-color:#dddddd; padding: 20px">
      <div class="col-md-4">
        <h4>Kode pemesanan : 
         <p style="color:rgb(51, 122, 183)"><?= $value->kode_pemesanan?></p>
       </h4>
        <br>Tanggal pemesanan :
        <p><?= date('d F Y', strtotime($value->tgl_pemesanan));?></p>
      </div>
      <div class="col-md-4" style="margin-top: 30px;">
       <h4 class="text-danger" id="demo<?=$value->id_pemesanan?>"></h4><br>
          <?php $html='<h4 class="text-danger" id="demo<?=$value->id_pemesanan?>"></h4><br>' ?> 
         <!--  <?php if ($value->status=='unveriv'): ?>
            <?php echo $html ?>
          <?php endif;?> -->
        
        <h4> Total bayar : Rp. <?= number_format($value->total_bayar,2,',','.')?></h4>
      </div>
       <div class="col-md-4" style="margin-top: 30px;">
          <?php if ($value->bukti_pembayaran !== '' && $value->status == 'unveriv'  ): ?>
             <button class="btn btn-info" data-toggle="modal" data-target="#ubahkonfirmasi<?=$value->id_pemesanan?>">Ubah Bukti Pembayaran</button>
          <?php elseif($value->bukti_pembayaran == '' && $value->status == 'unveriv'  ): ?>
             <button class="btn btn-danger" data-toggle="modal" data-target="#konfirmasi<?=$value->id_pemesanan?>">Konfirmasi Pembayaran</button>
             
           <?php endif; ?>
           <?php if ($value->status == 'verived' ){ ?>
               <a href="<?= base_url('penumpang/detail_pemesanan/'.$value->id_pemesanan) ?>" >
                <button href="<?= base_url('penumpang/detail_pemesanan/'.$value->id_pemesanan) ?>" type="button" class="btn btn-success">Tiket</button>
              </a>    
             <?php } ?>
           <button type="button" class="btn btn-primary lihat-status" id="<?= $value->status ?>">Lihat Status</button>
      </div>
    </div>
    <!-- Modal -->
                          <div class="modal fade" id="konfirmasi<?=$value->id_pemesanan?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran Anda</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="<?= base_url('penumpang/konfirmasi_pembayaran') ?>" method="post" enctype="multipart/form-data">
                                  <div class="modal-body">
                                      <div class="form-group">
                                        <label>Foto</label>
                                        <input type="hidden" name="id_pemesanan" value="<?= $value->id_pemesanan ?>">
                                        <input type="file" name="gambar" class="form-control">
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                  </div>
                              </form>
                              </div>
                            </div>
                          </div>
                           <!-- Modal -->
                          <div class="modal fade" id="ubahkonfirmasi<?=$value->id_pemesanan?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Ubah Bukti Pembayaran Anda</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action="<?= base_url('penumpang/konfirmasi_pembayaran') ?>" method="post" enctype="multipart/form-data">
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-md-6">
                                        <img src="<?= base_url('assets/img/bukti_pembayaran/'.$value->bukti_pembayaran) ?>" style="width:200px;">
                                      </div>
                                      <div class="col-md-6">
                                        <div class="form-group">
                                          <label>Foto</label>
                                          <input type="hidden" name="id_pemesanan" value="<?= $value->id_pemesanan ?>">
                                          <input type="file" name="gambar" class="form-control">
                                        </div>
                                      </div>
                                    </div>
                                     
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                  </div>
                              </form>
                              </div>
                            </div>
                          </div>
                           <script>
    var d = new Date();
    d.setHours(d.getHours() + 2);
    // var countDownDate = new Date(d).getTime();
    var countDownDate = new Date("<?= $value->batas_pembayaran ?>").getTime();
    console.log(countDownDate);
    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        // document.getElementById("demo").innerHTML = countDownDate;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo<?=$value->id_pemesanan?>").innerHTML = "Waktu Expired :" +hours + " jam " +
            minutes + " menit " + seconds + " detik ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo<?=$value->id_pemesanan?>").innerHTML = "EXPIRED";
        }
    }, 1000);

    // });
    </script>

                          
  <?php endforeach; ?>
  </div>
  
 
</div>


<!-- end content -->

