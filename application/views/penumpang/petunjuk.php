
<!-- lokasi halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / Petunjuk pemesanan</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->

<!-- keterangan halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h3> Petunjuk Pemesanan Tiket</h3>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->

<!-- content -->
<div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
               <div class="row">
               	<div class="col-md-12"><img src="<?= base_url() ?>assets/bootstrap/img/group.png" style="width: 100%;"></div>
               </div><hr>
            </div> 
        </div>
  </div> 

   <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <div class="row">
	               	<div class="col-md-5">
	               		<h3>1. cari rute</h3>
	               		<h5>Cari Rute yang anda tuju di halaman pencarian rute, isi form dengan benar sesuai dengan yang anda butuhkan</h5>
	               	</div>
                </div><hr>

                <div class="row">
               		<div class="col-md-6">               			
               		</div>
               		<div class="col-md-5">
	               		<h3>2. Pilih Rute</h3>
	               		<h5>Lalu akan muncul beberapa pilihan rute yang sesuai dengan yang anda cari, pilih sesuai dengan yang anda inginkan</h5>
               		</div>
               		</div><hr>          		
                <div class="row">
               		<div class="col-md-5">
	               		<h3> 3. Masukan Data Penumpang</h3>
	               		<h5>Isi data para penumpang dengan benar, karena data tersebut adalah data yang akan dicetak di tiket anda</h5>
               		</div>
                </div><hr>   
                <div class="row">
               		<div class="col-md-6">               			
               		</div>
               		<div class="col-md-5">
	               		<h3>4.Pesan</h3>
	               		<h5>pilih metode pembayaran lalu pesan, dengan memesan tiket di Travela anda menyetujui segala ketentuan yang berlaku</h5>
               		</div>
               	</div><hr> 
               	<div class="row">
               		<div class="col-md-5">
	               		<h3> 5. Bayar</h3>
	               		<h5>Bayar pesanan anda sesuai dengan pilihan metode pembayaran anda, lalu upload bukti pembayaran di halaman pemesanan, tunggu beberapa saat untuk konfirmasi dari petugas kami, Kembali ke halaman pemesanan dan pilih kursi lalu cetak tiket anda</h5>
               		</div>
                </div><hr>      
        	</div>
  		</div> 
  	</div>
<!-- end content -->
