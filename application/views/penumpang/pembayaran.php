
  <!-- lokasi halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / Pembayaran</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->


<form action="<?= base_url('penumpang/proses_pesan/') ?>" method="post">

  <?php $i=0;foreach ($datapenumpang as $value): ?>
      <input type="hidden" name="nama[]" value="<?= $datapenumpang[$i]['nama']?>">
      <input type="hidden" name="nik[]" value="<?= $datapenumpang[$i]['nik'] ?>">
  <?php $i++;endforeach; ?>
  <input type="hidden" name="totalbayar" value="<?php $totalbayar = $inforute->harga * $jmlpenumpang; echo $totalbayar; ?>">
  <input type="hidden" name="idrute" value="<?= $idrute ?>">
  <input type="hidden" name="idhargarute" value="<?= $idhargarute?>">
  <input type="hidden" name="metodepembayaran" value="<?= $metodepembayaran?>">

<!-- keterangan -->

 <div class="fluid-container">
        <div class="fluid-container-items-center ">
            <div class="container keteranganinput">
                <h6><img src="<?= base_url() ?>assets/img/partner/<?= $pilihanmetode->gambar ?>" class="mr-3"> <?=  $pilihanmetode->nama ?> </h6>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->

<!-- content -->
 
  <div class="fluid-container container-pembayaran m-auto" >
        <div class="fluid-container-items-center">            
                 <div class="container keteranganinputform" style="min-height: 400px;">                     
                      <div class="row">
                        <div class="col-md-8">
                          <h6>Harga Tiket</h6>
                        </div>
                        <div class="col-md-4">
                          <h6>Rp <?= $inforute->harga ?></h6>
                        </div>                        
                      </div>
                       <div class="row">
                        <div class="col-md-8">
                          <h6>Biaya Layanan</h6>
                        </div>
                        <div class="col-md-4">
                          <h6>Rp.<?= $pilihanmetode->biaya_transaksi ?></h6>
                        </div>                        
                      </div>  
                      <hr>    
                      <div class="row">
                        <div class="col-md-8">
                          <h6>Total Pembayaran</h6>
                        </div>
                        <div class="col-md-4">
                          <h6> Rp. <?php $totalbayar= $inforute->harga * $jmlpenumpang;echo $totalbayar; ?></h6>
                        </div>                        
                      </div>                  
                 </div>
         </div>
  </div>
  <div class="container">
    <div class="row">
     <div class="col-md-10">
      <h5>Dengan menekan tombol, anda telah menyetujui syarat dan ketentuan yang berlaku</h5>
    </div> 
  </div>
  </div>

  <div class="container ">
    <div class="row">
     <div class="col-md-10">
      
    </div>
   
    <div class="col-md-2 tombolkepembayaran">
     <button class="btn btn-primary "> Lanjut Pembayaran</button>
    </div>
  </div>
  </div>

  
<!-- end content -->

