

  <!-- keterangan rute-->
 <div class="container-fluid keterangan-rute">
        <div class="fluid-container-items-center text-light">
            <div class="container" ">
                 <h4><?= $rute_awal ?> - <?= $rute_akhir ?></h4>
                <h5><?= $tanggal ?>  - <?= $nama_kelas ?> - 2 orang</h5>
            </div>
        </div>
  </div>
<!-- end keterangan rute-->

<!-- keterangan -->
 <div class="fluid-container sub-keterangan-rute" >
        <div class="fluid-container-items-center">
            <div class="container">
                <h6 class="text-center">Kami Telah Menampilkan semua jadwal untuk rute ini</h6>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->


 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container container-pilih-rute" style="min-height: 400px;">
                

                <table class="table table-hover">
                  <thead class="thead-light">
                   
                  </thead>
                  <tbody>
                    <?php $i=1;
                    foreach ($rutekereta as $value ) :?>
                    <tr>
                      <th scope="row"><?= $i  ?></th>
                      <td><?= $value['nama'] ?></td>
                      <td> 
                          <img src="<?= base_url('assets/img/partner/'.$value['gambar'] ) ?>" class="img-pilih-rute">
                          
                      </td>      
                      <td><?= $value['jam_berangkat'] ?></td>
                      <td>Rp  .<?= number_format($value['harga'],2,',','.');  ?> /orang</td>
                      
                      <td><a href="<?= base_url('penumpang/inputdatapenumpang/').$value['id_rute'].'/' .$value['id_harga'] .'/'.$jmlpenumpang ; ?>"><button class="btn btn-primary">Pilih</button></a></td>
                    </tr>
                    <?php $i++;endforeach; ?>
                  </tbody>
                </table>
            </div>
        </div>
  </div>

