<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <img src="<?= base_url('assets/img/undrawnodata.png') ?>" style="width: 100%;">
        <center>
          <h1>Rute Tidak Ditemukan :(</h1>
          <a href="<?= base_url('penumpang/kereta')  ?>">
            <button class="btn btn-primary  mt-5 btn text-white" style="border-radius: 20px;border-color: white;border-width: 3px; margin-bottom: 40px;">
            Cari Rute kereta lain
            </button>
          </a>
        </center>
    </div>
  </div> 
</div>