
<!-- lokasi halaman -->
 <div class="fluid-container">
  <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
        <div class="fluid-container-items-center">
            <div class="container">
                <h5>Homepage / pemesanan</h5>
            </div>
        </div>
  </div>
<!-- end lokasi halaman -->

<!-- keterangan halaman -->
 <div class="fluid-container">
        <div class="fluid-container-items-center">
            <div class="container">
                <h3>Pemesanan </h3>
            </div>
        </div>
  </div>
<!-- end keterangan halaman -->

<!-- content -->
 <div class="fluid-container container-pemesanan" >
        <div class="fluid-container-items-center cari-tiket2" style="min-height: 200px;">
         <?php $batas_pembayaran=$pemesanan->batas_pembayaran ?>
       
          <h3 class="text-danger" id="demo"></h3>
          <div class="row">

            <div class="col-md-1"></div>
              <div class="col-md-11">
                <table class="table table-bordered table-pemesanan">
                  <thead>
                    <tr>
                      
                     
                      <th scope="col">Nama</th>
                      <th scope="col">Nik</th>
                      <th scope="col">Rute Awal</th>
                      <th scope="col">Rute Akhir</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?// var_dump($data_pemesanan);die(); ?>
                    <?php foreach ($detail_data_pemesanan as $value):?>
                      <tr>
                        
                        <td><?= $value->nama?></td>
                         <td><?= $value->nik?></td>
                        <td><?= $value->rute_awal?></td>
                        <td><?= $value->rute_akhir?></td>
                          
                        <td>
                          <?php if ($value->kode_kursi == ''): ?>
                            <button data-toggle="modal" data-target="#pilihkursi<?=$value->id_detail_pemesanan?>" id="<?= $value->id_detail_pemesanan ?>" type="button" class="btn btn-primary button-kursi">Pilih Kursi</button>
                          <?php else: ?>
                           <a href="<?= base_url('penumpang/cetaktiket/'.$value->id_pemesanan.'/'.$value->id_detail_pemesanan) ?>" >
                            <button type="button" class="btn btn-primary">Cetak Tiket</button>

                          </a>
                        <?php endif; ?>
                        
                          
                         <!-- Modal -->
                          <div class="modal fade" id="pilihkursi<?=$value->id_detail_pemesanan?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Pilih Kursi</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <?php if ($value->id_tipe_transportasi =='2'): ?>
                               
                                  <form action="<?= base_url('penumpang/ubahkursi/'.$value->id_pemesanan.'/'.$value->id_detail_pemesanan) ?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-body justify-content-center">
                                      <img src="" class="img-fluid" style="width: 100%;" id="img_gambar<?= $value->id_detail_pemesanan ?>">
                                      <select class="form-control" name="gerbong" id="select_gerbong<?= $value->id_detail_pemesanan ?>" onchange="cekGerbong('<?= $value->id_detail_pemesanan ?>')" style="margin-top: 10px;">
                                        <option disabled selected>==Pilih Gerbong===</option>
                                        <option value="1">Gerbong 1</option>
                                        <option value="2">Gerbong 2</option>
                                        <option value="3">gerbong 3</option>
                                      </select>
                                      <select class="form-control" name="bagian" id="bagian<?= $value->id_detail_pemesanan ?>" required onchange="cekBagian('<?= $value->id_detail_pemesanan ?>')" style="margin-top: 10px">
                                        <option disabled selected>===Pilih Bagian===</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                      </select>
                                       <p class="text-danger" style="margin-top: 5px;">Pilihan Kursi yang tampil adalah kursi yang Masih Tersedia</p>
                                      <select class="form-control" name="kode_kursi" id="bagian_a<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px;">

                                        <?php if ($kursi_a==null): ?>
                                        <!--   <option selected disabled> Maaf Tidak ada kursi dibagian ini</option>
 -->                                        <?php else: ?>
                                        <?php foreach ($kursi_a as $a) :?>
                                        <option value="<?= $a->id ?>"><?= $a->kursi ?></option>
                                        <?php endforeach; ?>
                                      <?php endif; ?>
                                      </select>
                                      <select class="form-control" name="kode_kursi" id="bagian_b<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px">
                                        <?php if ($kursi_b==null): ?>
                                         <!--  <option selected disabled> Maaf Tidak ada kursi dibagian ini</option> -->
                                        <?php else: ?>
                                        <?php foreach ($kursi_b as $b) :?>
                                        <option value="<?= $b->id ?>"><?= $b->kursi ?></option>
                                        <?php endforeach; ?>
                                      <?php endif; ?>
                                      </select>
                                        
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                      <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                  </form>
                                <?php else: ?>
                                  <form action="<?= base_url('penumpang/ubahkursi/'.$value->id_pemesanan.'/'.$value->id_detail_pemesanan) ?>" method="post" enctype="multipart/form-data">
                                    <div class="modal-body justify-content-center">
                                      <img src="<?= base_url('assets/img/kursi/kursipesawat.png') ?>" class="img-fluid" style="width: 100%;">
                                     
                                        
                                      <select class="form-control" name="bagian" id="bagian<?= $value->id_detail_pemesanan ?>" required onchange="cekBagian('<?= $value->id_detail_pemesanan ?>')" style="margin-top: 10px">
                                        <option disabled selected>===Pilih Bagian===</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                      </select>
                                      <p class="text-danger" style="margin-top: 5px;">Pilihan Kursi yang Tampil adalah kursi yang masih tersedia</p>
                                      <select class="form-control" name="kode_kursi" id="bagian_a<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px;">
                                        <?php foreach ($kursi_a as $a) : ?>
                                        <option value="<?= $a->id ?>"><?= $a->kursi ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      <select class="form-control" name="kode_kursi" id="bagian_b<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px">
                                         <?php foreach ($kursi_b as $b) : ?>
                                        <option value="<?= $b->id ?>"><?= $b->kursi ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      <select class="form-control" name="kode_kursi" id="bagian_c<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px">
                                         <?php foreach ($kursi_c as $c) : ?>
                                        <option value="<?= $c->id ?>"><?= $c->kursi ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                      <select class="form-control" name="kode_kursi" id="bagian_d<?= $value->id_detail_pemesanan ?>" style="margin-top: 10px">
                                        <?php foreach ($kursi_d as $d) : ?>
                                        <option value="<?= $d->id ?>"><?= $d->kursi ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                        
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Simpan</button>
                                    </div>
                                  </form>
                                <?php endif; ?>
                              </div>
                            </div>
                          </div>
                         
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
              </table>
              <script>
    var d = new Date();
    d.setHours(d.getHours() + 2);
    // var countDownDate = new Date(d).getTime();
    var countDownDate = new Date("<?= $batas_pembayaran ?>").getTime();
    console.log(countDownDate);
    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;
        // document.getElementById("demo").innerHTML = countDownDate;
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        document.getElementById("demo").innerHTML = "Waktu Expired :" +hours + " jam " +
            minutes + " menit " + seconds + " detik ";

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);

    // });
    </script>
            </div>  
          </div>
        </div>
  </div>
<!-- end content -->

