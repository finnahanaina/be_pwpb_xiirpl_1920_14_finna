 <footer class="page-footer" style="bottom: 0px!important">

  <div class="fluid-container fluid-container-footer">
     <div class="fluid-container d-flex align-items-center">
        <div class="container text-light opacity-1">
         <div class="row">
           <div class="col-md-4">
             <h1 class="travela-footer">Travela</h1>             
           </div>
           <div class="col-md-3"> 
             <h3>Navigation</h3>
                <div class="row">
                <div class="col-md-5">
                    <h5>Pesawat</h5> 
                     <h5>Kereta</h5>
                     <h5>Petunjuk</h5>
                </div>
            </div>
                       
           </div>
            <div class="col-md-3"> 
           <h3>Contact Us</h3>
          <p> <strong>Address:</strong> Kebayoran Baru, Jakarta, Indonesia</p>
          <p> <strong>Phone:</strong> (021) 456-789</p>
          <p> <strong>Email:</strong> <a href="mailto:travela@gmail.com">travela@gmail.com</a></p>
                      
           </div>
           
         <br>
         <div class="row">
           <div class="col-md-12">
            <center>
              <img src="<?= base_url() ?>assets/bootstrap/img/fb.png">
              <img src="<?= base_url() ?>assets/bootstrap/img/twitter.png">
              <img src="<?= base_url() ?>assets/bootstrap/img/ig.png">
            </center>              
           </div>
         </div>
         <br><br>
         <div class="row" >
           <div class="col-md-12">
            <center>© Copyright Travela 2019</center>              
           </div>
         </div>                       
        </div>
     </div>
  </div>
  </footer>
  
</div>
  
    <script src="<?= base_url() ?>assets/bootstrap/js/sweetalert2.all.min.js"></script>
    <script src="<?= base_url() ?>assets/vendor/drift/dist/drift.js"></script>
   <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
    <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>

    <script>
      $('.lihat-status').on('click', function (e) {
          e.preventDefault();
         const id = $(this).attr('id');  
         if (id =='verived') {
           Swal.fire({ 
            title: 'Status',
            text:'Terverivikasi, Silahkan Cetak Tiket',
            type: 'info',
            width:500
          });
         }else if(id=='unveriv'){
           Swal.fire({ 
            title: 'Status',
            text:'Belum Terverivikasi, Kirim Bukti Pembayaran, Jika sudah Silahkan Tunggu Beberapa Menit lagi',
            type: 'info',
            width:500,
            height:500
          });
         }
          
      });
    </script>
    <script>
      $('.button-kursi').on('click', function (e) {
        e.preventDefault();
         const id = $(this).attr('id');  
          $('#bagian_a'+id).hide();
          $('#bagian_b'+id).hide();
          $('#bagian_c'+id).hide();
          $('#bagian_d'+id).hide();
      });
     
      function cekGerbong($data){
        var gambar = $('#img_gambar'+$data);
        var gerbong = $('#select_gerbong'+$data);
        if (gerbong.val()=="1"){
          gambar.attr('src','<?= base_url('assets/img/kursi/gerbong1.png') ?>');
        } else if(gerbong.val()=="2"){
          gambar.attr('src','<?= base_url('assets/img/kursi/gerbong2.png') ?>');
        }else if(gerbong.val()=="3"){
          gambar.attr('src','<?= base_url('assets/img/kursi/gerbong3.png') ?>');
        }
      }
      function cekBagian($data){
        var bagian = $('#bagian'+$data);
        var bagian_a = $('#bagian_a'+$data);
        var bagian_b = $('#bagian_b'+$data);
        var bagian_c = $('#bagian_c'+$data);
        var bagian_d = $('#bagian_d'+$data);
        if (bagian.val()==="A"){
          bagian_a.show();
          bagian_b.hide();
          bagian_c.hide();
          bagian_d.hide();
        }else if(bagian.val()==="B"){
          bagian_b.show();
          bagian_a.hide();
          bagian_c.hide();
          bagian_d.hide();
        }else if(bagian.val()==="C"){
          bagian_c.show();
          bagian_a.hide();
          bagian_b.hide();
          bagian_d.hide();
        }else if(bagian.val()==="D"){
          bagian_d.show();
          bagian_a.hide();
          bagian_b.hide();
          bagian_c.hide();
        }
      }
    </script>

    <script>
      function additem($data) {

                    var i = $data;  
                    FilePond.registerPlugin(
                            FilePondPluginImagePreview,
                            );    
                        // Set default FilePond options
                        FilePond.setOptions({
                            // maximum allowed file size
                            maxFileSize: '50MB',
                            imagePreviewHeight: 100,
                            imagePreviewWidth: 200,
                            instantUpload: true,
                            // crop the image to a 1:1 ratio
                            imageCropAspectRatio: '1:1',
                            // upload to this server end point
                            server: {
                                url: '<?= base_url("Admin/uploadApi/") ?>',
                            }        
                        });
                       
                        var pond = FilePond.create(document.querySelector('input[id=filepond'+ i +']'));
                  }

                    </script>

    

</html>