<?php if ($this->session->userdata('id_level') != 3) {
  redirect('auth/index');
}?>


<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style.css">
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style.css">
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>

  <!--   <script src="<?= base_url() ?>assets/bootstrap/js/myscript.js"></script> -->
   
    <title>Travela</title>
    <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
   

</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Travela</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">      
      <ul class="nav navbar-nav navbar-right">
       
      <li><a href="<?=base_url() ?>penumpang/index"></span> Homepage</a></li>
      <li><a href="<?=base_url() ?>penumpang/Pesawat"></span> Pesawat</a></li>
      <li><a href="<?=base_url() ?>penumpang/kereta"></span> Kereta</a></li>
       <li><a href="<?=base_url() ?>penumpang/petunjuk"></span> Petunjuk</a></li>
       <li><a href="<?=base_url() ?>penumpang/riwayat_pemesanan"></span>Pemesanan</a></li>
      <li><a href="<?=base_url() ?>penumpang/akun"><span class="glyphicon glyphicon-user"></span></a></li>
      </ul>
    </div>
  </div>
</nav>
