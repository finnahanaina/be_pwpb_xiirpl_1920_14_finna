<?php if($this->session->userdata('id_level') != 1) {
    redirect('auth/index');
} ?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Travela Admin</title>
     <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
  <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min1.css">
      <!-- Datatables -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/datatables/datatables.min.css') ?>">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style4.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/styleadmin.css">


    <script>
    var baseurl = "<?php echo base_url(); ?>"; // Buat variabel baseurl untuk nanti di akses pada file config.js
    </script>
     <link rel="stylesheet" href="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.css'); ?>" />
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js" "></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/solid.js" ></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
    <script defer src="<?= base_url() ?>assets/js/customajax.js"></script>
     <script src="<?= base_url() ?>assets/charts/chart.js/dist/Chart.js"></script>
    <script src="<?= base_url() ?>assets/charts/chart.js/dist/Chart.min.js"></script>
    <script src="<?= base_url() ?>assets/js/Chart.js"></script>



</head>

<body style="max-width: 100%;">

    <div class="wrapper" style="margin-right: 0!important;margin-left: 0!important;">
        <!-- Sidebar  -->
        <nav id="sidebar" >
            <div class="sidebar-header">
                <h3>Travela</h3>
            </div>

             <ul class="list-unstyled components">
                <li >
                   <a href="<?= base_url() ?>admin/index">
                        <i class="fas fa-home"></i>
                        Dashboard
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/transportasi">
                        <i class="fas fa-plane"></i>
                        Transportasi
                    </a>
                    
                </li>
                <li > 
                    <a href="<?= base_url() ?>admin/rute">
                        <i class="fas fa-train"></i>
                        Rute
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/verivikasi">
                        <i class="fas fa-check"></i>
                        verivikasi
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>partner/index">
                        <i class="fas fa-handshake"></i>
                        Partner
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>kelola_petugas/index">
                        <i class="fas fa-user"></i>
                        Petugas
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/laporan">
                        <i class="fas fa-file"></i>
                        Laporan
                    </a>
                </li>
                <li>
                    <a  href="<?= base_url() ?>admin/logout" class="logout">
                        <i class="fas fa-sign-out-alt"></i>
                        logout
                    </a>
                </li>
            </ul>                
        </nav>

        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-inverse bg-dark navbaradmin" style="margin-right: 1px!important;">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                        <i class="fas fa-align-left"></i>                     
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto">
                             <li>
                                 <!--  <form class="navbar-form navbar-left" action="/action_page.php">
                                    <div class="input-group">
                                      <input type="text" class="form-control" placeholder="Search">
                                      <div class="input-group-btn">
                                        <button class="btn btn-primary" type="submit">
                                          <i class="fas fa-search"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </form> -->
                            </li>
                           <li><a  href="exampleModal2" data-toggle="modal" data-target="#exampleModal2"><i class="fas fa-user" style="width: 50px;margin-top: 10px;"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
