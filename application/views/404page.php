<!DOCTYPE html>
<html>
<head>
	<title>Halaman Tidak Di Temukan</title>
	 <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min1.css">
	 <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
</head>
<body style="background-color:#00ccff">
	<div class="container-fluid">
		<div class="row justify-content-center"  style="min-height:600px;">
			<div class="col-md-4 text-white">
				<center>
				<h1 style="font-size: 15em;">404</h1>
				
					<h4>Oops, Halaman Tidak Ditemukan</h4>
					<a href="<?= base_url('penumpang/index') ?>">
						<button class="btn btn-outline-info mt-5 btn text-white" style="border-radius: 20px;border-color: white;border-width: 3px;">kembali Ke Home Travela
						</button>
					</a>
				</center>
				<!-- <img src="<?= base_url('assets/img/undraw.png') ?>" style="max-width: 600px;"> -->
			</div>
			

		</div>
	</div>
</body>


<footer class="mb-0">
	<svg xmlns="<?= base_url('assets/img/wave.html') ?>" viewBox="0 0 1440 320" >
	  <path fill="#ffff" fill-opacity="1" d="M0,128L26.7,117.3C53.3,107,107,85,160,96C213.3,107,267,149,320,138.7C373.3,128,427,64,480,74.7C533.3,85,587,171,640,213.3C693.3,256,747,256,800,240C853.3,224,907,192,960,165.3C1013.3,139,1067,117,1120,138.7C1173.3,160,1227,224,1280,213.3C1333.3,203,1387,117,1413,74.7L1440,32L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z">
	  </path>
 	</svg>
</footer>




</html>