

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style.css">
  <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js"></script>
      <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>
  
     <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
     <script defer src="<?= base_url() ?>assets/bootstrap/js/solid.js" ></script>
    <title>Travela</title>
     <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
  </head>
  <style>
  	
  	.kotaklogin{
  		
  		background-color:#ffffff;
  		margin-top:80px;
  		border-radius:10px;
  		padding-left: 50px;
      padding-top: 50px;
      padding-right: 50px;
      margin-bottom: 50px;
      margin-left: 20px;
      margin-right: 20px;
  	}
  	.login{
  		color:#000000;
  	}
    .travela-login{
    margin-top: 10px;
    }
    .hr-login{
        border-color: #000000;  
        border-width: 2px;
    }
    .left-login{
        background-color: blue;
        width: 100%
    }
    .login-form{
      margin-top: 40px;
    }
    .belumpunyaakun{
      margin-top: 20px;
      margin-bottom: 0px;
    }
  </style>
  <!-- <svg xmlns="<?= base_url() ?>wave.html" viewBox="0 0 1440 320"><path fill="#0099ff" fill-opacity="1" d="M0,288L17.1,256C34.3,224,69,160,103,149.3C137.1,139,171,181,206,213.3C240,245,274,267,309,277.3C342.9,288,377,288,411,266.7C445.7,245,480,203,514,208C548.6,213,583,267,617,245.3C651.4,224,686,128,720,74.7C754.3,21,789,11,823,42.7C857.1,75,891,149,926,170.7C960,192,994,160,1029,144C1062.9,128,1097,128,1131,160C1165.7,192,1200,256,1234,261.3C1268.6,267,1303,213,1337,181.3C1371.4,149,1406,139,1423,133.3L1440,128L1440,0L1422.9,0C1405.7,0,1371,0,1337,0C1302.9,0,1269,0,1234,0C1200,0,1166,0,1131,0C1097.1,0,1063,0,1029,0C994.3,0,960,0,926,0C891.4,0,857,0,823,0C788.6,0,754,0,720,0C685.7,0,651,0,617,0C582.9,0,549,0,514,0C480,0,446,0,411,0C377.1,0,343,0,309,0C274.3,0,240,0,206,0C171.4,0,137,0,103,0C68.6,0,34,0,17,0L0,0Z"></path></svg> -->
  <body background="<?= base_url() ?>assets/bootstrap/img/bg.jpeg" style="background-repeat:no-repeat;background-size:cover;">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-2">
  			</div>
  			<div class="col-md-8 kotaklogin">
          <div class="row">
            <div class="col-md-4"><hr class="hr-login"></div>
             <div class="col-md-4"> <h3 class="travela-login"> <center>Travela</center> </h3></div>
              <div class="col-md-4"><hr class="hr-login"></div>
              
          </div>  				
           <?= $this->session->flashdata('message') ?>
  				<form method="post" action="<?= base_url() ?>auth/index">
				  <div class="form-group login-form">
				    <label for="formGroupExampleInput">Username</label>
				    <input type="text" class="form-control " id="formGroupExampleInput" placeholder="Masukan Username" name="username" value="<?= set_value('email'); ?>">
             <?= form_error('email','<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
				  <div class="form-group login-form">
				    <label for="formGroupExampleInput2">Password</label>

				    <input type="password" class="form-control password" id="password" placeholder="Masukan Password" name="password">
            <input type="checkbox" class="form-checkbox"> Show password
           
           
             <?= form_error('password','<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
				  <center> <button type="submit" class="btn btn-lg btn-primary"> Masuk</button> </a></center>
          <div class="row justify-content-center belumpunyaakun">
            <div class="col-md-12">
              <center><hr><h4>Belum Punya Akun? <a href="<?= base_url() ?>auth/registration">Daftar Disini</a></h4></center>
            </div>
          </div>
				</form>
  			</div>
  			<div class="col-md-4">
  			</div>
  		</div>
  	</div>
   

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="<?= base_url() ?>assets/bootstrap/js/myscript.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
