

            <div class="container-fluid">
             <div class="row content">
            <div class="col-sm-12">
                <div class="row justify-content-center">         
                    <div class="col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Petugas</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Verivikasi</li>
                          </ol>
                        </nav>
                    </div>
                </div>                
                <div class="row">
                     <div class="col-sm-1"></div>
        <div class="col-sm-10 greycontainer">
          <table class="table table-hover datatable">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Kode Pemesanan</th>
                      <th scope="col">Tanggal Pemesanan</th>
                      <th scope="col">Total Bayar</th>
                      <th scope="col">tempat Pemesanan</th>
                      <th scope="col">Upload</th>
                       <th scope="col">Status</th>  
                      <th scope="col">Tanggal</th>
                    </tr>
                  </thead>
                  <tbody>
                     <?php $i=1;
                     foreach ($verivikasi as $row) :?>
                   <tr>
                      <th scope="row"> <?= $i ?></th>
                      <td><?= $row['kode_pemesanan'] ?></td>
                      <td><?= $row['tgl_pemesanan'] ?></td>
                      <td><?= $row['total_bayar'] ?></td>
                      <td><?= $row['tempat_pemesanan'] ?></td>
                      <td>
                        <?php if ($row['bukti_pembayaran'] != null): ?>
                           <button class="btn btn-primary lihat-file" id="<?= base_url('assets/img/bukti_pembayaran/'.$row['bukti_pembayaran']) ?>">Lihat File</button>
                        <?php endif ?>
                       </td>
                      <td><?php if ($row['status'] =='verived'): ?>
                        <button class="btn btn-success"> verived</button>
                          <?php else: ?><button class="btn btn-danger"> unveriv</button>
                        <?php endif ?>
                      </td>    
                      <td class="icon-table" >
                        <a href="<?= base_url('petugas/updatepemesanan'). '/'.$row['id_pemesanan'] ?>" class="tombol-veriv"><img src="<?= base_url() ?>/assets/bootstrap/img/check.png" style="width: 25px;"></a>
                       
                        
                      </td>
                    </tr>
                  <?php $i++;
                  endforeach; ?>
                  
                  </tbody>
                </table>
        </div>
      </div>
         
        </div>
              

                      </div>
                    </div>
                  </div> 
                </div>

