<!DOCTYPE html>
<html lang="en">
<head>
  <title>Travela Petugas</title>
  <link rel="shortcut icon" href="favicon.png">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../../../assets/bootstrap/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <style>
   .navbar {
    background-color: #343A40;
      margin-bottom: 0;
      border-radius: 0;
    }

    .navbar-header .navbar-brand {
      color: #ffffff;
    }
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 100vh}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #212528;
      height: 100%;
    }

   .nav-stacked {
    margin-top: 20px;
    background-color: #212528;
   }

   .nav-stacked li a{
    color: #ffffff;
   }

   .nav-stacked li a:hover{
    background-color:#343A40;
   }

    .icon-nav{
      width: 20px;
    }

    .keteranganhalaman{
      margin-top: 20px;
      background-color: #C4C4C4;
    }

    .breadcrumb{
      margin-top: 20px;
    }

    .card{
      border-radius: 5px;
    }
    .card-body {
    -webkit-box-flex: 1;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    padding: 1.25rem;
  }
  .card-footer {
    padding: 0.75rem 1.25rem;
    background-color: rgba(0, 0, 0, 0.03);
    border-top: 1px solid rgba(0, 0, 0, 0.125);
}
.z-1 {
    z-index: 1;
}
.float-left {
    float: left !important;
    padding-left: 10px;
}
.float-right{
  float: right !important;
  padding-right: 10px;
}
.text-white {
    color: #fff !important;
}
.bg-merah{
  background-color: #D22F19;
}

.greycontainer{
  background-color: #f5f5f5;
  border-radius: 5px;
}
    
.table-hover tbody tr:hover{
  background-color: white;
}
.icon-table img{
  width: 20px;
}

.tambah{
  margin-top: 20px;
}


    /* Set black background color, white text and some padding */
    footer {
      background-color: #343A40;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Travela</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">      
      <ul class="nav navbar-nav navbar-right">
        <li>
          <form class="navbar-form navbar-left" action="/action_page.php">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </div>
            </div>
          </form>
        </li>
        <li><a href="#"><img src="../../../assets/bootstrap/img/chat.png" class="icon-nav"></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-user"></span></a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">

    <div class="col-sm-3 sidenav">      
      <ul class="nav nav-pills nav-stacked">
        <li ><a href="Dashboard.php">Dashboard</a></li>
      
        <li ><a href="verivikasi.php"><img src="../../../assets/bootstrap/img/validasi.png" class="icon-nav"> Verivikasi</a></li>
        <li class="active"><a href="laporan.php"><img src="../../../assets/bootstrap/img/laporan.png" class="icon-nav"> Laporan</a></li>
         <li><a href="exampleModal2" data-toggle="modal" data-target="#exampleModal2"><img src="../../../assets/bootstrap/img/logout.png" class="icon-nav"> Logout</a></li>
      </ul><br>      
    </div>

    <div class="col-sm-9">      
      <div class="row justify-content-center">         
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
             <li class="breadcrumb-item"><a href="#">Admin</a></li>
             <li class="breadcrumb-item active" aria-current="page">Verivikasi</li>
           </ol>
         </nav>
        </div>
      </div>
      <div class="row justify-content-center">
       <div class="col-xl-3 col-sm-4 mb-3">
              <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">  
                  <div class="mr-5">Pendapatan Perbulan</div>                  
                </div>
                <div class="card-footer text-white">
                  <span class=""><a href="pendapatanperbulan.php">View Details<a></span> 
                </div>
             </div>  
            </div>    
            <div class="col-xl-3 col-sm-4 mb-3">
              <div class="card text-white bg-merah o-hidden h-100">
                <div class="card-body">  
                  <div class="mr-5">Penumpang Hari Ini</div>                  
                </div>
                <div class="card-footer text-white">
                  <span class=""><a href="listpenumpang.php">View Details<a></span> 
                </div>
             </div>  
            </div> 
           <div class="col-xl-3 col-sm-4 mb-3">
              <div class="card text-white bg-primary o-hidden h-100">
                <div class="card-body">  
                  <div class="mr-5">Laporan Keberangkatan</div>                  
                </div>
                <div class="card-footer text-white">
                  <span class=""><a href="laporankeberangkatan.php">View Details<a></span> 
                </div>
             </div>  
            </div> 
        </div>
     </div>
    </div>
  </div> 
</div>
  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Peringatan !</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Yakin akan menghapus ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Yakin </button>
      </div>
    </div>
  </div>
</div>
    </div>
  </div> 
</div>
 <!-- logout Modal -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Yakin ingin keluar ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="button" class="btn btn-primary">Yakin </button>
      </div>
    </div>
  </div>
</div>
  </div> 
</div>
<footer class="container-fluid">
  <p class="text-center">Copyright Travela 2019</p>
</footer>

</body>
</html>
