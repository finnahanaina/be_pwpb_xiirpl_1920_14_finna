

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Laporan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" />
   
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js" "></script>
    <script src="<?= base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
</head>
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
        padding: 20px;
    }
</style>
<body style="min-height: 1000px;">
    <div class="container-fluid" style="min-height: 500px;">
        <div class="row">
            <div class="col-md-12" style="background: #007bff; border-bottom: 15px solid #007bff; border-top: 15px solid #007bff;">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h4 class="m-3" style="font-size: 3em;padding: 20px;"><strong>Travela</strong></h4>
            </div>
            <div class="col-md-6 text-right" style="padding: 20px;font-size: 1em;">
                 <p>Kantor Pusat Jakarta</p>
                <p>+62 888 455 6677</p>
                <p>travela@gmail.com</p>
            </div>
        </div>
        <hr>
        <div class="row justify-content-center" >
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top: 40px;">
                <center><h1><?= $ket ?></h1></center>
                   <div class="row">
            <div class="col-md-12 greycontainer mb-5 p-5">
                <table class="table">
                    <thead class="thead-light bg-primary">
                       <tr>
                          <th scope="col">#</th>
                          <th scope="col">kode rute</th>
                          <th scope="col">kode Transportasi</th>
                          <th scope="col">Tipe</th>
                          <th scope="col">Rute Awal</th>
                          <th scope="col">rute akhir</th>
                          <th scope="col">Tanggal</th> 
                          <th scope="col">Jam</th>
                          <th scope="col">Checkin</th>                       
                        </tr>
                      </thead>
                     <tbody> 
                        <?php
                        if( ! empty($laporan)){
                          $no = 1;
                          foreach($laporan as $data){
                                $tgl = date('d-m-Y', strtotime($data->tgl_berangkat));
                                
                            echo "<tr>";
                            echo "<td>".$tgl."</td>";
                            echo "<td>".$data->kode_rute."</td>";
                            echo "<td>".$data->kode_transportasi."</td>";
                            echo "<td>".$data->nama_tipe."</td>";
                            echo "<td>".$data->rute_awal."</td>";
                            echo "<td>".$data->rute_akhir."</td>";
                            echo "<td>".$data->tgl_berangkat ."</td>";
                            echo "<td>".$data->jam_berangkat."</td>";
                            echo "<td>".$data->jam_cekin."</td>";
                            echo "</tr>";
                            $no++;
                          }
                        } else{
                          echo "<tr>
                                  <td colspan='9'><center> Data Tidak Ditemukan : ( </center><td>
                               </tr>";
                        }
                        ?>
                      </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </div>
</style>

</div>
<script >
   window.print();
  </script>
</body>
</html>