<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('Admin/index') ?>">Admin</a></li>
                 <li class="breadcrumb-item"><a href="<?= base_url('kelola_petugas/index') ?>">Kelola Petugas</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
              </ol>
            </nav>
        </div>
      </div>                
               
      <div class="row">
        <div class="col-sm-12">      
          <center><h3 class="judul">Tambah Petugas</h3></center>
          <form action="<?= base_url() ?>kelola_petugas/aksi_add" method="post" enctype="multipart/form-data">
            <div class="row justify-content-center">
              <div class="col-sm-1"></div>
              <div class="col-sm-10 greycontainer-form">
                <div class="form-group">
                  <label for="koderute">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama petugas"required>
                </div>  
                
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="username" class="form-control" id="username" name="username" placeholder="Username petugas" required>              
                </div>    
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                </div>  
              </div>
            </div>
            <div class="row justify-content-center mt-4" style="margin-bottom: 20px">        
              <div class="col-sm-6"></div> 
                <div class="col-sm-4 text-right edit-rute-tombol">
                  <a href="<?= base_url() ?>kelola_petugas/index">
                    <button type="button" class="btn btn-danger">Batal</button>
                  </a>
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
 <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>



