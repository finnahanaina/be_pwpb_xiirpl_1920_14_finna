<div class="container-fluid">
  <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <div class="row content">
      <div class="col-sm-12">
        <div class="row justify-content-center">         
          <div class="col-sm-12">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>"><?= $level ?></a></li>
                  <li class="breadcrumb-item active" aria-current="page"><?= $statushalaman ?></li>
                </ol>
              </nav>
          </div>
        </div>                
        <div class="row">       
          <div class="col-md-2 text-right tambah">
            <a href="<?= base_url() ?>admin/formtambahrute"><button class="btn btn-primary"><i class="fas fa-plus "></i>Tambah Rute</button></a>
          </div>
        </div>
        <div class="row justify-content-center mt-3 mb-3 p-3">    
          <div class="col-sm-12 greycontainer">
            <table class="table table-hover datatable ">
              <thead class="thead-light">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Tipe</th>
                  <th scope="col">Kode</th>
                  <th scope="col">Asal</th>
                  <th scope="col">Tujuan</th>
                  <th scope="col">tanggal</th> 
                  <th scope="col">Jam berangkat</th>
                  <th scope="col">Checkin</th>
                  <th scope="col">Aksi</th>                      
                </tr>
              </thead>
              <tbody>
                 <?php $i=1;
                  foreach ($rute as $row) : ?>
                <tr>
                  <th scope="row"><?= $i ?></th>
                  <td><?= $row['nama_tipe'] ?></td>
                  <td><?= $row['kode_transportasi'] ?></td>
                  <td><?= $row['rute_awal'] ?></td>
                  <td><?= $row['rute_akhir'] ?></td>   
                  <td><?= date('d F Y', strtotime($row['tgl_berangkat'])); ?></td>  
                  <td><?= $row['jam_berangkat'] ?></td> 
                  <td><?= $row['jam_cekin'] ?></td>
                  <td class="icon-table">
                   <a href="<?= base_url('admin/editrute/') . $row['id_rute']  . '/' .$row['id_transportasi']?>"> <img src="<?= base_url() ?>assets/bootstrap/img/edit.png"></a>
                    <a class="tombol-hapus" href="<?= base_url('admin/hapusrute/') . $row['id_rute'] ?>" ><img src="<?= base_url() ?>assets/bootstrap/img/hapus.png"></a>
                  </td>       
                </tr>
                <?php $i++;endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
              </div>
            </div>
          </div>
        </div> 
      </div>

