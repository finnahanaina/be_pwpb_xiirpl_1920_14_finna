
            <div class="container-fluid">
               <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
             <div class="row content">
            <div class="col-sm-12">
                <div class="row justify-content-center">         
                    <div class="col-sm-12">
                        <nav aria-label="breadcrumb">
                          <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Admin</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Verivikasi</li>
                          </ol>
                        </nav>
                    </div>
                </div>                
                <div class="row">
                     <div class="col-sm-1"></div>
        <div class="col-sm-10 greycontainer">
          <table class="table table-hover">
                  <thead class="thead-light">

                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Kode Pemesanan</th>
                      <th scope="col">Tanggal Pemesanan</th>
                      <th scope="col">Total Bayar</th>
                      <th scope="col">tempat Pemesanan</th>
                      <th scope="col">Upload</th> 
                      <th scope="col">Status</th>  
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($verivikasi==null): ?>
                      <tr><td colspan="8" class="text-center">Tidak Ada Data :(</td></tr>
                    <?php endif ?>
                    <?php $i=1;foreach ($verivikasi as $row) :?>
                   <tr>
                      <th scope="row"><?= $i ?></th>
                      <td><?= $row['kode_pemesanan'] ?></td>
                      <td><?= $row['tgl_pemesanan'] ?></td>
                      <td><?= $row['total_bayar'] ?></td>
                      <td><?= $row['tempat_pemesanan'] ?></td>
                      <td><button class="btn btn-primary lihat-file" id="<?= base_url('assets/img/bukti_pembayaran/'.$row['bukti_pembayaran']) ?>">Lihat File</button></td>
                      <td><?php if ($row['status'] =='verived'): ?>
                        <button class="btn btn-success"> verived</button>
                          <?php else: ?><button class="btn btn-danger"> unveriv</button>
                        <?php endif ?>
                      </td>    
                      <td class="icon-table">
                        <a href="<?= base_url('admin/updatepemesanan'). '/'.$row['id_pemesanan'] ?>" class="tombol-veriv"><img src="<?= base_url() ?>/assets/bootstrap/img/check.png"></a>
                       
                      
                      </td>
                    </tr>
                  <?php $i++; endforeach; ?>
                    </tr>
                  </tbody>
                </table>
        </div>
      </div>
         </div>
               

                      </div>
                    </div>
                  </div> 
                </div>
           


