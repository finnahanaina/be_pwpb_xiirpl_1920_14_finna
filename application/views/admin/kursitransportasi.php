

<div class="container-fluid">
 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 <script>
   
 </script>
<div class="row content">
<div class="col-md-12">
  <div class="row justify-content-center">         
      <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
               <li class="breadcrumb-item"><a href="<?= base_url('admin/Transportasi') ?>">Transportasi</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= $statushalaman ?></li>
            </ol>
          </nav>
      </div>
  </div>           
 <!--  <div class="row ">       
    <div class="col-md-2 float-right text-right tambah">
      <a href="<?= base_url() ?>admin/formtambahtransportasi">
        <button class="btn btn-primary"> <i class="fas fa-plus "></i> Tambah Transportasi</button>
      </a> 
    </div>
  </div>      -->
  <div class="row  mb-5 mt- p-2">
                     
    <div class="col-md-7 greycontainer mt-3">
      <table class="table table-hover table-transportasi datatable">
        <thead class="thead-light">
          <tr>
            <th scope="col">No</th>
            <th scope="col">Bagian</th>
            <th scope="col">Nomor Kursi</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php  $i=1;
           foreach ($kursi as $row ):
               ?>
          <tr>

            <th scope="row"><?= $i ?></th>
            <td> <center><?= $row['bagian'] ?></center></td>
            <td><center><?= $row['kursi'] ?></center></td>
           
            <td class="icon-table">
              <!-- <button class="btn btn-primary">Delete Kursi</button> -->
              <a class="tombol-hapus" href="<?= base_url(); ?>admin/hapuskursi/<?= $row['id']?>/<?= $row['id_transportasi']?>">
                <img src="<?= base_url() ?>assets/bootstrap/img/hapus.png">
              </a>
            </td>
          </tr>
         
          <?php $i++;
                endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="col-md-5">
      <div class="card">
        <div class="card-header text-center">Tambah Bagian</div>
        <div class="card-body">
          <form action="<?= base_url('admin/tambahkursi/'.$id_transportasi_kursi) ?>" method="post">
            <div class="row">
              <div class="col-md-8">
                <label>Nama Bagian</label>
                <input type="text" name="bagian" class="form-control">
              </div>
              <div class="col-md-4">
                <label>Jumlah</label>
                <input type="number" name="kursi" class="form-control">
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-md-12">
                <button class="btn btn-primary btn-block">Submit</button>
              </div>
            </div>
          </form>
         </div>
        </div>
      </div>
    </div>
  </div> 
</div>

       
      
       

                      </div>
                    </div>
                  </div> 
                </div>
               

                 <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script> -->
