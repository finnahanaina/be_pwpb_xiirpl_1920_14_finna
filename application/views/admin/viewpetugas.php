
<div class="container-fluid">
   <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
              <li class="breadcrumb-item active" aria-current="page">Kelola Petugas</li>
            </ol>
          </nav>
        </div>
      </div>  
       <div class="row">       
          <div class="col-md-2 text-right tambah">
            <a href="<?= base_url() ?>kelola_petugas/add"><button class="btn btn-primary"><i class="fas fa-plus "></i>Tambah Petugas</button></a>
          </div>
        </div>
        <div class="row justify-content-center mt-3 mb-5 p-3">    
          <div class="col-sm-12 greycontainer">
            <table class="table table-hover datatable">
              <thead class="thead-light">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nama</th>
                  <th scope="col">Username</th>  
                  <th scope="col">Action</th>              
                </tr>
              </thead>
              <tbody>
                <?php $i=1;foreach ($petugas as $row ) : ?>
                 <tr>
                  <td><?= $i ?></td>
                   <td><?= $row->nama_petugas ?></td>
                   <td><?= $row->username ?></td>
                   <td>
                    <a href="<?= base_url('kelola_petugas/edit/'.$row->id_petugas) ?>">
                     <button class="btn btn-info"><i class="fa fa-edit"></i></button>
                    </a>
                    <a class="tombol-hapus" href="<?= base_url('kelola_petugas/delete/'.$row->id_petugas) ?>">
                      <button class="btn btn-danger"><i class="fa fa-trash"></i></button>
                    </a> 
                   </td>
                 </tr>
               <?php $i++;endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>    
  </div>
</div>

        </div>
      </div> 
    </div>

              


