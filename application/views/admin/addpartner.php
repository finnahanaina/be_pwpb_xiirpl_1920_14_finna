<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('partner/index') ?>">Partner</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
              </ol>
            </nav>
        </div>
      </div>                
               
      <div class="row">
        <div class="col-sm-12">      
          <center><h3 class="judul">Tambah Partner</h3></center>
          <form action="<?= base_url() ?>partner/aksi_add" method="post" enctype="multipart/form-data">
            <div class="row justify-content-center">
              <div class="col-sm-1"></div>
              <div class="col-sm-10 greycontainer-form">
                <div class="form-group">
                  <label for="koderute">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Partner"required>
                </div>  
                <div class="form-group">
                  <label for="kode">Keterangan</label>
                  <select class="form-control" id="keterangan" name="keterangan" required>
                    <option>Transportasi</option>
                    <option>Gerai Retail</option>
                    <option>Pembayaran Instan</option>                      
                  </select>
                </div>
                <div class="form-group">
                  <label for="file">Thumbnail</label>
                  <input type="file" class="form-control" id="thumbnail" name="thumbnail" required>              
                </div>    
                <div class="form-group">
                  <label for="akhir">Biaya Transaksi</label>
                  <input type="number" class="form-control" id="biayatransaksi" name="biayatransaksi" placeholder="Isi Jika Keterangan Bukan merupakan Transportasi">
                </div>  
              </div>
            </div>
            <div class="row justify-content-center mt-4" style="margin-bottom: 20px">        
              <div class="col-sm-6"></div> 
                <div class="col-sm-4 text-right edit-rute-tombol">
                  <a href="<?= base_url() ?>partner/index">
                    <button type="button" class="btn btn-danger">Batal</button>
                  </a>
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
 <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>



