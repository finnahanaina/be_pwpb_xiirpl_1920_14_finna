<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PDF</title>
    <title>Travela Admin</title>
    <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min1.css">
      <!-- Datatables -->
    <link rel="stylesheet" href="<?= base_url('assets/vendor/datatables/datatables.min.css') ?>">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style4.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/styleadmin.css">
    
    <link rel="stylesheet" href="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.css'); ?>" /> <!-- Load file css jquery-ui -->
    <script src="<?php echo base_url('assets/bootstrap/js/jquery.min.js'); ?>"></script> <!-- Load file jquery -->
    <script defer src="<?= base_url() ?>assets/bootstrap/js/solid.js" ></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
    <script defer src="<?= base_url() ?>assets/js/customajax.js"></script>
</head>

<body style="max-width: 100%;">
    <div class="wrapper" style="margin-right: 0!important;margin-left: 0!important;">
        <!-- Sidebar  -->
        <nav id="sidebar" >
            <div class="sidebar-header">
                <h3>Travela</h3>
            </div>

             <ul class="list-unstyled components">
                <li >
                   <a href="<?= base_url() ?>admin/index">
                        <i class="fas fa-briefcase"></i>
                        dashboard
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/transportasi">
                        <i class="fas fa-plane"></i>
                        Transportasi
                    </a>
                    
                </li>
                <li > 
                    <a href="<?= base_url() ?>admin/rute">
                        <i class="fas fa-train"></i>
                        Rute
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/verivikasi">
                        <i class="fas fa-check"></i>
                        verivikasi
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>partner/index">
                        <i class="fas fa-handshake"></i>
                        Partner
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>kelola_petugas/index">
                        <i class="fas fa-user"></i>
                        Petugas
                    </a>
                </li>
                <li>
                    <a href="<?= base_url() ?>admin/laporan">
                        <i class="fas fa-file"></i>
                        Laporan
                    </a>
                </li>
                <li>
                    <a  href="<?= base_url() ?>admin/logout" class="logout">
                        <i class="fas fa-sign-out-alt"></i>
                        logout
                    </a>
                </li>
            </ul>                
        </nav>
<!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-inverse bg-dark navbaradmin" style="margin-right: 1px!important;">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="btn btn-primary">
                        <i class="fas fa-align-left"></i>                     
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                </div>
            </nav>
            <div class="fluid-container">
               <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
                    <div class="row content">
                        <div class="col-sm-12">
                            <div class="row justify-content-center">         
                                <div class="col-sm-12">
                                    <nav aria-label="breadcrumb">
                                      <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#">Admin</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Laporan pendapatan</li>
                                      </ol>
                                    </nav>
                                </div>
                            </div>  
                            <div class="row justify-content-center">
                                <center><h4><?php echo $ket; ?></h4></center>
                            </div>              

                        <div class="row mt-3">
                            <div class="col-sm-3">
                                <form method="get" action="">
                                <label>Filter Berdasarkan</label><br>
                                <select name="filter" id="filter" class="form-control">
                                    <option value="">Pilih</option>
                                    <option value="1">Per Tanggal</option>
                                    <option value="2">Per Bulan</option>
                                    <option value="3">Per Tahun</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <div id="form-tanggal">
                                    <label>Tanggal</label><br>
                                    <input type="text" name="tanggal" class="input-tanggal form-control" />
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div id="form-bulan">
                                    <label>Bulan</label><br>
                                    <select name="bulan" class="form-control">
                                        <option value="">Pilih</option>
                                        <option value="1">Januari</option>
                                        <option value="2">Februari</option>
                                        <option value="3">Maret</option>
                                        <option value="4">April</option>
                                        <option value="5">Mei</option>
                                        <option value="6">Juni</option>
                                        <option value="7">Juli</option>
                                        <option value="8">Agustus</option>
                                        <option value="9">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div id="form-tahun">
                                    <label>Tahun</label><br>
                                    <select name="tahun" class="form-control">
                                        <option value="">Pilih</option>
                                        <?php
                                        foreach($option_tahun as $data){ // Ambil data tahun dari model yang dikirim dari controller
                                            echo '<option value="'.$data->tahun.'">'.$data->tahun.'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-sm-6 offset-7">
                              <button type="submit" class="btn btn-primary">Tampilkan</button>
                                <a href="<?php echo base_url('laporan/index'); ?>">
                                    <button type="button" class="btn btn-danger">Reset Filter</button>
                                </a>
                                <a href="<?php echo $url_cetak; ?>">
                                    <button class="btn btn-primary">CETAK PDF</button>
                                </a>
                            </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 greycontainer mb-5 p-5">
                                <table class="table table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                          <th scope="col">Tanggal Pemesanan</th>
                                          <th scope="col">kode pemesanan</th>                      
                                          <th scope="col">Rute Awal</th> 
                                          <th scope="col">Status</th>
                                          <th scope="col">Total Bayar</th>                       
                                        </tr>
                                      </thead>
                                      <tbody> 
                                        <?php
                                        if( ! empty($laporan)){
                                          $no = 1;
                                          foreach($laporan as $data){
                                                $tgl = date('d-m-Y', strtotime($data->tgl_pemesanan));
                                                
                                            echo "<tr>";
                                            echo "<td>".$tgl."</td>";
                                            echo "<td>".$data->kode_pemesanan."</td>";
                                            echo "<td>".$data->tempat_pemesanan."</td>";
                                            echo "<td>".$data->status."</td>";
                                            echo "<td>".$data->total_bayar."</td>";
                                            echo "</tr>";
                                            $no++;
                                          }
                                        }
                                        ?>
                                      </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</body>
<footer class="container-fluid">
  <p class="text-center">Copyright Travela 2019</p>
</footer>
    
    <script src="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.js'); ?>"></script> <!-- Load file plugin js jquery-ui -->
    <script>
    $(document).ready(function(){ // Ketika halaman selesai di load
        $('.input-tanggal').datepicker({
            dateFormat: 'yy-mm-dd' // Set format tanggalnya jadi yyyy-mm-dd
        });

        $('#form-tanggal, #form-bulan, #form-tahun').hide(); // Sebagai default kita sembunyikan form filter tanggal, bulan & tahunnya

        $('#filter').change(function(){ // Ketika user memilih filter
            if($(this).val() == '1'){ // Jika filter nya 1 (per tanggal)
                $('#form-bulan, #form-tahun').hide(); // Sembunyikan form bulan dan tahun
                $('#form-tanggal').show(); // Tampilkan form tanggal
            }else if($(this).val() == '2'){ // Jika filter nya 2 (per bulan)
                $('#form-tanggal').hide(); // Sembunyikan form tanggal
                $('#form-bulan, #form-tahun').show(); // Tampilkan form bulan dan tahun
            }else{ // Jika filternya 3 (per tahun)
                $('#form-tanggal, #form-bulan').hide(); // Sembunyikan form tanggal dan bulan
                $('#form-tahun').show(); // Tampilkan form tahun
            }

            $('#form-tanggal input, #form-bulan select, #form-tahun select').val(''); // Clear data pada textbox tanggal, combobox bulan & tahun
        })
    })
    </script>
</table>
</body>
</html>
