<div class="container-fluid">
   <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('partner/index') ?>">Partner</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
        </div>
      </div>                
               
      <div class="row">
        <div class="col-sm-12">      
          <center><h3 class="judul">Edit Partner</h3></center>
          <form action="<?= base_url() ?>partner/aksi_edit" method="post" enctype="multipart/form-data">
            <div class="row justify-content-center">
              <div class="col-sm-1"></div>
              <div class="col-sm-10 greycontainer-form">
                <div class="form-group">
                  <label for="koderute">Nama</label>
                   <input type="hidden" id="id" name="id" value="<?= $partner->id ?>">
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Partner" value="<?= $partner->nama ?>" required>
                </div>  
                <div class="form-group">
                  <label for="kode">Keterangan</label>
                  <select class="form-control" id="keterangan" name="keterangan" required>
                    <option selected="<?= $partner->keterangan ?>"><?= $partner->keterangan ?></option>     
                    <option>Transportasi</option>                      
                    <option>Gerai Retail</option> 
                    <option>Pembayaran Instan</option>                                         
                  </select>
                </div>
                <div class="form-group">
                  <label for="file">Thumbnail</label>
                  <div class="row">
                    <div class="col-sm-6">
                      <img src="<?= base_url('assets/img/partner/'.$partner->gambar) ?>" width="200">
                    </div>
                    <div class="col-sm-6">
                      <label>Update Thumbnail</label>
                      <input type="hidden" class="form-control"name="thumbnail2" value="<?= $partner->gambar ?>"> 
                      <input type="file" class="form-control" id="thumbnail" name="thumbnail"> 
                    </div>
                  </div>
                </div>    
                <div class="form-group">
                  <label for="akhir">Biaya Transaksi</label>
                  <input type="number" class="form-control" id="biayatransaksi" name="biayatransaksi"value="<?= $partner->biaya_transaksi ?>">
                </div>  
              </div>
            </div>
            <div class="row justify-content-center mt-4" style="margin-bottom: 20px">        
              <div class="col-sm-6"></div> 
                <div class="col-sm-4 text-right edit-rute-tombol">
                  <a href="<?= base_url() ?>partner/index">
                    <button type="button" class="btn btn-danger">Batal</button>
                  </a>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
 <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>



