<div class="container-fluid">
  <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
    <div class="row content">
      <div class="col-sm-12">
        <div class="row justify-content-center">         
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                     <li class="breadcrumb-item"><a href="<?= base_url('admin/transportasi') ?>">Transportasi</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                  </ol>
                </nav>
            </div>
        </div>                
        <div class="row">
          <div class="col-sm-12">   
            <div class="row justify-content-center">
              <center><h3 class="judul">Edit Transportasi</h3></center>
                <div class="col-sm-1"></div>
                <div class="col-sm-10 greycontainer-form">
                  <form action="<?= base_url() ?>admin/updatetransportasi" method="POST">
                    <div class="form-group">
                      <?php foreach ($transportasi as $t) { ?>  
                        <label for="tipe">Tipe Transportasi</label>
                        <?php foreach ($tipe as $value) : ?>
                          <select class="form-control" id="tipe" name="tipe">
                            <option selected value="<?= $value['id_tipe_transportasi']; ?>">
                              <?php echo $value['nama_tipe']; ?>
                            </option>
                        <?php endforeach; ?>
                        <?php foreach ($tipetransportasi as $value) : ?>
                          <option value="<?php echo $value['id_tipe_transportasi']; ?>">
                            <?php echo $value['nama_tipe']; ?>
                          </option>
                        <?php endforeach; ?>
                          </select>
                    </div>     
                    <div class="form-group">
                      <label for="kode">Kode Transportasi</label>
                        <input type="hidden" name="id_transportasi" value="<?php echo $t['id_transportasi'] ?>">
                        <input type="text" class="form-control" id="kode" name="kode" value="<?php echo $t['kode_transportasi'] ?>">
                    </div>
                    <div class="form-group">
                      <label for="kursi_ekonomi">Kursi Ekonomi</label>
                      <input type="number" class="form-control" id="kursi_ekonomi" name="kursi_ekonomi" value="<?php echo $t['kursi_ekonomi'] ?>">
                    </div>  
                    <div class="form-group">
                      <label for="kursi_bisnis">Kursi Bisnis</label>
                      <input type="number" class="form-control" id="kursi_bisnis" name="kursi_bisnis" value="<?php echo $t['kursi_bisnis'] ?>">
                    </div>      
                   
                      <?php } ?>
                </div>
            </div>
            <div class="row justify-content-center" style="margin-bottom: 20px;">        
              <div class="col-sm-6"></div> 
                <div class="col-sm-4 text-right edit-rute-tombol">
                <a href="<?= base_url() ?>admin/transportasi"><button type="button" class="btn btn-danger">Batal</button></a>
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </div>
            </div>
                </form>
          </div>
        </div>
      </div>
    </div> 
</div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>

