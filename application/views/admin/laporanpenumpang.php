
<div class="container-fluid">
   <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
  <div class="row content">
    <div class="col-md-12">
      <div class="row justify-content-center">         
        <div class="col-md-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url('admin/laporan') ?>">Laporan</a></li>
              <li class="breadcrumb-item active" aria-current="page">Penumpang</li>
            </ol>
          </nav>
        </div>
      </div>  
       <div class="row justify-content-center">       
          <center><h4><?php echo $ket; ?></h4></center>
        </div>
        <div class="row mt-3">
            <div class="col-md-3">
                <form method="get" action="">
                <label>Filter Berdasarkan</label><br>
                <select name="filter" id="filter" class="form-control">
                    <option value="">Pilih</option>
                    <option value="1">Per Tanggal</option>
                    <option value="2">Per Bulan</option>
                    <option value="3">Per Tahun</option>
                </select>
            </div>
            <div class="col-md-3">
                <div id="form-tanggal">
                    <label>Tanggal</label><br>
                    <input type="text" name="tanggal" class="input-tanggal form-control" />
                </div>
            </div>
            <div class="col-md-3">
                <div id="form-bulan">
                    <label>Bulan</label><br>
                    <select name="bulan" class="form-control">
                        <option value="">Pilih</option>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div id="form-tahun">
                    <label>Tahun</label><br>
                    <select name="tahun" class="form-control">
                        <option value="">Pilih</option>
                        <?php
                        foreach($option_tahun as $data){ // Ambil data tahun dari model yang dikirim dari controller
                            echo '<option value="'.$data->tahun.'">'.$data->tahun.'</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-6 offset-6 text-right">
              <button type="submit" class="btn btn-primary">Tampilkan</button>
                <a href="<?php echo base_url('laporan/indexlaporanpenumpang'); ?>">
                    <button type="button" class="btn btn-danger">Reset Filter</button>
                </a>
                <a href="<?php echo $url_cetak; ?>">
                    <button type="button" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
                </a>
                <a href="<?php echo $url_excel; ?>">
                    <button type="button" class="btn btn-primary"><i class="fa fa-download"></i> Export Excel</button>
                </a>
            </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 greycontainer mb-5 p-5">
                <table class="table table-hover">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">Tanggal</th>
                          <th scope="col">nama</th>
                          <th scope="col">nik</th>
                          <th scope="col">asal</th>
                          <th scope="col">Tujuan</th>  
                          <th scope="col">Kode Pemesanan</th> 
                        </tr>
                      </thead>
                      <tbody> 
                        <?php
                        if( ! empty($laporan)){
                          $no = 1;
                          foreach($laporan as $data){
                                $tgl = date('d-m-Y', strtotime($data->tgl_pemesanan));
                                
                            echo "<tr>";
                            echo "<td>".$tgl."</td>";
                            echo "<td>".$data->nama_penumpang."</td>";
                            echo "<td>".$data->nik."</td>";
                            echo "<td>".$data->rute_awal."</td>";
                            echo "<td>".$data->kode_rute."</td>";
                            echo "<td>".$data->kode_pemesanan."</td>";
                            
                            echo "</tr>";
                            $no++;
                          }
                        } else{
                          echo "<tr>
                                  <td colspan='9'><center> Data Tidak Ditemukan : ( </center><td>
                               </tr>";
                        }
                        ?>
                      </tbody>
                </table>
            </div>
        </div>

</div>
</div>

        </div>
      </div> 
    </div>



