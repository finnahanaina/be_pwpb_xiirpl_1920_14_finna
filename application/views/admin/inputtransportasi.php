
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('admin/transportasi') ?>">Transportasi</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
              </ol>
            </nav>
        </div>
      </div>                
      <div class="row">
        <div class="col-sm-12">    
          <div class="row justify-content-center">
            <center><h3 class="judul">Tambah Transportasi</h3></center>
            <div class="col-sm-1"></div>
            <div class="col-sm-10 greycontainer-form" >
                <?= $this->session->flashdata('message') ?>
              <form action="<?= base_url() ?>admin/formtambahtransportasi" method="post" id="kursi">
                <div class="form-group">
                  <label for="tipe">Tipe Transportasi</label>
                  <select class="form-control" id="tipe" name="tipe">
                    <?php foreach ($tipe as $value): ?>
                      <option value="<?= $value['id_tipe_transportasi'] ?>"><?= $value['nama_tipe'] ?></option>
                    <?php endforeach ?>
                               
                  </select>
                  <?= form_error('tipe','<small class="text-danger pl-3">', '</small>'); ?>
                </div>     
                <div class="form-group">
                  <label for="kode">Kode Transportasi</label>
                  <input type="text" class="form-control" id="kode" name="kode">
                   <?= form_error('kode','<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <div class="form-group">
                  <label for="kursi_ekonomi">Kursi Ekonomi</label>
                  <input type="number" class="form-control" id="kursi_ekonomi" name="kursi_ekonomi">
                   <?= form_error('kursi_ekonomi','<small class="text-danger pl-3">', '</small>'); ?>
                </div> 
                <div class="form-group">
                  <label for="kuris_bisnis">Kursi Bisnis</label>
                  <input type="number" class="form-control" id="kuris_bisnis" name="kursi_bisnis">
                   <?= form_error('kuris_bisnis','<small class="text-danger pl-3">', '</small>'); ?>
                </div>
                <div class="row mb-3">
                  <div class="col-md-5">
                    <label for="bagian_kursi">Bagian Kursi</label>
                    <input type="text" class="form-control" id="bagian_kursi" name="bagian_kursi[]" placeholder="Contoh 'A' ">
                     <?= form_error('bagian_kursi','<small class="text-danger pl-3">', '</small>'); ?>
                  </div>
                  <div class="col-md-5">
                    <label for="jumlah_kursi">Jumlah Kursi</label>
                    <input type="number" class="form-control" id="jumlah_kursi" name="jumlah_kursi[]" >
                     <?= form_error('jumlah_kursi','<small class="text-danger pl-3">', '</small>'); ?>
                  </div>
                 <!--  <div class="col-md-2 mt-4" >
                    <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                  </div> -->
                </div>      
                <div class="tambah-kursi">
                  
                </div>
                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <select class="form-control" name="keterangan">
                    <?php foreach ($partner as  $keterangan) { ?>
                      <option value="<?= $keterangan['id']?>"><?= $keterangan['nama'] ?></option>
                    <?php } ?>
                  </select>
                   <?= form_error('keterangan','<small class="text-danger pl-3">', '</small>'); ?>
                </div>
            </div>
          </div>
          <div class="row justify-content-center m-5">        
            <div class="col-md-6"></div> 
              <div class="col-md-6 text-right edit-rute-tombol">
                <button type="button" class="btn btn-info button-tambah-kursi">Tambah Kursi</button>
              <a href="<?=base_url() ?>admin/transportasi">
                <button type="button" class="btn btn-danger">Batal</button>
              </a>
                <button type="submit" class="btn btn-primary">Tambah</button>
              </div>
            </div>
          </div>
              </form>
        </div>
      </div>
    </div>
  </div> 
</div>
</div>  

