<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('Admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('kelola_petugas/index') ?>">Kelola Petugas</a></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
              </ol>
            </nav>
        </div>
      </div>                
               
      <div class="row">
        <div class="col-sm-12">      
          <center><h3 class="judul">Edit Petugas</h3></center>
          <form action="<?= base_url() ?>kelola_petugas/aksi_edit" method="post" enctype="multipart/form-data">
            <div class="row justify-content-center" id="petugas">
              <div class="col-sm-1"></div>
              <div class="col-sm-10 greycontainer-form" >
                <div class="form-group">
                  <label for="koderute">Nama</label>
                  <input type="hidden" class="form-control" id="id" name="id" value="<?= $petugas->id_petugas ?>">
                  <input type="text" class="form-control" id="nama" name="nama" value="<?= $petugas->nama_petugas ?>">
                </div>  
                
                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="username" class="form-control" id="username" name="username" value="<?= $petugas->username ?>">
                </div>    
               <div class="ganti-password">
                 
               </div>
                 
              </div>
            </div>
            <div class="row justify-content-center mt-4" style="margin-bottom: 20px">        
              <div class="col-md-5"></div> 
                <div class="col-md-6 text-right edit-rute-tombol">
                   <a href="<?= base_url() ?>kelola_petugas/index">
                    <button type="button" class="btn btn-danger"><i class="fa fa-times"></i> Batal</button>
                  </a>
                   <button type="button" class="btn btn-primary button-ganti-password"><i class="fa fa-cogs"></i> Ganti password</button>
                  <button type="submit" class="btn btn-success">Update</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
 <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>



