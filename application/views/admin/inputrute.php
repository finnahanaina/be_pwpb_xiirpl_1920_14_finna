<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item"><a href="<?= base_url('admin/rute') ?>">Rute</a></li>
                <li class="breadcrumb-item active" aria-current="page">Tambah</li>
              </ol>
            </nav>
        </div>
      </div>                
               
      <div class="row">
        <div class="col-sm-12">      
          <center><h3 class="judul">Tambah Rute</h3></center>
          <form action="<?= base_url() ?>admin/formtambahrute" method="post">
            <div class="row justify-content-center">
              <div class="col-sm-1"></div>
              <div class="col-sm-10 greycontainer-form">
                <div class="form-group">
                  <label for="koderute">Kode Rute</label>
                  <input type="hidden" name="koderute">
                   <input type="text" class="form-control" id="koderute" name="koderute">              
                </div>  
                <div class="form-group">
                  <label for="kode">Kode Transportasi</label>
                  <select class="form-control" id="transportasi" name="transportasi">
                    <?php foreach ($transportasiget as $value) : ?>
                      <option value="<?= $value['id_transportasi']; ?>"><?php echo $value['kode_transportasi']; ?></option>
                    <?php endforeach; ?>                        
                  </select>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="awal">Rute Awal</label>
                    <input type="text" class="form-control" id="awal" name="awal">              
                  </div>    
                  <div class="form-group col-md-6 ">
                    <label for="akhir">Rute akhir</label>
                    <input type="text" class="form-control" id="akhir" name="akhir" >
                  </div>  
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="ekonomi">Harga Ekonomi</label>
                    <input type="text" class="form-control" id="ekonomi" name="ekonomi">              
                  </div>  
                  <div class="form-group col-md-6">
                    <label for="bisnis">Harga Bisnis</label>
                    <input type="text" class="form-control" id="bisnis" name="bisnis">              
                  </div>     
                </div>
                <div class="form-row">
                  <div class="form-group col-md-4">
                    <label for="tgl">Tanggal Berangkat</label>
                    <input type="date" class="form-control" id="tgl" name="tglberangkat" >  
                  </div>
                  <div class="form-group col-md-4">
                    <label for="berangkat">Jam Berangkat</label>
                    <input type="time" class="form-control" id="berangkat" name="jamberangkat">
                  </div>
                  <div class="form-group col-md-4">
                    <label for="cekin">Jam Checkin</label>
                    <input type="time" class="form-control" id="cekin" name="jamcekin">
                  </div>
                </div>
              </div>
            </div>
            <div class="row justify-content-center" style="margin-bottom: 20px">        
              <div class="col-sm-6"></div> 
                <div class="col-sm-4 text-right edit-rute-tombol">
                  <a href="<?= base_url() ?>admin/rute">
                    <button type="button" class="btn btn-danger">Batal</button>
                  </a>
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
 <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>



