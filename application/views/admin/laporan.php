<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= base_url('Admin/index') ?>">Admin</a></li>
              <li class="breadcrumb-item active" aria-current="page">Laporan</li>
            </ol>
          </nav>
        </div>
      </div>    
      <div class="row">                                      
        <div class="col-md-4">
          <a href="<?= base_url() ?>laporan/indexlaporankeberangkatan">
            <div class="card-counter primary">
              <i class="fa fa-train iconcard"></i>
              <span class="count-numbers">12</span>
              <span class="count-name">Laporan Keberangkatan</span>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="<?= base_url() ?>laporan/indexlaporanpenumpang">
            <div class="card-counter danger">
              <i class="fa fa-user iconcard"></i>
              <span class="count-numbers">50</span>
              <span class="count-name">Data Penumpang</span>
            </div>
          </a>
        </div>
        <div class="col-md-4">
          <a href="<?= base_url() ?>laporan/index">
            <div class="card-counter success">       
              <i class="fa fa-handshake   iconcard"></i>
              <span class="count-numbers">4</span>
              <span class="count-name">Pendapatan</span>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
      </div> 
    </div>
