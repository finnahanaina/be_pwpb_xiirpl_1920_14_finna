<?php

header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=".$ket . ".xls");
header("Pragma: no-cache");
header("Expires: 0");

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Laporan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css" />
   
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js" "></script>
    <script src="<?= base_url() ?>bootstrap/js/bootstrap.min.js"></script>
    <script defer src="<?= base_url() ?>assets/bootstrap/js/fontawesome.js"></script>
</head>
<style>
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
        padding: 20px;
    }
</style>
<body style="min-height: 1000px;">
    <div class="container-fluid" style="min-height: 500px;">
        <div class="row">
            <div class="col-md-12" style="background: #007bff; border-bottom: 15px solid #007bff; border-top: 15px solid #007bff;">
            </div>
        </div>
       
        <hr>
        <div class="row justify-content-center" >
            <div class="col-md-1"></div>
            <div class="col-md-10" style="margin-top: 40px;">
                <center><h1><?= $ket ?></h1></center>
                   <div class="row">
            <div class="col-md-12 greycontainer mb-5 p-5">
                <table border="solid 5px;">
                    <thead class="thead-light bg-primary">
                        <tr>
                          <th scope="col">Tanggal</th>
                          <th scope="col">nama</th>
                          <th scope="col">nik</th>
                          <th scope="col">asal</th>
                          <th scope="col">Tujuan</th>  
                          <th scope="col">Kode Pemesanan</th> 
                        </tr>
                      </thead>
                     <tbody> 
                        <?php
                        if( ! empty($laporan)){
                          $no = 1;
                          foreach($laporan as $data){
                                $tgl = date('d-m-Y', strtotime($data->tgl_pemesanan));
                                
                            echo "<tr>";
                            echo "<td>".$tgl."</td>";
                            echo "<td>".$data->nama_penumpang."</td>";
                            echo "<td>".$data->nik."</td>";
                            echo "<td>".$data->rute_awal."</td>";
                            echo "<td>".$data->kode_rute."</td>";
                            echo "<td>".$data->kode_pemesanan."</td>";
                            
                            echo "</tr>";
                            $no++;
                          }
                        } else{
                          echo "<tr>
                                  <td colspan='9'><center> Data Tidak Ditemukan : ( </center><td>
                               </tr>";
                        }
                        ?>
                      </tbody>
                </table>
            </div>
        </div>
            </div>
        </div>
    </div>
</style>
<!-- Invoice Template - END -->

</div>
  
  
  <script >
   window.print();
  </script>

</body>
</html>

