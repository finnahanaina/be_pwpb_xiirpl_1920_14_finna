

<div class="container-fluid">
 <div class="flash-data" data-flashdata="<?= $this->session->flashdata('flash'); ?>"></div>
 <script>
   
 </script>
<div class="row content">
<div class="col-md-12">
  <div class="row justify-content-center">         
      <div class="col-sm-12">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
              <li class="breadcrumb-item active" aria-current="page"><?= $statushalaman ?></li>
            </ol>
          </nav>
      </div>
  </div>           
  <div class="row ">       
    <div class="col-md-2 float-right text-right tambah">
      <a href="<?= base_url() ?>admin/formtambahtransportasi">
        <button class="btn btn-primary"> <i class="fas fa-plus "></i> Tambah Transportasi</button>
      </a> 
    </div>
  </div>     
  <div class="row justify-content-center mb-5 mt- p-2">
                     
        <div class="col-md-12 greycontainer mt-3">
          <table class="table table-hover table-transportasi datatable">
          
                  <thead class="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Tipe</th>
                      <th scope="col">Kode Transportasi</th>
                      <th scope="col">Jumlah kursi</th>
                      <th scope="col">Keterangan</th>
                      <th scope="col">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php  $i=1;
                     foreach ($transportasi as $row ):
                         ?>
                    <tr>
                      <th scope="row"><?= $i ?></th>
                      <td><?= $row['nama_tipe'] ?></td>
                      <td><?= $row['kode_transportasi'] ?></td>
                      <td><?= $row['jumlah_kursi'] ?></td>
                      <td><?= $row['nama'] ?></td>
                      <td class="icon-table">
                        <a href="<?= base_url('admin/kursitransportasi/') . $row['id_transportasi']?>" data-toggle="tooltip" data-placement="top" title="Lihat Kursi">
                           <img src="<?= base_url() ?>assets/bootstrap/img/kursi.png">
                        </a>
                        <a href="<?= base_url('admin/edittransportasi/') . $row['id_transportasi']  . '/' .$row['id_tipe_transportasi']?>"> 
                          <img src="<?= base_url() ?>assets/bootstrap/img/edit.png">
                        </a>
                        <a class="tombol-hapus" href="<?= base_url(); ?>admin/hapustransportasi/<?= $row['id_transportasi']   ?>">
                          <img src="<?= base_url() ?>assets/bootstrap/img/hapus.png">
                        </a>
                      </td>
                    </tr>

                    <?php $i++;
                          endforeach; ?>
                  </tbody>
                </table>
        </div>
</div>
         
        </div>

       
      
       

                      </div>
                    </div>
                  </div> 
                </div>
               

                 <!-- <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script> -->
