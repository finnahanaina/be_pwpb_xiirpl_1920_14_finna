
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-12">
      <div class="row justify-content-center">         
        <div class="col-sm-12">
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('admin/index') ?>">Admin</a></li>
                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
              </ol>
            </nav>
        </div>
      </div>                
    <div class="row">
      <div class="col-md-3">
        <a href="<?= base_url('admin/transportasi') ?>">
          <div class="card-counter primary">
            <i class="fa fa-train iconcard"></i>
            <span class="count-numbers"><?= $jumlah_transportasi ?></span>
            <span class="count-name">Transportasi</span>
          </div>
        </a>
      </div>
      <div class="col-md-3">
        <a href="<?= base_url('admin/rute') ?>">
          <div class="card-counter danger">
            <i class="fa fa-road iconcard"></i>
            <span class="count-numbers"><?= $jumlah_rute ?></span>
            <span class="count-name">Rute</span>
          </div>
        </a>
      </div>  
      <div class="col-md-3">
        <a href="<?= base_url('partner/index') ?>">
          <div class="card-counter success">       
            <i class="fa fa-handshake iconcard"></i>
            <span class="count-numbers"><?= $jumlah_partner ?></span>
            <span class="count-name">Partner</span>
          </div>
        </a>
      </div>
      <div class="col-md-3">
        <a href="<?= base_url('admin') ?>">
          <div class="card-counter info">
            <i class="fa fa-users iconcard"></i>
            <span class="count-numbers"><?= $jumlah_penumpang ?></span>
            <span class="count-name">Penumpang</span>
          </div>
        </a>
      </div>
    </div> 

  <div class="row  col-md-12 p-4 d-flex chart" style="min-height: 200px; margin-top: 50px;margin-bottom: 50PX;">
    <div id="chart" class="bg-white col-md-7" style="min-height: 200px;   background-color: #fff;">
      <label>PEMESANAN</label>
        <canvas id="laporanOrder" style="min-height: 200px; "></canvas>
        <script>
          var getChart1 = document.getElementById('laporanOrder');
          var myChart1 = new Chart(getChart1, {
              type: 'bar',
              data: {
                  labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November'],
                  datasets: [{
                      label: 'Rupiah',
                      data: [ <?=$januari ?>, <?=$februari ?>, <?=$maret ?>, <?=$april ?>, <?=$mei ?>, <?=$juni ?>, <?=$juli ?>, <?=$agustus ?>, <?=$september ?>, <?=$oktober ?>, <?=$november ?>],
                      //data: [200000, 300000, 400000, 500000, 200000, 200000, 300000, 500000, 230000, 120000, 230000, 1000000],
                      backgroundColor: [
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 206, 86, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(153, 102, 255, 0.2)',
                         'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(75, 192, 192, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                          'rgba(54, 162, 235, 0.2)',
                      ],
                      borderColor: [
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)',
                          'rgba(255, 206, 86, 1)',
                          'rgba(75, 192, 192, 1)',
                          'rgba(153, 102, 255, 1)',
                          'rgba(255, 99, 132, 0.2)'
                      ],
                      borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  }
              }
          });

        </script>
          
          
    </div>
     <div id="chart" class="bg-white col-md-5" style="min-height: 200px;   background-color: #fff;">
      <label> </label>
        <canvas id="laporanHarian" style="min-height: 200px; "></canvas>
        <script>
          var getChart1 = document.getElementById('laporanHarian');
          var myChart1 = new Chart(getChart1, {
              type: 'pie',
              data: {
                   labels: [
                      "Pesawat",
                      "Kereta"
                  ],
                   datasets: [{
                    data: [<?= $pesawat ?>, <?= $kereta ?>],
                    backgroundColor: [
                        "#FF6384",
                        "#63FF84"
                       
                    ]
                }]
            }
        });
        </script>
        
    </div>
  </div>
</div>    
            </div>
          </div>
        </div>
      </div> 
    </div>

              


