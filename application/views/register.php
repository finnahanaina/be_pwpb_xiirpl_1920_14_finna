<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/style.css">
    <script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.js"></script>

    <title>Travela</title>
    <link rel="shortcut icon" href="<?= base_url() ?>favicon.png">
  </head>
  <style>
  	
  	.kotaklogin{
  		
  		background-color:#ffffff;
  		margin-top:80px;
  		border-radius:10px;
  		padding-left: 50px;
      padding-top: 50px;
      padding-right: 50px;
      margin-bottom: 50px;
      margin-left: 20px;
      margin-right: 20px;
  	}
  	.login{
  		color:#000000;
  	}
    .travela-login{
    margin-top: 10px;
    }
    .hr-login{
        border-color: #000000;  
        border-width: 2px;
    }
    .left-login{
        background-color: blue;
        width: 100%
    }
    .login-form{
      margin-top: 40px;
    }
    .belumpunyaakun{
      margin-top: 20px;
      margin-bottom: 0px;
    }
  </style>
  <body background="<?= base_url() ?>assets/bootstrap/img/bg.jpeg" style="background-repeat:no-repeat;background-size:cover;">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-2">
  			</div>
  			<div class="col-md-8 kotaklogin">
          <div class="row">
            <div class="col-md-4"><hr class="hr-login"></div>
             <div class="col-md-4"> <h3 class="travela-login"> <center>Travela</center> </h3></div>
              <div class="col-md-4"><hr class="hr-login"></div>
              
          </div>  				
  				<form action="<?= base_url() ?>auth/registration" method="POST">
				  <div class="form-group login-form">
				    <label for="nama">Nama</label>
				    <input type="text" class="form-control " id="nama" name="nama" placeholder="Masukan Nama" value="<?= set_value('nama'); ?>">
            <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
				  <div class="form-group login-form">
				    <label for="username">Username</label>
				    <input type="text" class="form-control " id="username" name="username" placeholder="Masukan Username" value="<?= set_value('username'); ?>">
            <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
				  <div class="form-group login-form">
				    <label for="password1">Password</label>
				    <input type="Password" class="form-control password" id="password1" name="password1" placeholder="Masukan Password">
             
             <?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
           <div class="form-group login-form">
            <label for="password2">Konfirmasi Password</label>
            <input type="Password" class="form-control password" id="password2" name="password2" placeholder="Masukan Ulang Password">
             <input type="checkbox" class="form-checkbox"> Show password

          </div>
				   <div class="form-group login-form">
				    <label for="tanggallahir">Tangal Lahir</label>
				    <input type="date" class="form-control " id="tanggallahir" name="tanggallahir" value="<?= set_value('tanggallahir'); ?>">
             <?= form_error('tanggallahir', '<small class="text-danger pl-3">', '</small>'); ?>
				  </div>
          <div class="form-group login-form">
            <label for="notelp">No Telphone</label>
            <input type="number" class="form-control " id="notelp" name="notelp" placeholder="Masukan Nomor telphone" value="<?= set_value('notelp'); ?>">
             <?= form_error('notelp', '<small class="text-danger pl-3">', '</small>'); ?>
          </div>
				  
				  <center><button type="submit"class="btn btn-lg btn-primary"> Daftar</button> </a></center>
          <div class="row justify-content-center belumpunyaakun">
            <div class="col-md-12">
              <center><hr><h4>Sudah Punya Akun? <a href="<?= base_url() ?>auth/index">Login Disini</a></h4></center>
            </div>
          </div>
				</form>
  			</div>
  			<div class="col-md-4">
  			</div>
  		</div>
  	</div>
   

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?= base_url() ?>assets/bootstrap/js/jquery.min.js"></script>
     <script src="<?= base_url() ?>assets/bootstrap/js/myscript.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>