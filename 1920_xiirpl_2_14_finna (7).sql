-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Nov 2019 pada 15.08
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `1920_xiirpl_2_14_finna`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `input_pemesanan` (IN `id_pemesanan` INT(11), IN `id_penumpang` INT(11), IN `id_petugas` INT(11), IN `kode_pemesanan` INT(11), IN `tgl_pemesanan` INT(11), IN `tempat_pemesanan` INT(11), IN `total_bayar` INT(11))  begin
insert into pemesanan values(id_pemesanan,id_penumpang,id_petugas,kode_pemesanan,tgl_pemesanan,tempat_pemesanan,total_bayar);
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `buattrigger`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `buattrigger` (
`id_transportasi` int(11)
,`id_rute` int(11)
,`jumlah_kursi` int(11)
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `detail_pemesanan`
--

CREATE TABLE `detail_pemesanan` (
  `id_detail_pemesanan` int(11) NOT NULL,
  `id_pemesanan` int(11) DEFAULT NULL,
  `id_penumpang` int(11) DEFAULT NULL,
  `id_rute` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `nik` varchar(255) DEFAULT NULL,
  `kode_kursi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `detail_pemesanan`
--

INSERT INTO `detail_pemesanan` (`id_detail_pemesanan`, `id_pemesanan`, `id_penumpang`, `id_rute`, `nama`, `nik`, `kode_kursi`) VALUES
(1, 1, 2, 1, 'Finna Hanaina Latifah', '0910291029102', ''),
(2, 2, 2, 1, 'Aprilisia Pracilia tan', '24324244324', ''),
(3, 3, 2, 2, 'Nabila Aprilia Putri', '5456476576587', '20'),
(4, 4, 2, 1, 'Nadia Marcelia', '3232323232', ''),
(5, 5, 2, 1, 'Nur Azizah', '2222323232', ''),
(6, 6, 2, 1, 'Finna Hanaina Latifah', '44343434343', '2'),
(7, 7, 2, 2, 'Finna Hanaina Latifah', '767688889098', '12'),
(8, 8, 2, 2, 'Anggi Nur Anggraini nabila', '435355435', 'A18'),
(9, 9, 2, 2, 'Patricia Tesalonika', '34342342345', '11'),
(10, 10, 2, 1, 'Abigail Amadea ', '12132314342', '3'),
(11, 11, 2, 2, 'Bayu Triestyan', '1212121212', ''),
(12, 12, 2, 3, 'penumpang1', 'nik1', ''),
(13, 13, 2, 3, 'Finna Hanaina Latifah', '121312321423', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga_per_kelas`
--

CREATE TABLE `harga_per_kelas` (
  `id_harga` int(11) NOT NULL,
  `id_rute` int(11) DEFAULT NULL,
  `nama_kelas` varchar(255) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `harga_per_kelas`
--

INSERT INTO `harga_per_kelas` (`id_harga`, `id_rute`, `nama_kelas`, `harga`) VALUES
(1, 1, 'ekonomi', 4000000),
(2, 1, 'bisnis', 4000000),
(3, 2, 'ekonomi', 3500),
(4, 2, 'bisnis', 3500),
(5, 3, 'ekonomi', 3500),
(6, 3, 'bisnis', 3500);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kursi`
--

CREATE TABLE `kursi` (
  `id` int(11) NOT NULL,
  `id_transportasi` int(11) NOT NULL,
  `bagian` varchar(5) NOT NULL,
  `kursi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kursi`
--

INSERT INTO `kursi` (`id`, `id_transportasi`, `bagian`, `kursi`) VALUES
(1, 1, 'A', 1),
(2, 1, 'A', 2),
(3, 1, 'A', 3),
(4, 1, 'A', 4),
(5, 1, 'A', 5),
(6, 1, 'A', 6),
(7, 1, 'A', 7),
(8, 1, 'A', 8),
(9, 1, 'A', 9),
(10, 1, 'A', 10),
(11, 2, 'A', 1),
(12, 2, 'A', 2),
(13, 2, 'A', 3),
(14, 2, 'A', 4),
(15, 2, 'A', 5),
(16, 2, 'A', 6),
(17, 2, 'A', 7),
(18, 2, 'A', 8),
(19, 2, 'A', 9),
(20, 2, 'A', 10),
(21, 3, 'A', 1),
(22, 3, 'A', 2),
(23, 3, 'A', 3),
(24, 3, 'A', 4),
(25, 3, 'A', 5),
(26, 3, 'A', 6),
(27, 3, 'A', 7),
(28, 3, 'A', 8),
(29, 3, 'A', 9),
(30, 3, 'A', 10),
(31, 4, 'A', 1),
(32, 4, 'A', 2),
(33, 4, 'A', 3),
(34, 4, 'A', 4),
(35, 4, 'A', 5),
(36, 4, 'A', 6),
(37, 4, 'A', 7),
(38, 4, 'A', 8),
(39, 4, 'A', 9),
(40, 4, 'A', 10),
(41, 4, 'B', 1),
(42, 4, 'B', 2),
(43, 4, 'B', 3),
(44, 4, 'B', 4),
(45, 4, 'B', 5),
(46, 4, 'B', 6),
(47, 4, 'B', 7),
(48, 4, 'B', 8),
(49, 4, 'B', 9),
(50, 4, 'B', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'petugas'),
(3, 'penumpang');

-- --------------------------------------------------------

--
-- Struktur dari tabel `partner`
--

CREATE TABLE `partner` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `biaya_transaksi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `partner`
--

INSERT INTO `partner` (`id`, `nama`, `gambar`, `keterangan`, `biaya_transaksi`) VALUES
(1, 'Citilink', 'Partner-1571306744-citilink.png', 'transportasi', 0),
(3, 'KAI', 'Partner-1571303361-kai.jpg', 'transportasi', 0),
(4, 'Lion Air', 'Partner-1571303404-lion.png', 'transportasi', 0),
(5, 'Gopay', 'Partner-1571303436-gopay.png', 'Pembayaran Instan', 3000),
(7, 'Indomaret', 'Partner-1571303469-indomaret.png', 'Gerai Retail', 2500),
(8, 'Alfamart', 'Partner-1571303505-alfa.png', 'Gerai Retail', 5000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemesanan`
--

CREATE TABLE `pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `id_penumpang` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL DEFAULT '0',
  `id_harga` int(11) NOT NULL,
  `kode_pemesanan` varchar(255) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `tempat_pemesanan` varchar(255) NOT NULL,
  `total_bayar` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `bukti_pembayaran` varchar(255) NOT NULL,
  `batas_pembayaran` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemesanan`
--

INSERT INTO `pemesanan` (`id_pemesanan`, `id_penumpang`, `id_petugas`, `id_harga`, `kode_pemesanan`, `tgl_pemesanan`, `tempat_pemesanan`, `total_bayar`, `status`, `bukti_pembayaran`, `batas_pembayaran`) VALUES
(1, 2, 2, 1, 'travela-4867523190', '2019-01-30', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441787-bottom.png', 'Nov 03, 2019 21:48:53'),
(2, 2, 2, 1, 'travela-6145782930', '2019-02-28', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441810-1.JPG', 'Nov 03, 2019 21:48:53'),
(3, 2, 2, 3, 'travela-2049136875', '2019-03-30', 'Jakarta', '3500', 'verived', 'bukti_pembayaran-1572441822-BIODATA_CALON_PESERTA___ASESI.png', 'Nov 03, 2019 21:48:53'),
(4, 2, 2, 1, 'travela-0631954278', '2019-04-30', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441832-admin.JPG', 'Nov 03, 2019 21:48:53'),
(5, 2, 2, 1, 'travela-5063289174', '2019-05-30', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441849-asesi.JPG', 'Nov 03, 2019 21:48:53'),
(6, 2, 2, 1, 'travela-3196780524', '2019-06-30', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441863-pertanyaan.JPG', 'Nov 03, 2019 21:48:53'),
(7, 2, 2, 3, 'travela-1743598026', '2019-07-30', 'Jakarta', '3500', 'verived', 'bukti_pembayaran-1572441876-faq.png', 'Nov 03, 2019 21:48:53'),
(8, 2, 2, 3, 'travela-1073265984', '2019-08-30', 'Jakarta', '3500', 'verived', 'bukti_pembayaran-1572441900-soal2.JPG', 'Nov 03, 2019 21:48:53'),
(9, 2, 2, 3, 'travela-6371405298', '2019-09-30', 'Jakarta', '3500', 'verived', 'bukti_pembayaran-1572441914-tuk.png', 'Nov 03, 2019 21:48:53'),
(10, 2, 2, 1, 'travela-3597216048', '2019-10-30', 'Jakarta', '4000000', 'verived', 'bukti_pembayaran-1572441930-soal.JPG', 'Nov 03, 2019 21:48:53'),
(11, 2, 1, 3, 'travela-4582310967', '2019-11-03', 'Jakarta', '3500', 'verived', 'bukti_pembayaran-1572764026-1.JPG', 'Nov 03, 2019 21:48:53'),
(12, 2, 2, 5, 'travela-5781493026', '2019-11-03', 'cobaawal', '3500', 'verived', 'bukti_pembayaran-1572765068-faq.png', 'Nov 03, 2019 21:48:53'),
(13, 2, 2, 5, 'travela-8109734256', '2019-11-03', 'cobaawal', '3500', 'unveriv', '', 'Nov 03, 2019 22:16:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penumpang`
--

CREATE TABLE `penumpang` (
  `id_penumpang` int(11) NOT NULL,
  `id_level` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama_penumpang` varchar(255) DEFAULT NULL,
  `alamat_penumpang` varchar(255) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` enum('l','p') DEFAULT NULL,
  `telephone` varchar(13) DEFAULT NULL,
  `created_when` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penumpang`
--

INSERT INTO `penumpang` (`id_penumpang`, `id_level`, `username`, `password`, `nama_penumpang`, `alamat_penumpang`, `tanggal_lahir`, `jenis_kelamin`, `telephone`, `created_when`) VALUES
(2, 3, 'penumpang', '$2y$10$YzHR2rWgSWBVS7QaqzLar.xHrHZ1R9UxEOh15X7Aq60ghC3t4I3dK', 'penumpang', NULL, '2019-09-16', NULL, '089682515502', '1568624300'),
(3, 3, 'login', '$2y$10$mXqvXmfPGcUVAmNR.7oRPer.pIqn18eI4puxJdQXmtmKppNwr.uLa', 'login', NULL, '2019-09-11', NULL, '3463875632478', '1568627900');

-- --------------------------------------------------------

--
-- Struktur dari tabel `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `id_level` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama_petugas` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `id_level`, `username`, `password`, `nama_petugas`) VALUES
(1, 1, 'admin', '$2y$10$q5ehwYM96OPJqXqks1m9WeWEfUKrtpOYW5wZ13jJIfxHeg2VxthzO', 'admin'),
(2, 2, 'petugas', '$2y$10$YwAJ4KQxj2YPGTh0jlTwA.lEfwmrDN47jCe3Y3tfMEpfIRHsxtJFy', 'petugas'),
(3, 2, 'finnalatifaaa', '$2y$10$ILZEfPKK6rNTnzJdv9PBxuCzF54TMDmYneNrMu90mYJXJr9SPjgB.', 'Finna Hanaina Latifah'),
(6, 2, 'bayutriestyan', '$2y$10$RHcpo7gd3WlNDg3LVRdpBeWC.nVqgRRuRJn65tB4erQNQioUxB0aC', 'Bayu Triestyan'),
(7, 2, 'cobapetugas', '$2y$10$DGZsAP/u2LySu2qKBOQ3Cuw6u/gi7E6F/f11MxeSM5WwNMP5C6B0S', 'cobapetugas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rute`
--

CREATE TABLE `rute` (
  `id_rute` int(11) NOT NULL,
  `id_transportasi` int(11) DEFAULT NULL,
  `kode_rute` varchar(255) NOT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `rute_awal` varchar(255) DEFAULT NULL,
  `rute_akhir` varchar(255) DEFAULT NULL,
  `tgl_berangkat` date NOT NULL,
  `jam_berangkat` time NOT NULL,
  `jam_cekin` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rute`
--

INSERT INTO `rute` (`id_rute`, `id_transportasi`, `kode_rute`, `tujuan`, `rute_awal`, `rute_akhir`, `tgl_berangkat`, `jam_berangkat`, `jam_cekin`) VALUES
(1, 1, 'jktsg01', 'Singapore', 'Jakarta', 'Singapore', '2019-10-30', '03:00:00', '02:00:00'),
(2, 2, 'jktbg01', 'Jakarta', 'Jakarta', 'Bogor', '2019-11-03', '12:59:00', '12:59:00'),
(3, 4, 'cobarute', 'cobakhir', 'cobaawal', 'cobakhir', '2019-11-03', '22:59:00', '12:58:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe_transportasi`
--

CREATE TABLE `tipe_transportasi` (
  `id_tipe_transportasi` int(11) NOT NULL,
  `nama_tipe` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe_transportasi`
--

INSERT INTO `tipe_transportasi` (`id_tipe_transportasi`, `nama_tipe`, `keterangan`) VALUES
(1, 'pesawat', '-'),
(2, 'kereta', '-');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transportasi`
--

CREATE TABLE `transportasi` (
  `id_transportasi` int(11) NOT NULL,
  `id_tipe_transportasi` int(11) DEFAULT NULL,
  `kode_transportasi` varchar(255) DEFAULT NULL,
  `jumlah_kursi` int(11) DEFAULT NULL,
  `kursi_ekonomi` int(11) NOT NULL,
  `kursi_bisnis` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transportasi`
--

INSERT INTO `transportasi` (`id_transportasi`, `id_tipe_transportasi`, `kode_transportasi`, `jumlah_kursi`, `kursi_ekonomi`, `kursi_bisnis`, `keterangan`) VALUES
(1, 1, 'citilink airlines 111', 20, 10, 10, '1'),
(2, 2, 'express111', 20, 10, 10, '3'),
(3, 1, 'Lion Air 01', 20, 10, 10, '4'),
(4, 2, 'cobatransportasi', 20, 10, 10, '3');

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `vrute`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `vrute` (
`id_rute` int(11)
,`kode_rute` varchar(255)
,`tujuan` varchar(255)
,`rute_awal` varchar(255)
,`rute_akhir` varchar(255)
,`tgl_berangkat` date
,`jam_berangkat` time
,`jam_cekin` time
,`id_transportasi` int(11)
,`kode_transportasi` varchar(255)
,`jumlah_kursi` int(11)
,`keterangan` varchar(255)
,`nama_tipe` varchar(255)
,`id_tipe_transportasi` int(11)
,`id_harga` int(11)
,`nama_kelas` varchar(255)
,`harga` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_bisnis`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_bisnis` (
`nama_kelas` varchar(255)
,`harga` int(11)
,`tujuan` varchar(255)
,`keterangan` varchar(255)
,`jumlah_kursi` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_ekonomi`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_ekonomi` (
`nama_kelas` varchar(255)
,`harga` int(11)
,`tujuan` varchar(255)
,`keterangan` varchar(255)
,`jumlah_kursi` int(11)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `buattrigger`
--
DROP TABLE IF EXISTS `buattrigger`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `buattrigger`  AS  select `t`.`id_transportasi` AS `id_transportasi`,`r`.`id_rute` AS `id_rute`,`t`.`jumlah_kursi` AS `jumlah_kursi` from ((`transportasi` `t` join `rute` `r` on((`r`.`id_transportasi` = `t`.`id_transportasi`))) join `detail_pemesanan` `d` on((`d`.`id_rute` = `r`.`id_rute`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `vrute`
--
DROP TABLE IF EXISTS `vrute`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vrute`  AS  select `rute`.`id_rute` AS `id_rute`,`rute`.`kode_rute` AS `kode_rute`,`rute`.`tujuan` AS `tujuan`,`rute`.`rute_awal` AS `rute_awal`,`rute`.`rute_akhir` AS `rute_akhir`,`rute`.`tgl_berangkat` AS `tgl_berangkat`,`rute`.`jam_berangkat` AS `jam_berangkat`,`rute`.`jam_cekin` AS `jam_cekin`,`transportasi`.`id_transportasi` AS `id_transportasi`,`transportasi`.`kode_transportasi` AS `kode_transportasi`,`transportasi`.`jumlah_kursi` AS `jumlah_kursi`,`transportasi`.`keterangan` AS `keterangan`,`tipe_transportasi`.`nama_tipe` AS `nama_tipe`,`tipe_transportasi`.`id_tipe_transportasi` AS `id_tipe_transportasi`,`harga_per_kelas`.`id_harga` AS `id_harga`,`harga_per_kelas`.`nama_kelas` AS `nama_kelas`,`harga_per_kelas`.`harga` AS `harga` from (((`harga_per_kelas` join `rute` on((`rute`.`id_rute` = `harga_per_kelas`.`id_rute`))) join `transportasi` on((`transportasi`.`id_transportasi` = `rute`.`id_transportasi`))) join `tipe_transportasi` on((`tipe_transportasi`.`id_tipe_transportasi` = `transportasi`.`id_tipe_transportasi`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_bisnis`
--
DROP TABLE IF EXISTS `v_bisnis`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_bisnis`  AS  select `h`.`nama_kelas` AS `nama_kelas`,`h`.`harga` AS `harga`,`r`.`tujuan` AS `tujuan`,`t`.`keterangan` AS `keterangan`,`t`.`jumlah_kursi` AS `jumlah_kursi` from ((`harga_per_kelas` `h` join `rute` `r` on((`h`.`id_rute` = `r`.`id_rute`))) join `transportasi` `t` on((`r`.`id_transportasi` = `t`.`id_transportasi`))) where (`h`.`nama_kelas` = 'bisnis') ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_ekonomi`
--
DROP TABLE IF EXISTS `v_ekonomi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_ekonomi`  AS  select `h`.`nama_kelas` AS `nama_kelas`,`h`.`harga` AS `harga`,`r`.`tujuan` AS `tujuan`,`t`.`keterangan` AS `keterangan`,`t`.`jumlah_kursi` AS `jumlah_kursi` from ((`harga_per_kelas` `h` join `rute` `r` on((`h`.`id_rute` = `r`.`id_rute`))) join `transportasi` `t` on((`r`.`id_transportasi` = `t`.`id_transportasi`))) where (`h`.`nama_kelas` = 'ekonomi') ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD PRIMARY KEY (`id_detail_pemesanan`),
  ADD KEY `detail_pemesanan_ibfk_1` (`id_rute`),
  ADD KEY `id_pemesanan` (`id_pemesanan`),
  ADD KEY `id_penumpang` (`id_penumpang`);

--
-- Indeks untuk tabel `harga_per_kelas`
--
ALTER TABLE `harga_per_kelas`
  ADD PRIMARY KEY (`id_harga`),
  ADD KEY `id_rute` (`id_rute`);

--
-- Indeks untuk tabel `kursi`
--
ALTER TABLE `kursi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indeks untuk tabel `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`),
  ADD KEY `id_penumpang` (`id_penumpang`),
  ADD KEY `id_petugas` (`id_petugas`);

--
-- Indeks untuk tabel `penumpang`
--
ALTER TABLE `penumpang`
  ADD PRIMARY KEY (`id_penumpang`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indeks untuk tabel `rute`
--
ALTER TABLE `rute`
  ADD PRIMARY KEY (`id_rute`),
  ADD KEY `id_transportasi` (`id_transportasi`);

--
-- Indeks untuk tabel `tipe_transportasi`
--
ALTER TABLE `tipe_transportasi`
  ADD PRIMARY KEY (`id_tipe_transportasi`);

--
-- Indeks untuk tabel `transportasi`
--
ALTER TABLE `transportasi`
  ADD PRIMARY KEY (`id_transportasi`),
  ADD KEY `id_tipe_transportasi` (`id_tipe_transportasi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  MODIFY `id_detail_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `harga_per_kelas`
--
ALTER TABLE `harga_per_kelas`
  MODIFY `id_harga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `kursi`
--
ALTER TABLE `kursi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT untuk tabel `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `penumpang`
--
ALTER TABLE `penumpang`
  MODIFY `id_penumpang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `rute`
--
ALTER TABLE `rute`
  MODIFY `id_rute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tipe_transportasi`
--
ALTER TABLE `tipe_transportasi`
  MODIFY `id_tipe_transportasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `transportasi`
--
ALTER TABLE `transportasi`
  MODIFY `id_transportasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `detail_pemesanan`
--
ALTER TABLE `detail_pemesanan`
  ADD CONSTRAINT `detail_pemesanan_ibfk_2` FOREIGN KEY (`id_penumpang`) REFERENCES `penumpang` (`id_penumpang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_pemesanan_ibfk_3` FOREIGN KEY (`id_rute`) REFERENCES `rute` (`id_rute`);

--
-- Ketidakleluasaan untuk tabel `harga_per_kelas`
--
ALTER TABLE `harga_per_kelas`
  ADD CONSTRAINT `harga_per_kelas_ibfk_1` FOREIGN KEY (`id_rute`) REFERENCES `rute` (`id_rute`);

--
-- Ketidakleluasaan untuk tabel `pemesanan`
--
ALTER TABLE `pemesanan`
  ADD CONSTRAINT `pemesanan_ibfk_1` FOREIGN KEY (`id_penumpang`) REFERENCES `penumpang` (`id_penumpang`),
  ADD CONSTRAINT `pemesanan_ibfk_2` FOREIGN KEY (`id_petugas`) REFERENCES `petugas` (`id_petugas`);

--
-- Ketidakleluasaan untuk tabel `penumpang`
--
ALTER TABLE `penumpang`
  ADD CONSTRAINT `penumpang_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Ketidakleluasaan untuk tabel `petugas`
--
ALTER TABLE `petugas`
  ADD CONSTRAINT `petugas_ibfk_1` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Ketidakleluasaan untuk tabel `rute`
--
ALTER TABLE `rute`
  ADD CONSTRAINT `rute_ibfk_1` FOREIGN KEY (`id_transportasi`) REFERENCES `transportasi` (`id_transportasi`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `transportasi`
--
ALTER TABLE `transportasi`
  ADD CONSTRAINT `transportasi_ibfk_1` FOREIGN KEY (`id_tipe_transportasi`) REFERENCES `tipe_transportasi` (`id_tipe_transportasi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
