const flashData = $('.flash-data').data('flashdata');

if (flashData) {
    Swal.fire({ 
	  title: 'Data',
	  text: 'Berhasil '+ flashData,
	  type: 'success'
	});
}


$('.tombol-hapus').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');	
        const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
              },              
            })
            swalWithBootstrapButtons.fire({
              title: 'Apakah Anda Yakin?',
              text: "Data Akan Dihapus!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
              confirmButtonText: 'Yakin!',
              cancelButtonText: 'Batal!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                document.location.href = href;
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons.fire(
                  'Dibatalkan',
                  'Data Aman :)',
                  'error'
                )
              }
            })
		});
// tombol veriv
$('.tombol-veriv').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');  
        const swalWithBootstrapButtons = Swal.mixin({
              customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
              },              
            })
            swalWithBootstrapButtons.fire({
              title: 'Verivikasi?',
              text: "Pastikan data sudah valid!",
              type: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
              confirmButtonText: 'Yakin!',
              cancelButtonText: 'Batal!',
              reverseButtons: true
            }).then((result) => {
              if (result.value) {
                document.location.href = href;
              } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
              ) {
                swalWithBootstrapButtons.fire(
                  'Dibatalkan',
                  ':)',
                  'error'
                )
              }
            })
        });
// tombol Lihat File
$('.lihat-file').on('click', function (e) {
    e.preventDefault();
    const id = $(this).attr('id');  
    Swal.fire({
      imageUrl: id,
      imageHeight: 500,
      imageWidth: 500,
      imageAlt: id
    })
});



$('.logout').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');  
     let timerInterval
      Swal.fire({
        title: 'Mohon Tunggu!',
        html: 'Anda Akan Keluar dalam  <strong></strong> detik lagi.',
        timer: 2000,
        onBeforeOpen: () => {
          Swal.showLoading()
          timerInterval = setInterval(() => {
            Swal.getContent().querySelector('strong')
              .textContent = Swal.getTimerLeft()
          }, 200)
        },
        onClose: () => {
          clearInterval(timerInterval)
        }
      }).then((result) => {
        if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.timer
        ) {
          document.location.href = href;
        } 
      })

  });




    var cek = $('.form-checkbox').val();
      $('.form-checkbox').click(function() {
        if ($(this).is(':checked')) {
          $('.password').attr('type', 'text');
        } else {
          $('.password').attr('type', 'password');
        }
   });
      
// datatable
$(document).ready( function () {
    $('.datatable').DataTable();
} );
 

      $(function () {
        
          $('.button-tambah-kursi').click(function () {
              var template = `
               <div class="row mb-3">
                  <div class="col-md-5">
                    <label for="bagian_kursi">Bagian Kursi</label>
                    <input type="text" class="form-control" id="bagian_kursi" name="bagian_kursi[]" placeholder="Contoh 'A' ">
                     
                  </div>
                  <div class="col-md-5">
                    <label for="jumlah_kursi">Jumlah Kursi</label>
                    <input type="number" class="form-control" id="jumlah_kursi" name="jumlah_kursi[]" >
                    
                  </div>
                  <div class="col-md-2 mt-4" >
                    <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                  </div>
                        `;
              $('#kursi .tambah-kursi').append(template);
          });

          $('#kursi').on('click', '.delete-row', function () {
              $(this).parent().parent().remove();

          });
      })

      $(function () {
        var button = $('.button-ganti-password');
          $('.button-ganti-password').click(function () {
              var template = `
               <div class="row mb-3">
                  <div class="col-md-10">
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="text" class="form-control" name="password">
                    </div>  
                  </div>
                  <div class="col-md-2 mt-4" >
                    <button type="button" class="btn btn-danger delete-row"><i class="fa fa-trash"></i></button>
                  </div>
                </div>
                        `;
              $('#petugas .ganti-password').append(template);
              button.hide();
          });

          $('#petugas').on('click', '.delete-row', function () {
              $(this).parent().parent().remove();
              button.show();
          });
      })
 

// halaman Penumpang
// tombol veriv
