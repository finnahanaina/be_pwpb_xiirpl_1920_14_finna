const flashData = $('.flash-data').data('flashdata');
console.log(flashdata);

if (flashData) {
    Swal({
        title: 'Data Mahasiswa ',
        text: 'Berhasil ' + flashData,
        type: 'success'
    });
}

// tombol-hapus
$('.tombol-hapus').on('click', function (e) {

    e.preventDefault();
    const href = $(this).attr('href');

    Swal({
        title: 'Apakah anda yakin',
        text: "data mahasiswa akan dihapus",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus Data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

// tombol-veriv
$('.tombol-veriv').on('click', function (e) {
    e.preventDefault();
    const href = $(this).attr('href');
    Swal({
        title: 'Apakah anda yakin',
        text: "data mahasiswa akan dihapus",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Hapus Data!'
    }).then((result) => {
        if (result.value) {
            document.location.href = href;
        }
    })
});

$('#tampilkan-password').ready(function(){
    let tipeSaatIni = $('#password').attr('type');
    let tipeBaru = '';
    if(tipeSaatIni == 'text'){
        tipeBaru = 'password';
    }else if(tipeSaatIni == 'password'){
        tipeBaru = 'text';
    }$(#password).attr("type",tipeBaru);
    
});